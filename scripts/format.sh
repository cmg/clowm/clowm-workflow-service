#!/bin/sh -e
set -x

isort --force-single-line-imports app
ruff check --fix --show-fixes app
ruff format app
isort app
