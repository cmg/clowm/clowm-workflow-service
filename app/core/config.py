import os
from enum import Enum
from typing import Literal, Tuple, Type

from pydantic import AnyHttpUrl, BaseModel, Field, FilePath, MySQLDsn, NameEmail, SecretStr, computed_field
from pydantic_settings import (
    BaseSettings,
    JsonConfigSettingsSource,
    PydanticBaseSettingsSource,
    SettingsConfigDict,
    TomlConfigSettingsSource,
    YamlConfigSettingsSource,
)


class MonitorJobBackoffStrategy(str, Enum):
    LINEAR = "LINEAR"
    EXPONENTIAL = "EXPONENTIAL"
    CONSTANT = "CONSTANT"
    NOMONITORING = "NOMONITORING"


class DBSettings(BaseModel):
    port: int = Field(3306, description="Port of the database.")
    host: str = Field("localhost", description="Host of the database.")
    name: str = Field(..., description="Name of the database.")
    user: str = Field(..., description="Username in the database.")
    password: SecretStr = Field(..., description="Password for the database user.")
    verbose: bool = Field(False, description="Flag whether to print the SQL Queries in the logs.")

    @computed_field
    def dsn_sync(self) -> MySQLDsn:
        return MySQLDsn.build(
            scheme="mysql+pymysql",
            password=self.password.get_secret_value(),
            host=self.host,
            port=self.port,
            path=self.name,
            username=self.user,
        )

    @computed_field
    def dsn_async(self) -> MySQLDsn:
        return MySQLDsn.build(
            scheme="mysql+aiomysql",
            password=self.password.get_secret_value(),
            host=self.host,
            port=self.port,
            path=self.name,
            username=self.user,
        )


class EmailSettings(BaseModel):
    server: str | Literal["console"] | None = Field(
        None,
        description="Hostname of SMTP server. If `console`, emails are printed to the console. If None, no emails are sent",
    )
    port: int = Field(587, description="Port of the SMTP server")
    sender_email: NameEmail = Field(
        NameEmail(email="no-reply@clowm.com", name="CloWM"), description="Email address from which the emails are sent."
    )
    reply_email: NameEmail | None = Field(None, description="Email address in the `Reply-To` header.")
    connection_security: Literal["ssl", "tls"] | Literal["starttls"] | None = Field(
        None, description="Connection security to the SMTP server."
    )
    local_hostname: str | None = Field(None, description="Overwrite the local hostname from which the emails are sent.")
    ca_path: FilePath | None = Field(None, description="Path to a custom CA certificate.")
    key_path: FilePath | None = Field(None, description="Path to the CA key.")
    user: str | None = Field(None, description="Username to use for SMTP login")
    password: SecretStr | None = Field(None, description="Password to use for SMTP login")


class SlurmSettings(BaseModel):
    uri: AnyHttpUrl = Field(..., description="URI of the Slurm Cluster")
    token: SecretStr = Field(..., description="JWT for authentication the Slurm REST API")
    user: str = Field("slurm", description="User of communication with slurm")


class ClusterSettings(BaseModel):
    slurm: SlurmSettings
    nxf_config: str | None = Field(None, description="Path to a nextflow configuration for every run")
    nxf_bin: str = Field("nextflow", description="Path to the nextflow executable")
    working_directory: str = Field("/tmp", description="Working directory for the slurm job with the nextflow command")
    active_workflow_execution_limit: int = Field(3, description="The limit of active workflow executions per user.")
    job_monitoring: MonitorJobBackoffStrategy = Field(
        MonitorJobBackoffStrategy.EXPONENTIAL,
        description="Strategy for polling the slurm job status for monitoring the workflow execution.",
    )
    execution_cleanup: bool = Field(True, description="Remove work directory after the workflow execution")


class S3Settings(BaseModel):
    uri: AnyHttpUrl = Field(..., description="URI of the S3 Object Storage.")
    access_key: str = Field(..., description="Access key for the S3 that owns the buckets.")
    secret_key: SecretStr = Field(..., description="Secret key for the S3 that owns the buckets.")
    admin_access_key: str = Field(
        ..., description="Access key for the Ceph Object Gateway user with `user=*,bucket=*` capabilities."
    )
    admin_secret_key: SecretStr = Field(
        ..., description="Secret key for the Ceph Object Gateway user with `user=*,bucket=*` capabilities."
    )
    params_bucket: str = Field(
        "clowm-workflow-params", description="Bucket where the nextflow configurations should be saved"
    )
    workflow_bucket: str = Field("clowm-workflows", description="Bucket where to cache workflow files")
    icon_bucket: str = Field("clowm-icons", description="Bucket where to save workflow icons")


class OPASettings(BaseModel):
    uri: AnyHttpUrl = Field(..., description="URI of the OPA Service")
    policy_path: str = Field("/clowm/authz/allow", description="Path to the OPA Policy for Authorization")


class OTLPSettings(BaseModel):
    grpc_endpoint: str | None = Field(
        None, description="OTLP compatible endpoint to send traces via gRPC, e.g. Jaeger", examples=["localhost:8080"]
    )
    secure: bool = Field(False, description="Connection type")


class Settings(BaseSettings):
    api_prefix: str = Field("", description="Path Prefix for all API endpoints.")
    dev_system: bool = Field(False, description="Open a endpoint where to execute arbitrary workflows.")
    public_key: SecretStr | None = Field(None, description="Public RSA Key in PEM format to verify the JWTs.")
    public_key_file: FilePath | None = Field(
        None, description="Path to Public RSA Key in PEM format to verify the JWTs."
    )
    ui_uri: AnyHttpUrl = Field(..., description="URL of the UI")

    @computed_field
    def public_key_value(self) -> SecretStr:
        pub_key = ""
        if self.public_key is not None:
            pub_key = self.public_key.get_secret_value()
        if self.public_key_file is not None:
            with open(self.public_key_file) as f:
                pub_key = f.read()
        if len(pub_key) == 0:
            raise ValueError("CLOWM_PUBLIC_KEY or CLOWM_PUBLIC_KEY_FILE must be set")
        return SecretStr(pub_key)

    db: DBSettings
    smtp: EmailSettings = EmailSettings()
    cluster: ClusterSettings
    s3: S3Settings
    opa: OPASettings
    otlp: OTLPSettings = OTLPSettings()

    model_config = SettingsConfigDict(
        env_prefix="CLOWM_",
        env_file=".env",
        extra="ignore",
        secrets_dir="/run/secrets" if os.path.isdir("/run/secrets") else None,
        env_nested_delimiter="__",
        yaml_file=os.getenv("CLOWM_CONFIG_FILE_YAML", "config.yaml"),
        toml_file=os.getenv("CLOWM_CONFIG_FILE_TOML", "config.toml"),
        json_file=os.getenv("CLOWM_CONFIG_FILE_JSON", "config.json"),
    )

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: Type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> Tuple[PydanticBaseSettingsSource, ...]:
        return (
            env_settings,
            file_secret_settings,
            dotenv_settings,
            YamlConfigSettingsSource(settings_cls),
            TomlConfigSettingsSource(settings_cls),
            JsonConfigSettingsSource(settings_cls),
        )


settings = Settings()
