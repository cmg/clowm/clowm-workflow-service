from typing import Annotated
from uuid import UUID as NativeUUID

from pydantic import AnyHttpUrl, PlainSerializer

UUID = Annotated[NativeUUID, PlainSerializer(lambda uuid: str(uuid), return_type=str, when_used="unless-none")]
URL = Annotated[AnyHttpUrl, PlainSerializer(lambda url: str(url), return_type=str, when_used="unless-none")]
