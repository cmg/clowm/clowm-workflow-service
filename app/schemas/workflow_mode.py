from pydantic import BaseModel, ConfigDict, Field

from app.schemas import UUID


class _BaseWorkflowMode(BaseModel):
    model_config = ConfigDict(from_attributes=True)
    schema_path: str = Field(
        ..., description="Path to the alternative parameter schema", examples=["modes/schema1.json"]
    )
    entrypoint: str = Field(
        ...,
        description="Name of the process the workflow should start with. Argument to the parameter '-entry'",
        examples=["example"],
    )
    name: str = Field(..., description="Name of the workflow mode", examples=["Example Name"])


class WorkflowModeIn(_BaseWorkflowMode):
    pass


class WorkflowModeOut(_BaseWorkflowMode):
    mode_id: UUID = Field(..., description="ID of the workflow mode", examples=["2a23a083-b6b9-4681-9ec4-ff4ffbe85d3c"])
