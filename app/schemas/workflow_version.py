from datetime import datetime

from clowmdb.models import WorkflowVersion as WorkflowVersionDB
from pydantic import AnyHttpUrl, BaseModel, Field

from app.core.config import settings
from app.schemas import URL, UUID


class ParameterExtension(BaseModel):
    mapping: dict[str, dict[str, str | int | float]] = Field(
        default_factory=lambda: {},
        description="The inner dictionary contains the display name as key and the parameter value as value. The "
        "outer dictionary has the parameter name as key.",
        examples=[{"some-complex-parameter": {"Option 1": "/some/path", "Option 2": "/some/other/path"}}],
    )
    defaults: dict[str, str | int | float | bool] = Field(
        default_factory=lambda: {},
        description="Dictionary with parameter name as key and default value as value",
        examples=[{"parameter1": "somevalue", "parameter2": 12}],
    )


class WorkflowVersionStatus(BaseModel):
    status: WorkflowVersionDB.Status = Field(
        ..., description="Status of the workflow version", examples=[WorkflowVersionDB.Status.PUBLISHED]
    )


class WorkflowVersion(WorkflowVersionStatus):
    workflow_id: UUID = Field(
        ..., description="ID of the corresponding workflow", examples=["20128c04-e834-40a8-9878-68939ae46423"]
    )
    version: str = Field(
        ...,
        description="Version of the Workflow. Should follow semantic versioning",
        examples=["v1.0.0"],
        min_length=5,
        max_length=10,
    )
    workflow_version_id: str = Field(
        ...,
        description="Hash of the git commit",
        examples=["ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
        pattern=r"^[0-9a-f]{40}$",
        min_length=40,
        max_length=40,
    )
    icon_url: AnyHttpUrl | None = Field(
        None,
        description="URL of the icon for this workflow version",
        examples=[f"{settings.s3.uri}{settings.s3.icon_bucket}/980a9446c7f2460c83187cbb876f8424.png"],
    )
    created_at: int = Field(
        ...,
        description="Timestamp when the version was created as UNIX timestamp",
        examples=[round(datetime(year=2023, month=1, day=1).timestamp())],
    )
    modes: list[UUID] = Field(
        default=[],
        description="Optional modes his workflow version has",
        examples=["2a23a083-b6b9-4681-9ec4-ff4ffbe85d3c"],
    )
    parameter_extension: ParameterExtension | None = Field(
        default=None, description="Parameter extension specific for this CloWM instance"
    )

    @staticmethod
    def from_db_version(
        db_version: WorkflowVersionDB, mode_ids: list[UUID] | None = None, load_modes: bool = False
    ) -> "WorkflowVersion":
        """
        Create a WorkflowVersion schema from the workflow version database model.

        Parameters
        ----------
        db_version : clowmdb.models.WorkflowVersion
            Database model of a workflow.version
        mode_ids : list[uuid.UUID] | None, default None
            List of mode IDs to add to the workflow version
        load_modes : bool, default False
            Flag if the workflow versions should load the modes from the database model.

        Returns
        -------
        version : WorkflowVersion
            Schema from the database model

        Notes
        -----
        If the parameter `load_modes` is True and the database model has a non-empty list of modes,
        then the parameter `mode_ids` will be ignored.
        """
        icon_url = None
        if mode_ids is None:
            mode_ids = []
        if db_version.icon_slug is not None:
            icon_url = str(settings.s3.uri) + "/".join([settings.s3.icon_bucket, db_version.icon_slug])
        return WorkflowVersion(
            workflow_id=db_version.workflow_id,
            version=db_version.version,
            workflow_version_id=db_version.git_commit_hash,
            icon_url=icon_url,
            created_at=db_version.created_at,
            status=db_version.status,
            modes=(
                [mode.mode_id for mode in db_version.workflow_modes]
                if load_modes and len(db_version.workflow_modes) > 0
                else mode_ids
            ),
            parameter_extension=(
                None
                if db_version.parameter_extension is None
                else ParameterExtension.model_validate(db_version.parameter_extension)
            ),
        )


class IconUpdateOut(BaseModel):
    icon_url: URL = Field(
        ...,
        description="URL to the uploaded icon",
        examples=[f"{settings.s3.uri}{settings.s3.icon_bucket}/980a9446c7f2460c83187cbb876f8424.png"],
    )
