from datetime import date
from typing import Sequence

from clowmdb.models import Workflow as WorkflowDB
from clowmdb.models import WorkflowVersion as WorkflowVersionDB
from pydantic import BaseModel, Field

from app.schemas import URL, UUID
from app.schemas.workflow_mode import WorkflowModeIn
from app.schemas.workflow_version import WorkflowVersion


class _BaseWorkflow(BaseModel):
    name: str = Field(
        ...,
        description="Short descriptive name of the workflow",
        examples=["RNA ReadMapper"],
        max_length=64,
        min_length=3,
    )
    short_description: str = Field(
        ...,
        description="Short description of the workflow",
        examples=["This should be a very good example of a short and descriptive description"],
        max_length=256,
        min_length=64,
    )
    repository_url: URL = Field(
        ...,
        description="URL to the Git repository belonging to this workflow",
        examples=["https://github.com/example-user/example"],
    )


class WorkflowIn(_BaseWorkflow):
    git_commit_hash: str = Field(
        ...,
        description="Hash of the git commit",
        examples=["ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
        pattern=r"^[0-9a-f]{40}$",
        min_length=40,
        max_length=40,
    )
    initial_version: str = Field(
        "v1.0.0",
        description="Initial version of the Workflow. Should follow semantic versioning",
        examples=["v1.0.0"],
        min_length=5,
        max_length=10,
    )
    token: str | None = Field(
        None,
        description="Token to access the content git repository",
        examples=["vnpau89avpa48iunga984gh9h89pvhj"],
        max_length=128,
    )
    modes: list[WorkflowModeIn] = Field(
        default=[], max_length=10, description="List of modes with alternative entrypoint the new workflow has"
    )


class WorkflowOut(_BaseWorkflow):
    workflow_id: UUID = Field(..., description="ID of the workflow", examples=["20128c04-e834-40a8-9878-68939ae46423"])
    versions: list[WorkflowVersion] = Field(..., description="Versions of the workflow")
    developer_id: UUID = Field(
        ..., description="ID of developer of the workflow", examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"]
    )
    private: bool = Field(default=False, description="Flag if the workflow is hosted in a private git repository")

    @staticmethod
    def from_db_workflow(
        db_workflow: WorkflowDB, versions: Sequence[WorkflowVersionDB] | None = None, load_modes: bool = True
    ) -> "WorkflowOut":
        """
        Create a WorkflowOut schema from the workflow database model.

        Parameters
        ----------
        db_workflow : clowmdb.models.Workflow
            Database model of a workflow.
        versions : list[clowmdb.models.WorkflowVersion] | None, default None
            List of versions to attach to the schema. If None, they will be loaded from the DB model.
        load_modes : bool, default True
            Flag if the workflow versions should load the modes from the database model.

        Returns
        -------
        workflow : WorkflowOut
            Schema from the database model
        """
        if versions is not None:
            temp_versions = versions
        else:
            temp_versions = db_workflow.versions
        return WorkflowOut(
            workflow_id=db_workflow.workflow_id,
            name=db_workflow.name,
            short_description=db_workflow.short_description,
            repository_url=db_workflow.repository_url,
            versions=[WorkflowVersion.from_db_version(v, load_modes=load_modes) for v in temp_versions],
            developer_id=db_workflow.developer_id,
            private=db_workflow.credentials_token is not None,
        )


class WorkflowStatistic(BaseModel):
    day: date = Field(..., description="Day of the datapoint", examples=[date(day=1, month=1, year=2023)])
    count: int = Field(..., description="Number of started workflows on that day", examples=[1])


class WorkflowCredentialsIn(BaseModel):
    token: str = Field(
        ...,
        description="Token to access the content git repository",
        examples=["vnpau89avpa48iunga984gh9h89pvhj"],
        max_length=128,
    )


class WorkflowCredentialsOut(BaseModel):
    token: str | None = Field(
        None,
        description="Token to access the content git repository",
        examples=["vnpau89avpa48iunga984gh9h89pvhj"],
        max_length=128,
    )


class WorkflowUpdate(BaseModel):
    version: str = Field(
        ...,
        description="Version of the Workflow. Should follow semantic versioning",
        examples=["v1.1.0"],
        min_length=5,
        max_length=10,
    )
    git_commit_hash: str = Field(
        ...,
        description="Hash of the git commit",
        examples=["ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
        pattern=r"^[0-9a-f]{40}$",
        min_length=40,
        max_length=40,
    )
    append_modes: list[WorkflowModeIn] = Field(default=[], description="Add modes to the new workflow version")
    delete_modes: list[UUID] = Field(
        [], description="Delete modes for the new workflow version.", examples=["2a23a083-b6b9-4681-9ec4-ff4ffbe85d3c"]
    )
