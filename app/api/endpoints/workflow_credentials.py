from typing import Annotated, Any, Awaitable, Callable

from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException, status
from opentelemetry import trace

from app.api.background import upload_scm_file
from app.api.dependencies import AuthorizationDependency, CurrentUser, CurrentWorkflow, DBSession, HTTPClient, S3Service
from app.api.utils import check_repo
from app.core.config import settings
from app.crud.crud_workflow import CRUDWorkflow, CRUDWorkflowVersion
from app.git_repository import GitHubRepository, build_repository
from app.schemas.workflow import WorkflowCredentialsIn, WorkflowCredentialsOut
from app.scm import SCM, SCMProvider
from app.utils.otlp import start_as_current_span_async

router = APIRouter(prefix="/workflows/{wid}/credentials", tags=["Workflow Credentials"])
workflow_authorization = AuthorizationDependency(resource="workflow")

Authorization = Annotated[Callable[[str], Awaitable[Any]], Depends(workflow_authorization)]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("", status_code=status.HTTP_200_OK, summary="Get the credentials of a workflow")
@start_as_current_span_async("api_workflow_credentials_get", tracer=tracer)
async def get_workflow_credentials(
    workflow: CurrentWorkflow, current_user: CurrentUser, authorization: Authorization
) -> WorkflowCredentialsOut:
    """
    Get the credentials for the repository of a workflow. Only the developer of a workflow can do this.\n
    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    current_user : clowmdb.models.User
        Current user.  Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    workflow : app.schemas.workflow.WorkflowOut
        Workflow with existing ID
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    await authorization("update")
    if current_user.uid != workflow.developer_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Only the developer can retrieve the repository credentials"
        )
    return WorkflowCredentialsOut(token=workflow.credentials_token)


@router.put("", status_code=status.HTTP_200_OK, summary="Update the credentials of a workflow")
@start_as_current_span_async("api_workflow_credentials_update", tracer=tracer)
async def update_workflow_credentials(
    credentials: WorkflowCredentialsIn,
    workflow: CurrentWorkflow,
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    background_tasks: BackgroundTasks,
    client: HTTPClient,
) -> None:
    """
    Update the credentials for the repository of a workflow.\n
    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    credentials : app.schemas.workflow.WorkflowCredentialsIn
        Updated credentials for the workflow git repository
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user.  Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    client : httpx.AsyncClient
        Http client with an open connection. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    workflow : app.schemas.workflow.WorkflowOut
        Workflow with existing ID
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    await authorization("update")
    if current_user.uid != workflow.developer_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Only the developer can update the repository credentials"
        )
    latest_version = await CRUDWorkflowVersion.get_latest(workflow.workflow_id, published=False, db=db)
    # Build a git repository object based on the repository url
    repo = build_repository(
        workflow.repository_url,
        latest_version.git_commit_hash,  # type: ignore[union-attr]
        token=credentials.token,
    )

    await check_repo(repo=repo, client=client)
    scm_provider = SCMProvider.from_repo(repo=repo, name=SCMProvider.generate_name(workflow.workflow_id))
    background_tasks.add_task(
        upload_scm_file,
        scm=SCM(providers=[scm_provider]),
        scm_file_id=workflow.workflow_id,
    )
    await CRUDWorkflow.update_credentials(workflow.workflow_id, token=credentials.token, db=db)


@router.delete("", status_code=status.HTTP_204_NO_CONTENT, summary="Delete the credentials of a workflow")
@start_as_current_span_async("api_workflow_credentials_delete", tracer=tracer)
async def delete_workflow_credentials(
    background_tasks: BackgroundTasks,
    workflow: CurrentWorkflow,
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    s3: S3Service,
) -> None:
    """
    Delete the credentials for the repository of a workflow.\n
    Permission `workflow:delete` required.
    \f
    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user.  Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.

    Returns
    -------
    workflow : app.schemas.workflow.WorkflowOut
        Workflow with existing ID
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    rbac_operation = "delete" if workflow.developer_id == current_user.uid else "delete_any"
    await authorization(rbac_operation)
    repo = build_repository(workflow.repository_url, workflow.versions[0].git_commit_hash)
    if isinstance(repo, GitHubRepository):
        with tracer.start_as_current_span("s3_delete_workflow_execution_parameters"):
            s3.Bucket(settings.s3.params_bucket).Object(SCM.generate_filename(workflow.workflow_id)).delete()
    else:
        scm_provider = SCMProvider.from_repo(repo=repo, name=SCMProvider.generate_name(workflow.workflow_id))
        background_tasks.add_task(
            upload_scm_file,
            scm=SCM(providers=[scm_provider]),
            scm_file_id=workflow.workflow_id,
        )
    await CRUDWorkflow.update_credentials(db=db, workflow_id=workflow.workflow_id, token=None)
