from typing import Annotated, Any, Awaitable, Callable
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, Path, status
from opentelemetry import trace

from app.api.dependencies import AuthorizationDependency, DBSession
from app.crud import CRUDWorkflowMode
from app.schemas.workflow_mode import WorkflowModeOut
from app.utils.otlp import start_as_current_span_async

router = APIRouter(prefix="/workflow_modes", tags=["Workflow Mode"])
workflow_authorization = AuthorizationDependency(resource="workflow")

Authorization = Annotated[Callable[[str], Awaitable[Any]], Depends(workflow_authorization)]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("/{mode_id}", status_code=status.HTTP_200_OK, summary="Get workflow mode")
@start_as_current_span_async("api_workflow_mode_get", tracer=tracer)
async def get_workflow_mode(
    db: DBSession,
    authorization: Authorization,
    mode_id: Annotated[
        UUID,
        Path(
            description="ID of a workflow mode",
        ),
    ],
) -> WorkflowModeOut:
    """
    Get a workflow mode\n
    Permission `workflow:read` required
    \f
    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    mode_id : uuid.UUID
        ID of workflow mode. Path parameter.

    Returns
    -------
    mode : app.schemas.workflow_mode.WorkflowModeOut
    """
    trace.get_current_span().set_attribute("workflow_mode_id", str(mode_id))
    await authorization("read")
    mode = await CRUDWorkflowMode.get(db=db, mode_id=mode_id)
    if mode is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=f"Workflow mode with ID '{mode_id}' not found"
        )
    return WorkflowModeOut.model_validate(mode)
