import json
from tempfile import SpooledTemporaryFile
from typing import Annotated, Any, Awaitable, Callable
from uuid import UUID

import jsonschema
from botocore.exceptions import ClientError
from clowmdb.models import WorkflowExecution, WorkflowVersion
from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException, Query, status
from opentelemetry import trace
from pydantic.json_schema import SkipJsonSchema

from app.api.background import (
    cancel_slurm_job,
    delete_s3_obj,
    download_file_to_bucket,
    start_workflow_execution,
    upload_scm_file,
)
from app.api.dependencies import (
    AuthorizationDependency,
    CurrentUser,
    DBSession,
    HTTPClient,
    S3Service,
    get_current_workflow_execution,
)
from app.api.utils import check_active_workflow_execution_limit, check_repo, check_used_resources
from app.core.config import settings
from app.crud import CRUDWorkflowExecution, CRUDWorkflowVersion
from app.git_repository import GitHubRepository, build_repository
from app.schemas.workflow_execution import DevWorkflowExecutionIn, WorkflowExecutionIn, WorkflowExecutionOut
from app.scm import SCM, SCMProvider
from app.utils.otlp import start_as_current_span_async

router = APIRouter(prefix="/workflow_executions", tags=["Workflow Execution"])
workflow_authorization = AuthorizationDependency(resource="workflow_execution")

Authorization = Annotated[Callable[[str], Awaitable[Any]], Depends(workflow_authorization)]
CurrentWorkflowExecution = Annotated[WorkflowExecution, Depends(get_current_workflow_execution)]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.post(
    "", status_code=status.HTTP_201_CREATED, summary="Start a new workflow execution", response_model_exclude_none=True
)
@start_as_current_span_async("api_workflow_execution_start", tracer=tracer)
async def start_workflow(
    background_tasks: BackgroundTasks,
    workflow_execution_in: WorkflowExecutionIn,
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    s3: S3Service,
    client: HTTPClient,
) -> WorkflowExecutionOut:
    """
    Start a new workflow execution. Workflow versions wit status `DEPRECATED` or `DENIED` can't be started.\n
    Permission `workflow_execution:start` required if workflow versions status is `PUBLISHED`,
    otherwise `workflow_execution:start_unpublished` required.
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow_execution_in : app.schemas.workflow_executionWorkflowExecutionIn
        Meta-data and parameters for the workflow to start. HTTP Body.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.
    client : httpx.AsyncClient
        Http client with an open connection. Dependency Injection.

    Returns
    -------
    execution : clowmdb.models.WorkflowExecution
        Created workflow execution from the database
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("workflow_version_id", workflow_execution_in.workflow_version_id)
    # Check if Workflow version exists
    workflow_version = await CRUDWorkflowVersion.get(
        workflow_execution_in.workflow_version_id, populate_workflow=True, db=db
    )
    if workflow_version is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Workflow version with git commit hash {workflow_execution_in.workflow_version_id} not found",
        )
    current_span.set_attribute("workflow_id", str(workflow_version.workflow_id))
    # Check authorization
    rbac_operation = "start" if workflow_version.status == WorkflowVersion.Status.PUBLISHED else "start_unpublished"
    await authorization(rbac_operation)

    # Check active workflow execution limit
    await check_active_workflow_execution_limit(db, current_user.uid)

    if len(workflow_version.workflow_modes) > 0 and workflow_execution_in.mode_id is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"A workflow mode needs to be specified for the workflow version '{workflow_execution_in.workflow_version_id}'",  # noqa: E501
        )

    # If a workflow mode is specified, check that the mode is associated with the workflow version
    workflow_mode = None
    if workflow_execution_in.mode_id is not None:
        current_span.set_attribute("workflow_mode_id", str(workflow_execution_in.mode_id))
        workflow_mode = next(
            (mode for mode in workflow_version.workflow_modes if mode.mode_id == workflow_execution_in.mode_id), None
        )
        if workflow_mode is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Workflow mode '{workflow_execution_in.mode_id}' does not exist on version '{workflow_execution_in.workflow_version_id}'",  # noqa: E501
            )

    # Check that the version can be used for execution
    if workflow_version.status not in [WorkflowVersion.Status.PUBLISHED, WorkflowVersion.Status.CREATED]:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"Workflow version with status {workflow_version.status} can't be started.",
        )

    # Validate schema with saved schema in bucket
    schema_name = (
        f"{workflow_execution_in.workflow_version_id}.json"
        if workflow_execution_in.mode_id is None
        else f"{workflow_execution_in.workflow_version_id}-{workflow_execution_in.mode_id.hex}.json"
    )
    with SpooledTemporaryFile(max_size=512000) as f:
        try:
            with tracer.start_as_current_span("s3_download_workflow_parameter_schema"):
                s3.Bucket(settings.s3.workflow_bucket).Object(schema_name).download_fileobj(f)
        except ClientError as e:  # if schema is not in bucket, download it from repo and cache it again in bucket
            f.seek(0)
            current_span.record_exception(e)
            repo = build_repository(
                url=workflow_version.workflow.repository_url,
                git_commit_hash=workflow_version.git_commit_hash,
                token=workflow_version.workflow.credentials_token,
            )
            background_tasks.add_task(
                download_file_to_bucket,
                repo=repo,
                filepath="nextflow_schema.json",
                bucket_name=settings.s3.workflow_bucket,
                key=f"{workflow_version.git_commit_hash}.json",
            )
            await repo.download_file(filepath="nextflow_schema.json", file_handle=f, client=client)
        f.seek(0)
        nextflow_schema = json.load(f)
    try:
        jsonschema.validate(workflow_execution_in.parameters, nextflow_schema)
    except jsonschema.exceptions.ValidationError as e:  # pragma: no cover
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=f"Nextflow Parameter validation error: {e.message}"
        )

    await check_used_resources(workflow_execution_in.parameters, db=db, background_tasks=background_tasks)

    # Create execution in database
    execution = await CRUDWorkflowExecution.create(db=db, execution=workflow_execution_in, executor_id=current_user.uid)
    # Start workflow execution
    background_tasks.add_task(
        start_workflow_execution,
        execution=execution,
        parameters=workflow_execution_in.parameters,
        git_repo=build_repository(
            url=workflow_version.workflow.repository_url, git_commit_hash=workflow_version.git_commit_hash
        ),
        scm_file_id=workflow_version.workflow_id,
        workflow_entrypoint=workflow_mode.entrypoint if workflow_mode is not None else None,
    )

    current_span.set_attribute("execution_id", str(execution.execution_id))
    return WorkflowExecutionOut.from_db_model(execution, workflow_id=workflow_version.workflow_id)


@router.post(
    "/arbitrary",
    status_code=status.HTTP_201_CREATED,
    summary="Start a workflow execution with arbitrary git repository",
    include_in_schema=settings.dev_system,
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_workflow_execution_start_arbitrary", tracer=tracer)
async def start_arbitrary_workflow(
    background_tasks: BackgroundTasks,
    workflow_execution_in: DevWorkflowExecutionIn,
    db: DBSession,
    current_user: CurrentUser,
    client: HTTPClient,
    authorization: Annotated[Callable[[str], Awaitable[Any]], Depends(AuthorizationDependency(resource="workflow"))],
) -> WorkflowExecutionOut:
    """
    Start a new workflow execution from an arbitrary git repository.\n
    Permission `workflow:create` required.\n
    For private Gitlab repositories, a Project Access Token with the role Reporter and scope `read_api` is needed.\n
    For private GitHub repositories, a Personal Access Token (classic) with scope `repo` is needed.
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow_execution_in : app.schemas.workflow_executionWorkflowExecutionIn
        Meta-data and parameters for the workflow to start. HTTP Body.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    client : httpx.AsyncClient
        Http client with an open connection. Dependency Injection.

    Returns
    -------
    execution : clowmdb.models.WorkflowExecution
        Created workflow execution from the database
    """
    if not settings.dev_system:  # pragma: no cover
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Not available")
    current_span = trace.get_current_span()
    current_span.set_attributes(
        {
            "repository_url": str(workflow_execution_in.repository_url),
            "git_commit_hash": workflow_execution_in.git_commit_hash,
        }
    )
    if workflow_execution_in.token is not None:
        current_span.set_attribute("private_repository", True)
    if workflow_execution_in.mode is not None:
        current_span.set_attributes(
            {
                "workflow_entrypoint": workflow_execution_in.mode.entrypoint,
                "workflow_schema_path": workflow_execution_in.mode.schema_path,
            }
        )

    await authorization("create")
    await check_active_workflow_execution_limit(db, current_user.uid)

    try:
        # Build a git repository object based on the repository url
        repo = build_repository(
            workflow_execution_in.repository_url,
            workflow_execution_in.git_commit_hash,
            token=workflow_execution_in.token,
        )
    except NotImplementedError:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Supplied Git Repository is not supported")

    await check_repo(
        repo=repo, client=client, modes=[workflow_execution_in.mode] if workflow_execution_in.mode is not None else None
    )

    # Validate schema with saved schema in bucket
    schema_name = (
        "nextflow_schema.json" if workflow_execution_in.mode is None else workflow_execution_in.mode.schema_path
    )
    with SpooledTemporaryFile(max_size=512000) as f:
        await repo.download_file(schema_name, client=client, file_handle=f)
        f.seek(0)
        nextflow_schema = json.load(f)

    try:
        jsonschema.validate(workflow_execution_in.parameters, nextflow_schema)
    except jsonschema.exceptions.ValidationError as e:  # pragma: no cover
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=f"Nextflow Parameter validation error: {e.message}"
        )

    await check_used_resources(workflow_execution_in.parameters, db=db, background_tasks=background_tasks)

    execution_note = f"Repository URL: {workflow_execution_in.repository_url}\nGit Commit Hash{workflow_execution_in.git_commit_hash}"  # noqa: E501
    if workflow_execution_in.mode is not None:
        execution_note += f"\nEntrypoint {workflow_execution_in.mode.entrypoint}"
    execution = await CRUDWorkflowExecution.create(
        db=db,
        execution=workflow_execution_in,
        executor_id=current_user.uid,
        notes=execution_note,
    )

    scm_provider = SCMProvider.from_repo(repo=repo, name=SCMProvider.generate_name(execution.execution_id))
    if repo.token is not None or not isinstance(repo, GitHubRepository):
        background_tasks.add_task(upload_scm_file, scm=SCM([scm_provider]), scm_file_id=execution.execution_id)

    background_tasks.add_task(
        start_workflow_execution,
        execution=execution,
        parameters=workflow_execution_in.parameters,
        git_repo=repo,
        scm_file_id=execution.execution_id,
        workflow_entrypoint=workflow_execution_in.mode.entrypoint if workflow_execution_in.mode is not None else None,
    )
    current_span.set_attribute("execution_id", str(execution.execution_id))
    return WorkflowExecutionOut.from_db_model(execution)


@router.get("", status_code=status.HTTP_200_OK, summary="Get all workflow executions", response_model_exclude_none=True)
@start_as_current_span_async("api_workflow_execution_list", tracer=tracer)
async def list_workflow_executions(
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    executor_id: Annotated[
        UUID | SkipJsonSchema[None],
        Query(
            description="Filter for workflow executions by a user. If none, Permission `workflow_execution:read_any` required.",  # noqa: E501
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
    execution_status: Annotated[
        list[WorkflowExecution.WorkflowExecutionStatus] | SkipJsonSchema[None],
        Query(description="Filter for status of workflow execution"),
    ] = None,
    workflow_version_id: Annotated[
        str | SkipJsonSchema[None],
        Query(
            description="Filter for workflow version",
            examples=["ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
            pattern=r"^[0-9a-f]{40}$",
        ),
    ] = None,
) -> list[WorkflowExecutionOut]:
    """
    Get all workflow executions.\n
    Permission `workflow_execution:list` required, if 'user_id' is the same as the current user,
    otherwise `workflow_execution:list_all` required.
    \f
    Parameters
    ----------
    executor_id : str | None, default None
        Filter for workflow executions by a user. Query Parameter.
    execution_status : list[clowmdb.models.WorkflowExecution.WorkflowExecutionStatus] | None, default None
        Filter for status of workflow execution. Query Parameter.
    workflow_version_id : str | None, default None
        Filter for workflow version, Query Parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    executions : list[clowmdb.models.WorkflowExecution]
        List of filtered workflow executions.
    """
    current_span = trace.get_current_span()
    if executor_id is not None:  # pragma: no cover
        current_span.set_attribute("user_id", str(executor_id))
    if execution_status is not None and len(execution_status) > 0:  # pragma: no cover
        current_span.set_attribute("execution_status", [stat.name for stat in execution_status])
    if workflow_version_id is not None:  # pragma: no cover
        current_span.set_attribute("workflow_version_id", workflow_version_id)

    rbac_operation = "list" if executor_id is not None and executor_id == current_user.uid else "list_all"
    await authorization(rbac_operation)
    executions = await CRUDWorkflowExecution.list(
        db=db, executor_id=executor_id, workflow_version_id=workflow_version_id, status_list=execution_status
    )
    return [
        WorkflowExecutionOut.from_db_model(
            execution, execution.workflow_version.workflow_id if execution.workflow_version is not None else None
        )
        for execution in executions
    ]


@router.get(
    "/{eid}", status_code=status.HTTP_200_OK, summary="Get a workflow execution", response_model_exclude_none=True
)
@start_as_current_span_async("api_workflow_execution_get", tracer=tracer)
async def get_workflow_execution(
    workflow_execution: CurrentWorkflowExecution,
    current_user: CurrentUser,
    authorization: Authorization,
) -> WorkflowExecutionOut:
    """
    Get a specific workflow execution.\n
    Permission `workflow_execution:read` required if the current user started the workflow execution,
    otherwise `workflow_execution:read_any` required.
    \f
    Parameters
    ----------
    workflow_execution : clowmdb.models.WorkflowExecution
        Workflow execution with given ID. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    execution : clowmdb.models.WorkflowExecution
        Workflow execution with the given ID.
    """
    trace.get_current_span().set_attribute("execution_id", str(workflow_execution.execution_id))
    rbac_operation = "read" if workflow_execution.executor_id == current_user.uid else "read_any"
    await authorization(rbac_operation)
    return WorkflowExecutionOut.from_db_model(
        workflow_execution,
        workflow_execution.workflow_version.workflow_id if workflow_execution.workflow_version is not None else None,
    )


@router.get("/{eid}/params", status_code=status.HTTP_200_OK, summary="Get the parameters of a workflow execution")
@start_as_current_span_async("api_workflow_execution_params_get", tracer=tracer)
async def get_workflow_execution_params(
    workflow_execution: CurrentWorkflowExecution,
    current_user: CurrentUser,
    authorization: Authorization,
    s3: S3Service,
) -> dict[str, Any]:
    """
    Get the parameters of a specific workflow execution.\n
    Permission `workflow_execution:read` required if the current user started the workflow execution,
    otherwise `workflow_execution:read_any` required.
    \f
    Parameters
    ----------
    workflow_execution : clowmdb.models.WorkflowExecution
        Workflow execution with given ID. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    s3 : boto3_type_annotations.s3.ServiceResource
        S3 Service to perform operations on buckets in Ceph. Dependency Injection.

    Returns
    -------
    execution : clowmdb.models.WorkflowExecution
        Workflow execution with the given id.
    """
    trace.get_current_span().set_attribute("execution_id", str(workflow_execution.execution_id))
    rbac_operation = "read" if workflow_execution.executor_id == current_user.uid else "read_any"
    await authorization(rbac_operation)
    params_file_name = f"params-{workflow_execution.execution_id.hex}.json"
    with SpooledTemporaryFile(max_size=512000) as f:
        s3.Bucket(name=settings.s3.params_bucket).Object(key=params_file_name).download_fileobj(f)
        f.seek(0)
        return json.load(f)


@router.delete("/{eid}", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a workflow execution")
@start_as_current_span_async("api_workflow_execution_delete", tracer=tracer)
async def delete_workflow_execution(
    background_tasks: BackgroundTasks,
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    workflow_execution: CurrentWorkflowExecution,
) -> None:
    """
    Delete a specific workflow execution.\n
    Permission `workflow_execution:delete` required if the current user started the workflow execution,
    otherwise `workflow_execution:delete_any` required.
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow_execution : clowmdb.models.WorkflowExecution
        Workflow execution with given ID. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    """
    trace.get_current_span().set_attribute("execution_id", str(workflow_execution.execution_id))
    rbac_operation = "delete" if workflow_execution.executor_id == current_user.uid else "delete_any"
    await authorization(rbac_operation)
    if workflow_execution.status in [
        WorkflowExecution.WorkflowExecutionStatus.PENDING,
        WorkflowExecution.WorkflowExecutionStatus.SCHEDULED,
        WorkflowExecution.WorkflowExecutionStatus.RUNNING,
    ]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Cannot delete workflow execution that is not finished."
        )
    background_tasks.add_task(
        delete_s3_obj, bucket_name=settings.s3.params_bucket, key=f"params-{workflow_execution.execution_id.hex}.json"
    )
    await CRUDWorkflowExecution.delete(workflow_execution.execution_id, db=db)


@router.post("/{eid}/cancel", status_code=status.HTTP_204_NO_CONTENT, summary="Cancel a workflow execution")
@start_as_current_span_async("api_workflow_execution_cancel", tracer=tracer)
async def cancel_workflow_execution(
    background_tasks: BackgroundTasks,
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    workflow_execution: CurrentWorkflowExecution,
) -> None:
    """
    Cancel a running workflow execution.\n
    Permission `workflow_execution:cancel` required if the current user started the workflow execution,
    otherwise `workflow_execution:cancel_any` required.
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow_execution : clowmdb.models.WorkflowExecution
        Workflow execution with given ID. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    """
    trace.get_current_span().set_attribute("execution_id", str(workflow_execution.execution_id))
    rbac_operation = "cancel" if workflow_execution.executor_id == current_user.uid else "cancel_any"
    await authorization(rbac_operation)
    if workflow_execution.status not in [
        WorkflowExecution.WorkflowExecutionStatus.PENDING,
        WorkflowExecution.WorkflowExecutionStatus.SCHEDULED,
        WorkflowExecution.WorkflowExecutionStatus.RUNNING,
    ]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Cannot cancel workflow execution that is finished."
        )
    if workflow_execution.slurm_job_id >= 0:
        background_tasks.add_task(cancel_slurm_job, job_id=workflow_execution.slurm_job_id)
    await CRUDWorkflowExecution.set_error(workflow_execution.execution_id, db=db)
