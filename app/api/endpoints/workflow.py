from datetime import date
from typing import Annotated, Any, Awaitable, Callable
from uuid import UUID

from clowmdb.models import Workflow, WorkflowMode, WorkflowVersion
from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException, Query, Response, status
from opentelemetry import trace
from pydantic.json_schema import SkipJsonSchema

from app.api.background import (
    delete_s3_obj,
    download_file_to_bucket,
    send_new_workflow_email,
    send_review_request_email,
    upload_scm_file,
)
from app.api.dependencies import AuthorizationDependency, CurrentUser, CurrentWorkflow, DBSession, HTTPClient
from app.api.utils import check_repo
from app.core.config import settings
from app.crud import CRUDWorkflow, CRUDWorkflowMode, CRUDWorkflowVersion
from app.git_repository import GitHubRepository, build_repository
from app.schemas.workflow import WorkflowIn, WorkflowOut, WorkflowStatistic, WorkflowUpdate
from app.schemas.workflow_execution import AnonymizedWorkflowExecution
from app.schemas.workflow_version import WorkflowVersion as WorkflowVersionSchema
from app.scm import SCM, SCMProvider
from app.utils.otlp import start_as_current_span_async

router = APIRouter(prefix="/workflows", tags=["Workflow"])
workflow_authorization = AuthorizationDependency(resource="workflow")

Authorization = Annotated[Callable[[str], Awaitable[Any]], Depends(workflow_authorization)]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@router.get("", status_code=status.HTTP_200_OK, summary="List workflows", response_model_exclude_none=True)
@start_as_current_span_async("api_workflow_list", tracer=tracer)
async def list_workflows(
    db: DBSession,
    authorization: Authorization,
    current_user: CurrentUser,
    name_substring: Annotated[
        str | SkipJsonSchema[None],
        Query(
            min_length=3,
            max_length=30,
            description="Filter workflows by a substring in their name.",
        ),
    ] = None,
    version_status: Annotated[
        list[WorkflowVersion.Status] | SkipJsonSchema[None],
        Query(
            description=f"Which versions of the workflow to include in the response. Permission `workflow:list_filter` required, unless `developer_id` is provided and current user is developer, then only permission `workflow:list` required. Default `{WorkflowVersion.Status.PUBLISHED.name}` and `{WorkflowVersion.Status.DEPRECATED.name}`.",  # noqa: E501
        ),
    ] = None,
    developer_id: Annotated[
        UUID | SkipJsonSchema[None],
        Query(
            description="Filter for workflow by developer. If current user is the developer, permission `workflow:list` required, otherwise `workflow:list_filter`.",  # noqa: E501
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
) -> list[WorkflowOut]:
    """
    List all workflows.\n
    Permission `workflow:list` required.
    \f
    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    developer_id : str | None, default None
        Filter workflows by a developer. Query Parameter.
    name_substring : string | None, default None
        Filter workflows by a substring in their name. Query Parameter.
    version_status : list[clowmdb.models.WorkflowVersion.Status] | None, default None
        Status of Workflow versions to filter for to fetch. Query Parameter.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency injection.

    Returns
    -------
    workflows : list[app.schemas.workflow.WorkflowOut]
        Workflows in the system
    """
    current_span = trace.get_current_span()
    if developer_id is not None:  # pragma: no cover
        current_span.set_attribute("developer_id", str(developer_id))
    if name_substring is not None:  # pragma: no cover
        current_span.set_attribute("name_substring", name_substring)
    if version_status is not None and len(version_status) > 0:  # pragma: no cover
        current_span.set_attribute("version_status", [stat.name for stat in version_status])
    rbac_operation = "list"
    if developer_id is not None and current_user.uid != developer_id:
        rbac_operation = "list_filter"
    elif version_status is not None and developer_id is None:
        rbac_operation = "list_filter"

    await authorization(rbac_operation)
    workflows: list[Workflow] = await CRUDWorkflow.list_workflows(
        db=db,
        name_substring=name_substring,
        developer_id=developer_id,
        version_status=(
            [WorkflowVersion.Status.PUBLISHED, WorkflowVersion.Status.DEPRECATED]
            if version_status is None
            else version_status
        ),
    )
    return [WorkflowOut.from_db_workflow(workflow, versions=workflow.versions) for workflow in workflows]


@router.post("", status_code=status.HTTP_201_CREATED, summary="Create a new workflow")
@start_as_current_span_async("api_workflow_create", tracer=tracer)
async def create_workflow(
    background_tasks: BackgroundTasks,
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    client: HTTPClient,
    workflow: WorkflowIn,
) -> WorkflowOut:
    """
    Create a new workflow.\n
    Permission `workflow:create` required.\n
    For private Gitlab repositories, a Project Access Token with the role Reporter and scope `read_api` is needed.\n
    For private GitHub repositories, a Personal Access Token (classic) with scope `repo` is needed.
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow : app.schemas.workflow.WorkflowIn
        Data about the new Workflow. HTML Body.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    client : httpx.AsyncClient
        Http client with an open connection. Dependency Injection.

    Returns
    -------
    workflow : app.schemas.workflow.WorkflowOut
        The newly created workflow
    """
    await authorization("create")
    # Check if name is workflow name is already taken
    if await CRUDWorkflow.get_by_name(db=db, workflow_name=workflow.name) is not None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=f"Workflow with name '{workflow.name}' already exists"
        )
    # Check if git commit is already used
    elif await CRUDWorkflowVersion.get(workflow.git_commit_hash, db=db) is not None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Workflow with git_commit_hash'{workflow.git_commit_hash}' already exists",
        )

    try:
        # Build a git repository object based on the repository url
        repo = build_repository(workflow.repository_url, workflow.git_commit_hash, token=workflow.token)
    except NotImplementedError:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Supplied Git Repository is not supported")

    # Check if the relevant files are present in the repository
    await check_repo(repo=repo, client=client, modes=workflow.modes)

    # Create the workflow in the DB
    workflow_db = await CRUDWorkflow.create(workflow, current_user.uid, db=db)

    # If it is a private repository, create an SCM file and upload it to the params bucket
    scm_provider = SCMProvider.from_repo(repo, name=SCMProvider.generate_name(workflow_db.workflow_id))
    if repo.token is not None or not isinstance(repo, GitHubRepository):
        background_tasks.add_task(upload_scm_file, scm=SCM([scm_provider]), scm_file_id=workflow_db.workflow_id)

    # If there are workflow modes with alternative parameter schemas, cache them in the WORKFLOW Bucket
    if len(workflow.modes) > 0:
        for mode_db in workflow_db.versions[0].workflow_modes:
            background_tasks.add_task(
                download_file_to_bucket,
                repo=repo,
                filepath=mode_db.schema_path,
                bucket_name=settings.s3.workflow_bucket,
                key=f"{workflow.git_commit_hash}-{mode_db.mode_id.hex}.json",
            )
    else:
        # Cache the parameter schema in the WORKFLOW Bucket
        background_tasks.add_task(
            download_file_to_bucket,
            repo=repo,
            filepath="nextflow_schema.json",
            bucket_name=settings.s3.workflow_bucket,
            key=f"{workflow.git_commit_hash}.json",
        )
    trace.get_current_span().set_attribute("workflow_id", str(workflow_db.workflow_id))
    background_tasks.add_task(send_new_workflow_email, workflow=workflow_db, version=workflow_db.versions[0])
    return WorkflowOut.from_db_workflow(await CRUDWorkflow.get(workflow_db.workflow_id, db=db))


@router.get(
    "/developer_statistics",
    status_code=status.HTTP_200_OK,
    summary="Get anonymized workflow execution",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_workflow_get_developer_statistics", tracer=tracer)
async def get_developer_workflow_statistics(
    db: DBSession,
    authorization: Authorization,
    response: Response,
    current_user: CurrentUser,
    developer_id: Annotated[
        UUID | SkipJsonSchema[None],
        Query(
            description="Filter by the developer of the workflows",
            examples=["1d3387f3-95c0-4813-8767-2cad87faeebf"],
        ),
    ] = None,
    workflow_ids: Annotated[
        list[UUID] | SkipJsonSchema[None], Query(description="Filter by workflow IDs", alias="workflow_id")
    ] = None,
    start: Annotated[
        date | SkipJsonSchema[None], Query(description="Filter by workflow executions after this date")
    ] = None,
    end: Annotated[
        date | SkipJsonSchema[None], Query(description="Filter by workflow executions before this date")
    ] = None,
) -> list[AnonymizedWorkflowExecution]:
    """
    Get the workflow executions with meta information and anonymized user IDs.\n
    Permission `workflow:read_statistics` required if the `developer_id` is the same as the uid of the current user,
    other `workflow:read_statistics_any`.
    \f
    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    response : fastapi.Response
        Temporary Response object. Dependency Injection.
    current_user : clowmdb.models.User
        Current user.  Dependency Injection.
    developer_id : str | None, default None
        Filter for workflows developed by a specific user. Query Parameter.
    workflow_ids: list[uuid.UUID] | None, default None
        Filter by workflow IDs. Query Parameter.
    start : datetime.date | None, default None
        Filter by executions that started after the specified date. Query Parameter.
    end : datetime.date | None, default None
        Filter by executions that started before the specified date. Query Parameter.

    Returns
    -------
    statistics : list[app.schema.workflow.AnonymizedWorkflowExecution]
        List of raw datapoints for analysis.
    """
    span = trace.get_current_span()
    if developer_id:  # pragma: no cover
        span.set_attribute("developer_id", str(developer_id))
    if workflow_ids:  # pragma: no cover
        span.set_attribute("workflow_ids", [str(wid) for wid in workflow_ids])
    if start:  # pragma: no cover
        span.set_attribute("start_day", start.isoformat())
    if end:  # pragma: no cover
        span.set_attribute("end_day", end.isoformat())
    await authorization("read_statistics" if current_user.uid == developer_id else "read_statistics_any")
    # Instruct client to cache response for 1 hour
    response.headers["Cache-Control"] = "max-age=3600"
    return await CRUDWorkflow.developer_statistics(
        db=db, developer_id=developer_id, workflow_ids=workflow_ids, start=start, end=end
    )


@router.get("/{wid}", status_code=status.HTTP_200_OK, summary="Get a workflow", response_model_exclude_none=True)
@start_as_current_span_async("api_workflow_get", tracer=tracer)
async def get_workflow(
    workflow: CurrentWorkflow,
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    version_status: Annotated[
        list[WorkflowVersion.Status] | SkipJsonSchema[None],
        Query(
            description=f"Which versions of the workflow to include in the response. Permission `workflow:read_any` required if you are not the developer of this workflow. Default `{WorkflowVersion.Status.PUBLISHED.name}` and `{WorkflowVersion.Status.DEPRECATED.name}`",  # noqa: E501
        ),
    ] = None,
) -> WorkflowOut:
    """
    Get a specific workflow.\n
    Permission `workflow:read` required.
    \f
    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    version_status : list[clowmdb.models.WorkflowVersion.Status] | None, default None
        Status of Workflow versions to filter for to fetch. Query Parameter
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user.  Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    workflow : app.schemas.workflow.WorkflowOut
        Workflow with existing ID
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("workflow_id", str(workflow.workflow_id))
    if version_status is not None and len(version_status) > 0:  # pragma: no cover
        current_span.set_attribute("version_status", [stat.name for stat in version_status])
    rbac_operation = "read_any" if workflow.developer_id != current_user.uid and version_status is not None else "read"
    await authorization(rbac_operation)
    version_stat = (
        [WorkflowVersion.Status.PUBLISHED, WorkflowVersion.Status.DEPRECATED]
        if version_status is None
        else version_status
    )
    versions = await CRUDWorkflowVersion.list_workflow_versions(workflow.workflow_id, version_stat, db=db)
    return WorkflowOut.from_db_workflow(workflow, versions)


@router.get("/{wid}/statistics", status_code=status.HTTP_200_OK, summary="Get statistics for a workflow")
@start_as_current_span_async("api_workflow_get_statistics", tracer=tracer)
async def get_workflow_statistics(
    workflow: CurrentWorkflow, db: DBSession, authorization: Authorization, response: Response
) -> list[WorkflowStatistic]:
    """
    Get the number of started workflow per day.\n
    Permission `workflow:read` required.
    \f
    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    response : fastapi.Response
        Temporary Response object. Dependency Injection.

    Returns
    -------
    statistics : list[app.schema.workflow.WorkflowStatistic]
        List of datapoints aggregated by day.
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    await authorization("read")
    # Instruct client to cache response for 24 hour
    response.headers["Cache-Control"] = "max-age=86400"
    return await CRUDWorkflow.statistics(workflow.workflow_id, db=db)


@router.delete("/{wid}", status_code=status.HTTP_204_NO_CONTENT, summary="Delete a workflow")
@start_as_current_span_async("api_workflow_delete", tracer=tracer)
async def delete_workflow(
    background_tasks: BackgroundTasks,
    workflow: CurrentWorkflow,
    db: DBSession,
    authorization: Authorization,
    current_user: CurrentUser,
) -> None:
    """
    Delete a workflow.\n
    Permission `workflow:delete` required.
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency Injection.
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    rbac_operation = "delete" if workflow.developer_id == current_user.uid else "delete_any"
    await authorization(rbac_operation)
    versions = await CRUDWorkflowVersion.list_workflow_versions(workflow.workflow_id, db=db)
    # Delete SCM file for private repositories
    background_tasks.add_task(
        delete_s3_obj, bucket_name=settings.s3.params_bucket, key=SCM.generate_filename(workflow.workflow_id)
    )
    # Delete files in buckets
    mode_ids: set[UUID] = set()
    for version in versions:
        # Delete parameter schema of every mode
        if version.workflow_modes is not None and len(version.workflow_modes) > 0:
            for mode in version.workflow_modes:
                mode_ids.add(mode.mode_id)
                background_tasks.add_task(
                    delete_s3_obj,
                    bucket_name=settings.s3.workflow_bucket,
                    key=f"{version.git_commit_hash}-{mode.mode_id.hex}.json",
                )
        else:
            # Delete standard parameter schema of workflow
            background_tasks.add_task(
                delete_s3_obj, bucket_name=settings.s3.workflow_bucket, key=f"{version.git_commit_hash}.json"
            )
        # Delete icons of workflow version
        if version.icon_slug is not None:
            background_tasks.add_task(delete_s3_obj, bucket_name=settings.s3.icon_bucket, key=version.icon_slug)
    await CRUDWorkflow.delete(workflow.workflow_id, db=db)
    if len(mode_ids) > 0:
        await CRUDWorkflowMode.delete(mode_ids, db=db)


@router.post(
    "/{wid}/update", status_code=status.HTTP_201_CREATED, summary="Update a workflow", response_model_exclude_none=True
)
@start_as_current_span_async("api_workflow_update", tracer=tracer)
async def update_workflow(
    background_tasks: BackgroundTasks,
    workflow: CurrentWorkflow,
    client: HTTPClient,
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    version_update: WorkflowUpdate,
) -> WorkflowVersionSchema:
    """
    Create a new workflow version.\n
    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    client : httpx.AsyncClient
        Http client with an open connection. Dependency Injection.
    version_update : app.schemas.workflow
        Data about the new workflow version. HTML Body.
    Returns
    -------
    version : app.schemas.workflow_version.WorkflowVersion
        The new workflow version
    """
    trace.get_current_span().set_attribute("workflow_id", str(workflow.workflow_id))
    await authorization("update")
    if current_user.uid != workflow.developer_id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Only the developer can update his workflow")

    # Check if git commit is already used
    if await CRUDWorkflowVersion.get(version_update.git_commit_hash, db=db) is not None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Workflow Version with git_commit_hash'{version_update.git_commit_hash}' already exists",
        )
    # Get previous version
    previous_version: WorkflowVersion = await CRUDWorkflowVersion.get_latest(
        workflow.workflow_id, published=False, db=db
    )

    # Get modes of previous version
    previous_version_modes = await CRUDWorkflowMode.list_modes(previous_version.git_commit_hash, db=db)

    if len(version_update.delete_modes) > 0:
        # Check if mode to delete actually exist
        mode_ids = [mode.mode_id for mode in previous_version_modes]
        for delete_mode in version_update.delete_modes:
            if delete_mode not in mode_ids:
                raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail=f"Workflow mode {delete_mode} does not exist for the latest version {previous_version.git_commit_hash} of workflow {workflow.workflow_id}",  # noqa: E501
                )
        # Filter out modes that should be deleted in new workflow version
        previous_version_modes = [
            mode for mode in previous_version_modes if mode.mode_id not in version_update.delete_modes
        ]

    # Build a git repository object based on the repository url
    repo = build_repository(
        workflow.repository_url,
        version_update.git_commit_hash,
        token=workflow.credentials_token,
    )

    check_repo_modes = previous_version_modes.copy()
    # If there are new modes, add them to the list for file checking
    if len(version_update.append_modes) > 0:
        check_repo_modes += version_update.append_modes
    await check_repo(repo=repo, client=client, modes=check_repo_modes)

    append_modes_db: list[WorkflowMode] = []
    # Create new modes in database
    if len(version_update.append_modes) > 0:
        append_modes_db = await CRUDWorkflowMode.create(db=db, modes=version_update.append_modes)
    # Make a list with all DB modes of modes for the new workflow version
    db_modes = previous_version_modes + append_modes_db
    # Copy important files to a bucket in a background task, not relevant for successful response
    if len(db_modes) > 0:
        for mode in db_modes:
            background_tasks.add_task(
                download_file_to_bucket,
                repo=repo,
                filepath=mode.schema_path,
                bucket_name=settings.s3.workflow_bucket,
                key=f"{version_update.git_commit_hash}-{mode.mode_id.hex}.json",
            )
    else:
        background_tasks.add_task(
            download_file_to_bucket,
            filepath="nextflow_schema.json",
            repo=repo,
            bucket_name=settings.s3.workflow_bucket,
            key=f"{version_update.git_commit_hash}.json",
        )

    # Create list with mode ids that are connected to the new workflow version
    mode_ids = [mode.mode_id for mode in db_modes]
    version = await CRUDWorkflowVersion.create(
        db=db,
        git_commit_hash=version_update.git_commit_hash,
        version=version_update.version,
        workflow_id=workflow.workflow_id,
        icon_slug=previous_version.icon_slug if previous_version else None,
        previous_version=previous_version.git_commit_hash if previous_version else None,
        modes=mode_ids,
        parameter_extension=previous_version.parameter_extension if previous_version else None,
    )
    background_tasks.add_task(send_review_request_email, workflow=workflow, version=version)
    return WorkflowVersionSchema.from_db_version(version, mode_ids=mode_ids)
