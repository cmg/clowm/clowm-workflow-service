from enum import StrEnum, unique
from typing import Annotated, Any, Awaitable, Callable
from uuid import UUID

from clowmdb.models import WorkflowVersion
from fastapi import APIRouter, BackgroundTasks, Depends, File, HTTPException, Path, Query, Request, UploadFile, status
from fastapi.responses import StreamingResponse
from opentelemetry import trace
from pydantic.json_schema import SkipJsonSchema
from starlette.background import BackgroundTask

from app.api.background import delete_remote_icon, send_workflow_status_update_email
from app.api.dependencies import (
    AuthorizationDependency,
    CurrentUser,
    CurrentWorkflow,
    CurrentWorkflowVersion,
    DBSession,
    HTTPClient,
)
from app.api.utils import upload_icon
from app.core.config import settings
from app.crud import CRUDWorkflowVersion
from app.git_repository import build_repository
from app.schemas.workflow_version import IconUpdateOut, ParameterExtension
from app.schemas.workflow_version import WorkflowVersion as WorkflowVersionSchema
from app.schemas.workflow_version import WorkflowVersionStatus
from app.utils.otlp import start_as_current_span_async

router = APIRouter(prefix="/workflows/{wid}/versions", tags=["Workflow Version"])
workflow_authorization = AuthorizationDependency(resource="workflow")

Authorization = Annotated[Callable[[str], Awaitable[Any]], Depends(workflow_authorization)]

tracer = trace.get_tracer_provider().get_tracer(__name__)


@unique
class DocumentationEnum(StrEnum):
    USAGE = "usage"
    INPUT = "input"
    OUTPUT = "output"
    CHANGELOG = "changelog"
    PARAMETER_SCHEMA = "parameter_schema"
    CLOWM_INFO = "clowm_info"

    @property
    def standard_path(self) -> str:
        if self is DocumentationEnum.USAGE:
            return "README.md"
        elif self is DocumentationEnum.INPUT:
            return "docs/usage.md"
        elif self is DocumentationEnum.OUTPUT:
            return "docs/output.md"
        elif self is DocumentationEnum.CHANGELOG:
            return "CHANGELOG.md"
        elif self is DocumentationEnum.CLOWM_INFO:
            return "clowm_info.json"
        return "nextflow_schema.json"


@router.get(
    "", status_code=status.HTTP_200_OK, summary="Get all versions of a workflow", response_model_exclude_none=True
)
@start_as_current_span_async("api_workflow_version_list", tracer=tracer)
async def list_workflow_version(
    current_user: CurrentUser,
    workflow: CurrentWorkflow,
    db: DBSession,
    authorization: Authorization,
    version_status: Annotated[
        list[WorkflowVersion.Status] | SkipJsonSchema[None],
        Query(
            description=f"Which versions of the workflow to include in the response. Permission `workflow:list_filter` required if you are not the developer of this workflow. Default `{WorkflowVersion.Status.PUBLISHED.name}` and `{WorkflowVersion.Status.DEPRECATED.name}`",  # noqa: E501
        ),
    ] = None,
) -> list[WorkflowVersionSchema]:
    """
    List all versions of a Workflow.\n
    Permission `workflow:list` required.
    \f
    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    version_status : list[clowmdb.models.WorkflowVersion.Status] | None, default None
        Status of Workflow versions to filter for to fetch. Query Parameter
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    versions : [app.schemas.workflow_version.WorkflowVersion]
        All versions of the workflow
    """
    current_span = trace.get_current_span()
    current_span.set_attribute("workflow_id", str(workflow.workflow_id))
    if version_status is not None and len(version_status) > 0:
        current_span.set_attribute("version_status", [stat.name for stat in version_status])
    rbac_operation = (
        "list_filter" if workflow.developer_id != current_user.uid and version_status is not None else "list"
    )
    await authorization(rbac_operation)

    versions = await CRUDWorkflowVersion.list_workflow_versions(
        workflow.workflow_id,
        version_status=(
            version_status
            if version_status is not None
            else [WorkflowVersion.Status.PUBLISHED, WorkflowVersion.Status.DEPRECATED]
        ),
        db=db,
    )
    return [WorkflowVersionSchema.from_db_version(v, load_modes=True) for v in versions]


@router.get(
    "/{git_commit_hash}",
    status_code=status.HTTP_200_OK,
    summary="Get a workflow version",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_workflow_version_get", tracer=tracer)
async def get_workflow_version(
    workflow: CurrentWorkflow,
    db: DBSession,
    current_user: CurrentUser,
    authorization: Authorization,
    git_commit_hash: Annotated[
        str,
        Path(
            description="Git commit `git_commit_hash` of specific version or `latest`.",
            pattern=r"^([0-9a-f]{40}|latest)$",
            examples=["latest", "ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
        ),
    ],
) -> WorkflowVersionSchema:
    """
    Get a specific version of a workflow.\n
    Permission `workflow:read` required if the version is public or you are the developer of the workflow,
    otherwise `workflow:read_any`
    \f
    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    git_commit_hash: str
        Version ID
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    current_user : clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    version : app.schemas.workflow_version.WorkflowVersion
        The specified WorkflowVersion
    """
    trace.get_current_span().set_attributes(
        {"workflow_id": str(workflow.workflow_id), "workflow_version_id": git_commit_hash}
    )
    rbac_operation = "read"
    version = (
        await CRUDWorkflowVersion.get_latest(workflow.workflow_id, db=db)
        if git_commit_hash == "latest"
        else await CRUDWorkflowVersion.get(git_commit_hash, workflow_id=workflow.workflow_id, db=db)
    )
    if version is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Workflow Version with git_commit_hash '{git_commit_hash}' not found",
        )
    # Developer can read any version of his workflow, others only published and deprecated ones
    if (
        current_user.uid != workflow.developer_id
        and version.status != WorkflowVersion.Status.PUBLISHED
        and version.status != WorkflowVersion.Status.DEPRECATED
    ):
        rbac_operation = "read_any"
    await authorization(rbac_operation)
    return WorkflowVersionSchema.from_db_version(version, load_modes=True)


@router.patch(
    "/{git_commit_hash}/status",
    status_code=status.HTTP_200_OK,
    summary="Update status of workflow version",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_workflow_version_status_update", tracer=tracer)
async def update_workflow_version_status(
    background_tasks: BackgroundTasks,
    workflow: CurrentWorkflow,
    version_status: WorkflowVersionStatus,
    workflow_version: CurrentWorkflowVersion,
    db: DBSession,
    authorization: Authorization,
) -> WorkflowVersionSchema:
    """
    Update the status of a workflow version.\n
    Permission `workflow:update_status`
    \f
    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    version_status : app.schemas.workflow_version.WorkflowVersionStatus
        New Status of the workflow version. HTTP Body.
    workflow_version : clowmdb.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.

    Returns
    -------
    version : clowmdb.models.WorkflowVersion
        Version of the workflow with updated status
    """
    trace.get_current_span().set_attributes(
        {
            "workflow_id": str(workflow_version.workflow_id),
            "workflow_version_id": workflow_version.git_commit_hash,
            "version_status": version_status.status.name,
        }
    )
    await authorization("update_status")
    await CRUDWorkflowVersion.update_status(workflow_version.git_commit_hash, version_status.status, db=db)
    workflow_version.status = version_status.status
    if (
        workflow_version.status == WorkflowVersion.Status.DENIED
        or workflow_version.status == WorkflowVersion.Status.PUBLISHED
    ):
        background_tasks.add_task(send_workflow_status_update_email, workflow=workflow, version=workflow_version)
    return WorkflowVersionSchema.from_db_version(workflow_version, load_modes=True)


@router.patch(
    "/{git_commit_hash}/deprecate",
    status_code=status.HTTP_200_OK,
    summary="Deprecate a workflow version",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_workflow_version_status_update", tracer=tracer)
async def deprecate_workflow_version(
    workflow: CurrentWorkflow,
    workflow_version: CurrentWorkflowVersion,
    db: DBSession,
    authorization: Authorization,
    current_user: CurrentUser,
) -> WorkflowVersionSchema:
    """
    Deprecate a workflow version.\n
    Permission `workflow:update` required if you are the developer of the workflow,
    otherwise `workflow:read_status`
    \f
    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    workflow_version : clowmdb.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user: clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.

    Returns
    -------
    version : clowmdb.models.WorkflowVersion
        Version of the workflow with deprecated status
    """
    trace.get_current_span().set_attributes(
        {"workflow_id": str(workflow_version.workflow_id), "workflow_version_id": workflow_version.git_commit_hash}
    )
    await authorization("update_status" if current_user.uid != workflow.developer_id else "update")
    await CRUDWorkflowVersion.update_status(workflow_version.git_commit_hash, WorkflowVersion.Status.DEPRECATED, db=db)
    workflow_version.status = WorkflowVersion.Status.DEPRECATED
    return WorkflowVersionSchema.from_db_version(workflow_version, load_modes=True)


@router.patch(
    "/{git_commit_hash}/parameter-extension",
    status_code=status.HTTP_200_OK,
    summary="Update parameter extension of workflow version",
    response_model_exclude_none=True,
)
@start_as_current_span_async("api_workflow_version_update_parameter_extension", tracer=tracer)
async def update_workflow_version_parameter_extension(
    workflow: CurrentWorkflow,
    workflow_version: CurrentWorkflowVersion,
    db: DBSession,
    authorization: Authorization,
    current_user: CurrentUser,
    parameter_extension: ParameterExtension,
) -> WorkflowVersionSchema:
    """
    Update the parameter extension of a workflow version.\n

    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    workflow_version : clowmdb.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user: clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    parameter_extension : app.schemas.workflow_version.ParameterExtension
        Parameter extension specific for this CloWM instance.

    Returns
    -------
    version : clowmdb.models.WorkflowVersion
        Version of the workflow with the updated parameter extension
    """
    trace.get_current_span().set_attributes(
        {"workflow_id": str(workflow_version.workflow_id), "workflow_version_id": workflow_version.git_commit_hash}
    )
    await authorization("update")
    if current_user.uid != workflow.developer_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Only the developer can update the parameter extension"
        )
    await CRUDWorkflowVersion.update_parameter_extension(workflow_version.git_commit_hash, parameter_extension, db=db)
    workflow_version.parameter_extension = parameter_extension.model_dump()
    return WorkflowVersionSchema.from_db_version(workflow_version, load_modes=True)


@router.delete(
    "/{git_commit_hash}/parameter-extension",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete parameter extension of workflow version",
)
@start_as_current_span_async("api_workflow_version_delete_parameter_extension", tracer=tracer)
async def delete_workflow_version_parameter_extension(
    workflow: CurrentWorkflow,
    workflow_version: CurrentWorkflowVersion,
    db: DBSession,
    authorization: Authorization,
    current_user: CurrentUser,
) -> None:
    """
    Delete the parameter extension of a workflow version.\n

    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    workflow_version : clowmdb.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user: clowmdb.models.User
        Current user who will be the owner of the newly created bucket. Dependency Injection.
    """
    trace.get_current_span().set_attributes(
        {"workflow_id": str(workflow_version.workflow_id), "workflow_version_id": workflow_version.git_commit_hash}
    )
    await authorization("update")
    if current_user.uid != workflow.developer_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN, detail="Only the developer can delete the parameter extension"
        )
    await CRUDWorkflowVersion.update_parameter_extension(workflow_version.git_commit_hash, None, db=db)


@router.get(
    "/{git_commit_hash}/documentation",
    status_code=status.HTTP_200_OK,
    summary="Fetch documentation for a workflow version",
    response_class=StreamingResponse,
)
@start_as_current_span_async("api_workflow_version_get_documentation", tracer=tracer)
async def download_workflow_documentation(
    request: Request,
    workflow: CurrentWorkflow,
    workflow_version: CurrentWorkflowVersion,
    authorization: Authorization,
    client: HTTPClient,
    document: Annotated[
        DocumentationEnum, Query(description="Specify which type of documentation the client wants to fetch")
    ] = DocumentationEnum.USAGE,
    mode_id: Annotated[UUID | SkipJsonSchema[None], Query(description="Workflow Mode")] = None,
) -> StreamingResponse:
    """
    Get the documentation for a specific workflow version.
    Streams the response directly from the right git repository.\n
    Permission `workflow:read` required.
    \f
    Parameters
    ----------
    request : fastapi.Request
        Raw request object
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    workflow_version : clowmdb.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    client : httpx.AsyncClient
        HTTP Client with an open connection. Dependency Injection.
    document : DocumentationEnum, default DocumentationEnum.USAGE
        Which type of documentation the client wants to fetch. HTTP Query
    mode_id : UUID | None
        Select the workflow mode of the workflow version. HTTP Query

    Returns
    -------
    response : StreamingResponse
        Streams the requested document from the git repository directly to the client
    """
    current_span = trace.get_current_span()
    current_span.set_attributes(
        {
            "workflow_id": str(workflow_version.workflow_id),
            "workflow_version_id": workflow_version.git_commit_hash,
            "document": document.name,
            "filepath": document.standard_path,
        }
    )
    if mode_id is not None:
        current_span.set_attribute("workflow_mode_id", str(mode_id))
    await authorization("read")
    repo = build_repository(
        workflow.repository_url,
        workflow_version.git_commit_hash,
        workflow.credentials_token,
    )
    path = document.standard_path
    if mode_id is not None:
        workflow_mode = next((mode for mode in workflow_version.workflow_modes if mode.mode_id == mode_id), None)
        if workflow_mode is None:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail=f"Workflow mode with ID '{mode_id}' not found"
            )
        if document is DocumentationEnum.PARAMETER_SCHEMA:
            path = workflow_mode.schema_path

    r_headers = {}
    headers = {"cache-control": "max-age=86400"}
    if request.headers.get("accept-encoding", None) is not None:  # pragma: no cover
        r_headers["accept-encoding"] = request.headers["accept-encoding"]
    if request.headers.get("etag", None) is not None:  # pragma: no cover
        r_headers["etag"] = request.headers["etag"]
    try:
        req = client.build_request(
            method="GET",
            url=str(await repo.download_file_url(path, client)),
            headers=r_headers,
        )
    except AssertionError:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"File {path} not present in workflow repository",
            headers=headers,
        )
    with tracer.start_as_current_span(
        "git_start_stream",
        attributes={
            "repository": repo.url,
            "git_commit_hash": repo.commit,
            "file": path,
            **{"repo.request.headers." + key: val for key, val in r_headers.items()},
        },
    ) as span:
        r = await client.send(
            req,
            stream=True,
            follow_redirects=True,
            auth=repo.request_auth,
        )
        if r.headers.get("content-type", None) is not None:  # pragma: no cover
            headers["content-type"] = r.headers["content-type"]
        if r.headers.get("content-length", None) is not None:  # pragma: no cover
            headers["content-length"] = r.headers["content-length"]
        if r.headers.get("content-encoding", None) is not None:  # pragma: no cover
            headers["content-encoding"] = r.headers["content-encoding"]
        if r.headers.get("etag", None) is not None:  # pragma: no cover
            headers["etag"] = r.headers["etag"]
        span.set_attributes({"repo.response.headers." + key: val for key, val in headers.items()})
    return StreamingResponse(
        r.aiter_raw(),
        status_code=r.status_code,
        headers=headers,
        background=BackgroundTask(r.aclose),
    )


@router.post(
    "/{git_commit_hash}/icon",
    status_code=status.HTTP_201_CREATED,
    summary="Upload icon for workflow version",
)
@start_as_current_span_async("api_workflow_version_upload_icon", tracer=tracer)
async def upload_workflow_version_icon(
    workflow: CurrentWorkflow,
    background_tasks: BackgroundTasks,
    workflow_version: CurrentWorkflowVersion,
    authorization: Authorization,
    current_user: CurrentUser,
    db: DBSession,
    icon: Annotated[UploadFile, File(description="Icon for the Workflow.")],
) -> IconUpdateOut:
    """
    Upload an icon for the workflow version and returns the new icon URL.\n
    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    workflow_version : clowmdb.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    icon : fastapi.UploadFile
        New Icon for the workflow version. HTML Form.

    Returns
    -------
    icon_url : str
        URL where the icon can be downloaded
    """
    current_span = trace.get_current_span()
    current_span.set_attributes(
        {"workflow_id": str(workflow_version.workflow_id), "workflow_version_id": workflow_version.git_commit_hash}
    )
    if icon.content_type is not None:  # pragma: no cover
        current_span.set_attribute("content_type", icon.content_type)
    if icon.filename is not None:  # pragma: no cover
        current_span.set_attribute("filename", icon.filename)
    await authorization("update")
    if current_user.uid != workflow.developer_id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Only the developer can update his workflow")
    old_slug = workflow_version.icon_slug
    icon_slug = await upload_icon(background_tasks=background_tasks, icon=icon)
    current_span.set_attribute("icon_slug", icon_slug)
    await CRUDWorkflowVersion.update_icon(workflow_version.git_commit_hash, icon_slug, db=db)
    # Delete old icon if possible
    if old_slug is not None:
        background_tasks.add_task(delete_remote_icon, icon_slug=old_slug)
    return IconUpdateOut(icon_url=str(settings.s3.uri) + "/".join([settings.s3.icon_bucket, icon_slug]))


@router.delete(
    "/{git_commit_hash}/icon",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete icon of workflow version",
)
@start_as_current_span_async("api_workflow_version_delete_icon", tracer=tracer)
async def delete_workflow_version_icon(
    workflow: CurrentWorkflow,
    workflow_version: CurrentWorkflowVersion,
    background_tasks: BackgroundTasks,
    authorization: Authorization,
    current_user: CurrentUser,
    db: DBSession,
) -> None:
    """
    Delete the icon of the workflow version.\n
    Permission `workflow:update` required.
    \f
    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        Workflow with given ID. Dependency Injection.
    workflow_version : clowmdb.models.WorkflowVersion
        Workflow version with given ID. Dependency Injection.
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks. Provided by FastAPI.
    authorization : Callable[[str], Awaitable[Any]]
        Async function to ask the auth service for authorization. Dependency Injection.
    current_user : clowmdb.models.User
        Current user. Dependency Injection.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    """
    current_span = trace.get_current_span()
    current_span.set_attributes(
        {"workflow_id": str(workflow_version.workflow_id), "workflow_version_id": workflow_version.git_commit_hash}
    )
    await authorization("update")
    if current_user.uid != workflow.developer_id:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Only the developer can update his workflow")
    if workflow_version.icon_slug is not None:
        background_tasks.add_task(delete_remote_icon, icon_slug=workflow_version.icon_slug)
        await CRUDWorkflowVersion.update_icon(workflow_version.git_commit_hash, icon_slug=None, db=db)
