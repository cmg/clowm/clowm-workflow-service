import json
import re
from pathlib import Path
from typing import Any, Sequence
from uuid import UUID, uuid4

from anyio import open_file
from clowmdb.models import ResourceVersion, WorkflowExecution, WorkflowMode
from fastapi import BackgroundTasks, HTTPException, UploadFile, status
from httpx import AsyncClient
from opentelemetry import trace
from PIL import Image, UnidentifiedImageError
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.background import process_and_upload_icon, update_used_resources
from app.core.config import settings
from app.crud import CRUDResourceVersion, CRUDWorkflowExecution
from app.git_repository.abstract_repository import GitRepository
from app.schemas.workflow_mode import WorkflowModeIn

# regex to find resources in parameters of workflow execution
resource_regex = re.compile(r"/[\w\-/]*/CLDB-([a-f0-9]{32})/([a-f0-9]{32}|latest)")

tracer = trace.get_tracer_provider().get_tracer(__name__)


async def upload_icon(background_tasks: BackgroundTasks, icon: UploadFile) -> str:
    """
    Upload an icon to the icon bucket.

    Parameters
    ----------
    background_tasks : fastapi.BackgroundTasks
        Entrypoint for new BackgroundTasks.
    icon : fastapi.UploadFile
        Icon for the workflow version.

    Returns
    -------
    icon_slug : str
        Slug of the icon in the bucket
    """
    try:
        Image.open(icon.file)
        trace.get_current_span().set_attribute("uploaded_icon_size", icon.file.tell())
        icon.file.seek(0)
    except UnidentifiedImageError:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="icon needs to be an image")
    icon_slug = f"{uuid4().hex}.png"
    # Save the icon to a file to access it in a background task
    icon_file = Path(f"/tmp/{icon_slug}")
    async with await open_file(icon_file, "wb") as f:
        size = 32000
        buffer = await icon.read(size)
        while len(buffer) > 0:
            await f.write(buffer)
            buffer = await icon.read(size)

    background_tasks.add_task(process_and_upload_icon, icon_slug=icon_slug, icon_buffer_file=icon_file)
    return icon_slug


async def check_repo(
    repo: GitRepository, client: AsyncClient, modes: Sequence[WorkflowModeIn | WorkflowMode] | None = None
) -> None:
    """
    Check if the necessary files are present in the git repository. If not, raises an HTTP Exception.

    Parameters
    ----------
    repo : app.git_repository.GitRepository
        Repo to check against
    client : httpx.AsyncClient
        Http client with an open connection.
    modes : list[app.schemas.workflow_mode.WorkflowModeIn | clowmdb.models.WorkflowMode] | None
        List of different mode of the workflow with different parameter schemas.
    """
    files = ["README.md", "CHANGELOG.md", "main.nf", "docs/usage.md", "docs/output.md"]
    if modes is not None and len(modes) > 0:
        # If there are modes, add their parameter schemas to the list
        for mode in modes:
            files.append(mode.schema_path)
    else:
        # Otherwise add default parameter schemas file location
        files.append("nextflow_schema.json")
    await repo.check_files_exist(
        files=files,
        client=client,
        raise_error=True,
    )


async def check_active_workflow_execution_limit(db: AsyncSession, uid: UUID) -> None:
    """
    Check the number of active workflow executions of a usr and raise an HTTP exception if a new one would violate the
    limit of active workflow executions.

    Parameters
    ----------
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on.
    uid : str
        ID of a user.
    """
    active_executions = await CRUDWorkflowExecution.list(
        db=db, executor_id=uid, status_list=WorkflowExecution.WorkflowExecutionStatus.active_workflows()
    )
    if settings != -1 and len(active_executions) + 1 > settings.cluster.active_workflow_execution_limit:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"The active workflow execution limit per user is {settings.cluster.active_workflow_execution_limit}",
        )


async def check_used_resources(
    parameters: dict[str, Any], *, db: AsyncSession, background_tasks: BackgroundTasks
) -> None:
    with tracer.start_as_current_span("check_used_resources"):
        used_rvid: list[UUID] = []
        for match in resource_regex.finditer(json.dumps(parameters)):
            rid, rvid = match.groups()
            resource_version = await CRUDResourceVersion.get(
                resource_id=UUID(hex=rid),
                resource_version_id=UUID(hex=rvid) if rvid != "latest" else None,
                latest=rvid == "latest",
                db=db,
            )
            if resource_version is None or resource_version.status not in [
                ResourceVersion.Status.SYNCHRONIZED,
                ResourceVersion.Status.LATEST,
                ResourceVersion.Status.SETTING_LATEST,
            ]:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND,
                    detail=f"resource at path {match.group(0)} does not exist on cluster",
                )
            used_rvid.append(resource_version.resource_version_id)
    if len(used_rvid) > 0:
        background_tasks.add_task(update_used_resources, ids=used_rvid)
