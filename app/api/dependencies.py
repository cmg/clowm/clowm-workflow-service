from contextlib import asynccontextmanager
from typing import TYPE_CHECKING, Annotated, AsyncIterator, Awaitable, Callable
from uuid import UUID

from authlib.jose.errors import BadSignatureError, DecodeError, ExpiredTokenError
from clowmdb.db.session import get_async_session
from clowmdb.models import User, Workflow, WorkflowExecution, WorkflowVersion
from fastapi import Depends, HTTPException, Path, Request, status
from fastapi.security import HTTPBearer
from fastapi.security.http import HTTPAuthorizationCredentials
from httpx import AsyncClient
from opentelemetry import trace
from rgwadmin import RGWAdmin
from sqlalchemy.ext.asyncio import AsyncSession

from app.ceph.rgw import rgw
from app.ceph.s3 import s3_resource
from app.core.config import settings
from app.core.security import decode_token, request_authorization
from app.crud import CRUDUser, CRUDWorkflow, CRUDWorkflowExecution, CRUDWorkflowVersion
from app.schemas.security import JWT, AuthzRequest, AuthzResponse
from app.slurm.rest_client import SlurmClient
from app.utils.otlp import start_as_current_span_async

if TYPE_CHECKING:
    from mypy_boto3_s3.service_resource import S3ServiceResource
else:
    S3ServiceResource = object

tracer = trace.get_tracer_provider().get_tracer(__name__)

bearer_token = HTTPBearer(description="JWT Header", auto_error=False)


def get_s3_resource() -> S3ServiceResource:  # pragma: no cover
    return s3_resource


S3Service = Annotated[S3ServiceResource, Depends(get_s3_resource)]


def get_rgw_admin() -> RGWAdmin:  # pragma: no cover
    return rgw


async def get_db() -> AsyncIterator[AsyncSession]:  # pragma: no cover
    """
    Get a Session with the database.

    FastAPI Dependency Injection Function.

    Returns
    -------
    db : AsyncIterator[AsyncSession, None]
        Async session object with the database
    """
    async with get_async_session(str(settings.db.dsn_async), verbose=settings.db.verbose) as db:
        yield db


DBSession = Annotated[AsyncSession, Depends(get_db)]


@asynccontextmanager
async def get_background_http_client() -> AsyncIterator[AsyncClient]:  # pragma: no cover
    async with AsyncClient() as client:
        yield client


async def get_httpx_client(request: Request) -> AsyncClient:  # pragma: no cover
    # Fetch open http client from the app
    return request.app.requests_client


HTTPClient = Annotated[AsyncClient, Depends(get_httpx_client)]


def get_slurm_client(client: HTTPClient) -> SlurmClient:  # pragma: no cover
    return SlurmClient(client=client)


HTTPSlurmClient = Annotated[SlurmClient, Depends(get_slurm_client)]


def get_decode_jwt_function() -> Callable[[str], dict[str, str]]:  # pragma: no cover
    """
    Get function to decode and verify the JWT.

    This will be injected into the function which will handle the JWT. With this approach, the function to decode and
    verify the JWT can be overwritten during tests.

    Returns
    -------
    decode : Callable[[str], dict[str, str]]
        Function to decode & verify the token. raw_token -> claims. Dependency Injection
    """
    return decode_token


@start_as_current_span_async("decode_jwt", tracer=tracer)
async def decode_bearer_token(
    token: Annotated[HTTPAuthorizationCredentials | None, Depends(bearer_token)],
    decode: Annotated[Callable[[str], dict[str, str]], Depends(get_decode_jwt_function)],
    db: DBSession,
) -> JWT:
    """
    Get the decoded JWT or reject request if it is not valid or the user doesn't exist.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    token : fastapi.security.http.HTTPAuthorizationCredentials
        Bearer token sent with the HTTP request. Dependency Injection.
    decode : Callable[[str], dict[str, str]]
        Function to decode & verify the token. raw_token -> claims. Dependency Injection
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    token : app.schemas.security.JWT
        The verified and decoded JWT.
    """
    if token is None:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated")
    try:
        jwt = JWT(**decode(token.credentials), raw_token=token.credentials)
        trace.get_current_span().set_attributes({"exp": jwt.exp.isoformat(), "uid": jwt.sub})
        await get_current_user(jwt, db)  # make sure the user exists
        return jwt
    except ExpiredTokenError:  # pragma: no cover
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="JWT Signature has expired")
    except (DecodeError, BadSignatureError):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Malformed JWT")


async def get_current_user(token: JWT = Depends(decode_bearer_token), db: AsyncSession = Depends(get_db)) -> User:
    """
    Get the current user from the database based on the JWT.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    token : app.schemas.security.JWT
        The verified and decoded JWT.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    user : clowmdb.models.User
        User associated with the JWT sent with the HTTP request.
    """
    try:
        uid = UUID(token.sub)
    except ValueError:  # pragma: no cover
        raise DecodeError()
    user = await CRUDUser.get(db=db, uid=uid)
    if user:
        return user
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")


CurrentUser = Annotated[User, Depends(get_current_user)]


class AuthorizationDependency:
    """
    Class to parameterize the authorization request with the resource to perform an operation on.
    """

    def __init__(self, resource: str):
        """
        Parameters
        ----------
        resource : str
            Resource parameter for the authorization requests
        """
        self.resource = resource

    def __call__(
        self,
        user: CurrentUser,
        client: HTTPClient,
    ) -> Callable[[str], Awaitable[AuthzResponse]]:
        """
        Get the function to request the authorization service with the resource, JWT and HTTP Client already injected.

        Parameters
        ----------
        user : clowmdb.models.User
            The current user based on the JWT. Dependency Injection.

        client : httpx.AsyncClient
            HTTP Client with an open connection. Dependency Injection.

        Returns
        -------
        authorization_function : Callable[[str], Awaitable[app.schemas.security.AuthzResponse]]
            Async function which ask the Auth service for authorization. It takes the operation as the only input.
        """

        async def authorization_wrapper(operation: str) -> AuthzResponse:
            params = AuthzRequest(operation=operation, resource=self.resource, uid=user.lifescience_id)
            return await request_authorization(request_params=params, client=client)

        return authorization_wrapper


async def get_current_workflow(
    wid: Annotated[UUID, Path(description="ID of a workflow", examples=["0cc78936-381b-4bdd-999d-736c40591078"])],
    db: DBSession,
) -> Workflow:
    """
    Get the workflow from the database with the ID given in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    wid: uuid.UUID
        ID of workflow. Path parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    workflow : clowmdb.models.Workflow
        User associated with ID in the path.

    """
    workflow = await CRUDWorkflow.get(db=db, workflow_id=wid)
    if workflow:
        return workflow
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Workflow with ID {wid} not found")


CurrentWorkflow = Annotated[Workflow, Depends(get_current_workflow)]


async def get_current_workflow_execution(
    eid: Annotated[
        UUID, Path(description="ID of a workflow execution.", examples=["0cc78936-381b-4bdd-999d-736c40591078"])
    ],
    db: DBSession,
) -> WorkflowExecution:
    """
    Get the workflow execution from the database with the ID given in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    eid: uuid.UUID
        ID of workflow execution. Path parameter.
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.

    Returns
    -------
    workflow : clowmdb.models.Workflow
        User associated with ID in the path.

    """
    execution = await CRUDWorkflowExecution.get(db=db, execution_id=eid)
    if execution is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Workflow Execution with id {eid} not found")
    return execution


async def get_current_workflow_version(
    workflow: CurrentWorkflow,
    db: DBSession,
    git_commit_hash: Annotated[
        str,
        Path(
            description="Git commit git_commit_hash of specific version.",
            pattern=r"^([0-9a-f]{40}|latest)$",
            examples=["ba8bcd9294c2c96aedefa1763a84a18077c50c0f"],
        ),
    ],
) -> WorkflowVersion:
    """
    Get the workflow version from the database with the ID given in the path.

    FastAPI Dependency Injection Function.

    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        Current workflow with ID given in Path. Dependency Injection..
    db : sqlalchemy.ext.asyncio.AsyncSession.
        Async database session to perform query on. Dependency Injection.
    git_commit_hash : str
        ID of a workflow version. Path Parameter.

    Returns
    -------
    version : clowmdb.models.WorkflowVersion
        Workflow version with the given ID.
    """
    version = await CRUDWorkflowVersion.get(git_commit_hash, workflow.workflow_id, db=db)
    if version is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Combination of Workflow '{workflow.workflow_id}' and Workflow Version '{git_commit_hash}' not found",  # noqa:E501
        )
    return version


CurrentWorkflowVersion = Annotated[WorkflowVersion, Depends(get_current_workflow_version)]
