from typing import Any

from fastapi import APIRouter, Depends, status

from app.api.dependencies import decode_bearer_token
from app.api.endpoints.workflow import router as workflow_router
from app.api.endpoints.workflow_credentials import router as credentials_router
from app.api.endpoints.workflow_execution import router as execution_router
from app.api.endpoints.workflow_mode import router as mode_router
from app.api.endpoints.workflow_version import router as version_router
from app.schemas.security import ErrorDetail

alternative_responses: dict[int | str, dict[str, Any]] = {
    status.HTTP_400_BAD_REQUEST: {
        "model": ErrorDetail,
        "description": "Error decoding JWT Token",
        "content": {"application/json": {"example": {"detail": "Malformed JWT Token"}}},
    },
    status.HTTP_401_UNAUTHORIZED: {
        "model": ErrorDetail,
        "description": "Not authenticated",
        "content": {"application/json": {"example": {"detail": "Not authenticated"}}},
    },
    status.HTTP_403_FORBIDDEN: {
        "model": ErrorDetail,
        "description": "Not authorized",
        "content": {"application/json": {"example": {"detail": "Not authorized"}}},
    },
    status.HTTP_404_NOT_FOUND: {
        "model": ErrorDetail,
        "description": "Entity not Found",
        "content": {"application/json": {"example": {"detail": "Entity not found."}}},
    },
}

api_router = APIRouter()
api_router.include_router(workflow_router, responses=alternative_responses, dependencies=[Depends(decode_bearer_token)])
api_router.include_router(version_router, responses=alternative_responses, dependencies=[Depends(decode_bearer_token)])
api_router.include_router(mode_router, responses=alternative_responses, dependencies=[Depends(decode_bearer_token)])
api_router.include_router(
    credentials_router, responses=alternative_responses, dependencies=[Depends(decode_bearer_token)]
)
api_router.include_router(
    execution_router, responses=alternative_responses, dependencies=[Depends(decode_bearer_token)]
)
