import json
import re
import shlex
from asyncio import sleep as async_sleep
from os import environ
from pathlib import Path
from tempfile import SpooledTemporaryFile
from typing import Any
from uuid import UUID

import botocore.client
import dotenv
from clowmdb.models import WorkflowExecution
from httpx import HTTPError
from mako.template import Template
from opentelemetry import trace

from app.api import dependencies
from app.api.background import dependencies as background_dependencies
from app.ceph.rgw import get_s3_keys
from app.core.config import MonitorJobBackoffStrategy, settings
from app.crud import CRUDWorkflowExecution
from app.git_repository.abstract_repository import GitRepository
from app.scm import SCM
from app.slurm import SlurmClient, SlurmJobSubmission
from app.utils.backoff_strategy import BackoffStrategy, ExponentialBackoff, LinearBackoff, NoBackoff

nextflow_command_template = Template(filename="app/templates/scripts/nextflow_command.sh.tmpl")
# regex to find S3 files in parameters of workflow execution
s3_file_regex = re.compile(
    r"s3://(?!(((2(5[0-5]|[0-4]\d)|[01]?\d{1,2})\.){3}(2(5[0-5]|[0-4]\d)|[01]?\d{1,2})$))[a-z\d][a-z\d.-]{1,61}[a-z\d][^\"]*"
)

tracer = trace.get_tracer_provider().get_tracer(__name__)

dotenv.load_dotenv()
execution_env: dict[str, str | int | bool] = {
    key[13:]: val for key, val in environ.items() if key.startswith("WORKFLOW_ENV_") and len(key) > 12
}
execution_env["SCM_DIR"] = str(Path(settings.cluster.working_directory) / "scm")


async def cancel_slurm_job(job_id: int) -> None:
    """
    Cancel a slurm job by its id.

    Parameters
    ----------
    job_id : int
        Job id in slurm.
    """
    async with background_dependencies.get_async_slurm_client() as slurm_client:
        await slurm_client.cancel_job(job_id=job_id)


async def start_workflow_execution(
    execution: WorkflowExecution,
    parameters: dict[str, Any],
    git_repo: GitRepository,
    scm_file_id: UUID | None = None,
    workflow_entrypoint: str | None = None,
) -> None:
    """
    Start a workflow on the Slurm cluster.

    Parameters
    ----------
    execution : clowmdb.models.WorkflowExecution
        Workflow execution to execute.
    parameters : dict[str, Any]
        Parameters for the workflow.
    git_repo : app.git_repository.abstract_repository.GitRepository
        Git repository of the workflow version.
    scm_file_id : UUID | None
        ID of the SCM file for private git repositories.
    workflow_entrypoint : str | None
        Entrypoint for the workflow by specifying the `-entry` parameter
    """
    with tracer.start_as_current_span(
        "background_start_workflow_execution",
        attributes={
            "execution_id": str(execution.execution_id),
            "executor_id": str(execution.executor_id),
            "git_commit_hash": git_repo.commit,
            "repository_url": git_repo.url,
        },
    ) as span:
        if workflow_entrypoint is not None:  # pragma: no cover
            span.set_attribute("workflow_entrypoint", workflow_entrypoint)
        if execution.workflow_version_id is not None:  # pragma: no cover
            span.set_attribute("workflow_version_id", execution.workflow_version_id)
        if execution.workflow_mode_id is not None:  # pragma: no cover
            span.set_attribute("workflow_mode_id", str(execution.workflow_mode_id))
        s3 = dependencies.get_s3_resource()
        # Upload parameters to
        params_file_name = f"params-{execution.execution_id.hex}.json"
        with SpooledTemporaryFile(max_size=512000) as f:
            f.write(json.dumps(parameters).encode("utf-8"))
            f.seek(0)
            with tracer.start_as_current_span("s3_upload_workflow_execution_parameters") as span:
                span.set_attribute("workflow_execution_id", str(execution.execution_id))
                s3.Bucket(name=settings.s3.params_bucket).Object(key=params_file_name).upload_fileobj(f)
        for key in parameters.keys():
            if isinstance(parameters[key], str):
                # Escape string parameters for bash shell
                parameters[key] = shlex.quote(parameters[key]).replace("$", "\$")

        # Check if the there is an SCM file for the workflow
        if scm_file_id is not None:
            try:
                with tracer.start_as_current_span("s3_check_workflow_scm_file"):
                    s3.Bucket(settings.s3.params_bucket).Object(SCM.generate_filename(scm_file_id)).load()
            except botocore.client.ClientError:
                scm_file_id = None

        nextflow_script = nextflow_command_template.render(
            repo=git_repo,
            parameters=parameters,
            execution_id=execution.execution_id,
            settings=settings,
            scm_file_id=scm_file_id,
            debug_s3_path=execution.debug_path,
            logs_s3_path=execution.logs_path,
            provenance_s3_path=execution.provenance_path,
            workflow_entrypoint=workflow_entrypoint,
        )

        rgw = dependencies.get_rgw_admin()
        keys = get_s3_keys(rgw=rgw, uid=execution.executor_id)
        if len(keys) == 0:
            raise KeyError("no s3 keys available")
        # Setup env for the workflow execution
        work_directory = str(Path(settings.cluster.working_directory) / f"run-{execution.execution_id.hex}")
        env = execution_env.copy()
        env["TOWER_WORKSPACE_ID"] = execution.execution_id.hex[:16]
        env["NXF_WORK"] = str(Path(work_directory) / "work")
        env["NXF_ANSI_LOG"] = False
        env["NXF_ASSETS"] = str(
            Path(env.get("NXF_ASSETS", "$HOME/.nextflow/assets")) / f"{git_repo.name}_{git_repo.commit}"  # type: ignore[arg-type]
        )
        env["AWS_ACCESS_KEY_ID"] = keys[0].access_key
        env["AWS_SECRET_ACCESS_KEY"] = keys[0].secret_key

        try:
            job_submission = SlurmJobSubmission(
                script=nextflow_script.strip(),
                job={
                    "current_working_directory": settings.cluster.working_directory,
                    "environment": env,
                    "name": execution.execution_id.hex,
                    "requeue": False,
                    "standard_output": str(
                        Path(settings.cluster.working_directory) / f"slurm-{execution.execution_id.hex}.out"
                    ),
                },
            )
            async with background_dependencies.get_async_slurm_client() as slurm_client:
                # Try to start the job on the slurm cluster
                slurm_job_id = await slurm_client.submit_job(job_submission=job_submission)
                async with background_dependencies.get_background_db() as db:
                    await CRUDWorkflowExecution.update_slurm_job_id(
                        db=db, slurm_job_id=slurm_job_id, execution_id=execution.execution_id
                    )
                if not settings.cluster.job_monitoring == MonitorJobBackoffStrategy.NOMONITORING:  # pragma: no cover
                    await _monitor_proper_job_execution(
                        slurm_client=slurm_client, execution_id=execution.execution_id, slurm_job_id=slurm_job_id
                    )
        except (HTTPError, KeyError):
            # Mark job as aborted when there is an error
            async with background_dependencies.get_background_db() as db:
                await CRUDWorkflowExecution.set_error(
                    db=db, execution_id=execution.execution_id, status=WorkflowExecution.WorkflowExecutionStatus.ERROR
                )


async def _monitor_proper_job_execution(
    slurm_client: SlurmClient, execution_id: UUID, slurm_job_id: int
) -> None:  # pragma: no cover
    """
    Check in an interval based on a backoff strategy if the slurm job is still running
    the workflow execution in the database is not marked as finished.

    Parameters
    ----------
    slurm_client : app.slurm.rest_client.SlurmClient
        Slurm Rest Client to communicate with Slurm cluster.
    execution_id : uuid.UUID
        ID of the workflow execution
    slurm_job_id : int
        ID of the slurm job to monitor
    """
    with tracer.start_as_current_span(
        "background_monitor_job_execution",
        attributes={
            "execution_id": str(execution_id),
            "slurm_job_id": slurm_job_id,
            "backoff_strategy": settings.cluster.job_monitoring.name,
        },
    ):
        if settings.cluster.job_monitoring == MonitorJobBackoffStrategy.EXPONENTIAL:
            # exponential to 50 minutes
            sleep_generator: BackoffStrategy = ExponentialBackoff(initial_delay=30, max_value=300)  # type: ignore[no-redef]
        elif settings.cluster.job_monitoring == MonitorJobBackoffStrategy.LINEAR:
            # 5 seconds increase to 5 minutes
            sleep_generator: BackoffStrategy = LinearBackoff(  # type: ignore[no-redef]
                initial_delay=30, backoff=5, max_value=300
            )
        elif settings.cluster.job_monitoring == MonitorJobBackoffStrategy.CONSTANT:
            # constant 30 seconds polling
            sleep_generator: BackoffStrategy = NoBackoff(initial_delay=30, constant_value=30)  # type: ignore[no-redef]
        else:
            return
        for sleep_time in sleep_generator:
            await async_sleep(sleep_time)
            with tracer.start_span(
                "monitor_job", attributes={"execution_id": str(execution_id), "slurm_job_id": slurm_job_id}
            ) as span:
                if await slurm_client.is_job_finished(slurm_job_id):
                    async with background_dependencies.get_background_db() as db:
                        execution = await CRUDWorkflowExecution.get(db=db, execution_id=execution_id)
                        # Check if the execution is marked as finished in the database
                        if execution is not None:
                            span.set_attribute("workflow_execution_status", str(execution.status))
                            if execution.end_time is None:
                                # Mark job as finished with an error
                                await CRUDWorkflowExecution.set_error(
                                    db=db,
                                    execution_id=execution_id,
                                    status=WorkflowExecution.WorkflowExecutionStatus.ERROR,
                                )
                    sleep_generator.close()
