from io import BytesIO
from pathlib import Path
from uuid import UUID

from opentelemetry import trace
from PIL import Image

from app.api import dependencies
from app.core.config import settings
from app.crud import CRUDWorkflowVersion
from app.git_repository import GitRepository
from app.scm import SCM

tracer = trace.get_tracer_provider().get_tracer(__name__)


async def process_and_upload_icon(icon_slug: str, icon_buffer_file: Path) -> None:
    """
    Process the icon and upload it to the S3 Icon Bucket

    Parameters
    ----------
    icon_slug : str
        Slug of the icon
    icon_buffer_file : pathlib.Path
        Path to the file containing the icon
    """
    with tracer.start_as_current_span(
        "background_process_upload_icon", attributes={"icon_slug": icon_slug, "icon_buffer_file": str(icon_buffer_file)}
    ) as span:
        try:
            thumbnail_buffer = BytesIO()
            with open(icon_buffer_file, "rb") as f:
                im = Image.open(f)
                im.thumbnail((64, 64))  # Crop to 64x64 image
                im.save(thumbnail_buffer, "PNG")  # save in buffer as PNG image
                span.set_attribute("uploaded_icon_size", f.tell())
            thumbnail_buffer.seek(0)
            with tracer.start_as_current_span("s3_upload_workflow_version_icon", attributes={"icon": icon_slug}):
                # Upload to bucket
                s3 = dependencies.get_s3_resource()
                s3.Bucket(name=settings.s3.icon_bucket).Object(key=icon_slug).upload_fileobj(
                    Fileobj=thumbnail_buffer, ExtraArgs={"ContentType": "image/png"}
                )
        finally:
            icon_buffer_file.unlink()


def upload_scm_file(scm: SCM, scm_file_id: UUID) -> None:
    """
    Upload the SCM file of a private workflow into the PARAMS_BUCKET with the workflow id as key

    Parameters
    ----------
    scm : app.scm.SCM
        Python object representing the SCM file
    scm_file_id : str
        ID of the scm file for the name of the file in the bucket
    """
    with (
        BytesIO() as handle,
        tracer.start_as_current_span("background_upload_scm_file", attributes={"scm_file_id": str(scm_file_id)}),
    ):
        scm.serialize(handle)
        handle.seek(0)
        with tracer.start_as_current_span(
            "s3_upload_workflow_credentials",
            attributes={"bucket_name": settings.s3.params_bucket, "key": SCM.generate_filename(scm_file_id)},
        ):
            s3 = dependencies.get_s3_resource()
            s3.Bucket(settings.s3.params_bucket).Object(SCM.generate_filename(scm_file_id)).upload_fileobj(handle)


def delete_s3_obj(bucket_name: str, key: str) -> None:
    """
    Delete an object in S3.

    Parameters
    ----------
    bucket_name : str
        The name of the S3 bucket.
    key : str
        The key for the S3 object.
    """
    with tracer.start_as_current_span(
        "background_s3_delete_object", attributes={"bucket_name": bucket_name, "key": key}
    ):
        s3 = dependencies.get_s3_resource()
        s3.Bucket(bucket_name).Object(key).delete()


async def delete_remote_icon(icon_slug: str) -> None:
    """
    Delete icon in S3 Bucket if there are no other workflow versions that depend on it

    Parameters
    ----------
    icon_slug : str
        Name of the icon file.
    """
    with tracer.start_as_current_span("background_delete_remote_icon", attributes={"icon_slug": icon_slug}):
        # If there are no more Workflow versions that have this icon, delete it in the S3 ICON_BUCKET
        async for db in dependencies.get_db():
            check = await CRUDWorkflowVersion.icon_exists(icon_slug, db=db)
        if not check:
            delete_s3_obj(bucket_name=settings.s3.icon_bucket, key=icon_slug)


async def download_file_to_bucket(repo: GitRepository, *, filepath: str, bucket_name: str, key: str) -> None:
    """
    Download a file from a git repository to a bucket.

    Parameters
    ----------
    repo : app.git_repository.GitRepository
        The git repository object.
    filepath : str
        The path to the file in the git repository.
    bucket_name : str
        The name of the S3 bucket.
    key : str
        The key for the S3 object.
    """
    with tracer.start_as_current_span(
        "background_download_file_to_bucket",
        attributes={"repository_url": repo.url, "filepath": filepath, "bucket_name": bucket_name, "key": key},
    ):
        s3 = dependencies.get_s3_resource()
        async with dependencies.get_background_http_client() as client:
            await repo.copy_file_to_bucket(
                filepath=filepath,
                obj=s3.Bucket(name=bucket_name).Object(key=key),
                client=client,
            )
