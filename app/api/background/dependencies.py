import smtplib
from contextlib import asynccontextmanager, contextmanager
from ssl import PROTOCOL_TLS_CLIENT, SSLContext
from typing import TYPE_CHECKING, AsyncIterator, Generator, Sequence

from sqlalchemy.ext.asyncio import AsyncSession

from app.api import dependencies
from app.slurm.rest_client import SlurmClient

if TYPE_CHECKING:
    from _typeshed import SizedBuffer
else:
    SizedBuffer = bytes

from app.core.config import settings

_context = SSLContext(protocol=PROTOCOL_TLS_CLIENT)
_context.load_default_certs()
if settings.smtp.ca_path is not None and settings.smtp.key_path is not None:  # pragma: no cover
    _context.load_cert_chain(certfile=settings.smtp.ca_path, keyfile=settings.smtp.key_path)


@asynccontextmanager
async def get_async_slurm_client() -> AsyncIterator[SlurmClient]:  # pragma: no cover
    async with dependencies.get_background_http_client() as client:
        yield SlurmClient(client)


@asynccontextmanager
async def get_background_db() -> AsyncIterator[AsyncSession]:
    async for db in dependencies.get_db():
        yield db
        break


class FakeSMTP(smtplib.SMTP):  # pragma: no cover
    """
    Fake SMTP class to print sent emails to the console or discard them.
    """

    def __init__(self, print_to_console: bool = False) -> None:
        super().__init__()
        self.print_to_console = print_to_console

    def ehlo_or_helo_if_needed(self) -> None:
        pass

    def sendmail(
        self,
        from_addr: str,
        to_addrs: str | Sequence[str],
        msg: SizedBuffer | str,
        mail_options: Sequence[str] = (),
        rcpt_options: Sequence[str] = (),
    ) -> dict[str, tuple[int, bytes]]:
        if self.print_to_console:
            print(msg if isinstance(msg, str) else msg.decode("utf-8"))  # type: ignore[attr-defined]
        return {}

    def __str__(self) -> str:
        return f"FakeSMTP(print_to_console={self.print_to_console})"

    def __repr__(self) -> str:
        return str(self)


@contextmanager
def smtp_connection() -> Generator[smtplib.SMTP, None, None]:  # pragma: no cover
    """
    Yield a connection the smtp server configured in the options.

    Returns
    -------
    smtp : smtplib.SMTP
        The open connection to the smtp server
    """
    global _context
    if settings.smtp.server == "console":
        yield FakeSMTP(print_to_console=True)
        return
    if settings.smtp.server is None:
        yield FakeSMTP(print_to_console=False)
        return
    if settings.smtp.connection_security == "ssl" or settings.smtp.connection_security == "tls":
        smtp_manager: smtplib.SMTP = smtplib.SMTP_SSL(
            settings.smtp.server,
            settings.smtp.port,
            local_hostname=settings.smtp.local_hostname,
            context=_context,
        )
    else:
        smtp_manager = smtplib.SMTP(
            settings.smtp.server, settings.smtp.port, local_hostname=settings.smtp.local_hostname
        )

    with smtp_manager as server:
        if settings.smtp.connection_security == "starttls":
            server.starttls(context=_context)
        if settings.smtp.user is not None and settings.smtp.password is not None:
            server.login(user=settings.smtp.user, password=settings.smtp.password.get_secret_value())
        yield server
