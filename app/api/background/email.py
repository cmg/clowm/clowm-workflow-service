import smtplib
from email import utils as email_utils
from email.message import EmailMessage
from enum import StrEnum, unique
from typing import Any

from clowmdb.models import User, Workflow, WorkflowVersion
from mako.lookup import TemplateLookup
from opentelemetry import trace

from app.api import dependencies
from app.api.background.dependencies import get_background_db, smtp_connection
from app.core.config import settings
from app.core.security import request_role_users
from app.crud import CRUDUser
from app.schemas.security import Roles

tracer = trace.get_tracer_provider().get_tracer(__name__)

_emailHtmlLookup = TemplateLookup(directories=["app/templates/email/html"], module_directory="/tmp/mako_modules")
_emailPlainLookup = TemplateLookup(directories=["app/templates/email/plain"], module_directory="/tmp/mako_modules")


@unique
class EmailTemplates(StrEnum):
    REVIEW_REQUEST = "review_request"
    REVIEW_RESPONSE = "review_response"
    NEW_WORKFLOW = "new_workflow"

    def render(self, **kwargs: Any) -> tuple[str, str]:
        """
        Render both HTML and plain text email templates

        Parameters
        ----------
        kwargs : Any
            Arguments for the email template

        Returns
        -------
        html, plain : tuple[str, str]
            The tuple of the rendered HTML and plain text email
        """
        return _emailHtmlLookup.get_template(f"{self}.html.tmpl").render(
            settings=settings, **kwargs
        ), _emailPlainLookup.get_template(f"{self}.txt.tmpl").render(settings=settings, **kwargs)


def _format_user_to_email(user: User) -> str:
    return f"{user.display_name} <{user.email}>"


def send_email(
    recipients: list[User] | User,
    subject: str,
    plain_msg: str,
    html_msg: str | None = None,
    *,
    smtp_server: smtplib.SMTP,
) -> None:
    """
    Send an email via the provided smtp server.

    Parameters
    ----------
    recipients: list[clowmdb.models.User] | clowmdb.models.User
        The recipients of the email. Multiple will be sent via BCC
    subject: str
        The subject of the email.
    plain_msg: str
        An alternative message in plain text format.
    html_msg : str
        An alternative message in HTML format.
    smtp_server : smtplib.SMTP
        SMTP server object
    """
    email_msg = EmailMessage()
    email_msg["Subject"] = subject
    email_msg["From"] = (
        f"CloWM <{settings.smtp.sender_email.email}>"
        if settings.smtp.sender_email.email.startswith(settings.smtp.sender_email.name)
        else str(settings.smtp.sender_email)
    )
    if isinstance(recipients, list):
        email_msg["Bcc"] = ",".join(_format_user_to_email(user) for user in recipients)
    else:
        email_msg["To"] = _format_user_to_email(recipients)
    email_msg["Date"] = email_utils.formatdate()
    if settings.smtp.reply_email is not None:  # pragma: no cover
        email_msg["Reply-To"] = (
            f"CloWM Support <{settings.smtp.reply_email.email}>"
            if settings.smtp.reply_email.email.startswith(settings.smtp.reply_email.name)
            else str(settings.smtp.reply_email)
        )
    email_msg.set_content(plain_msg)
    if html_msg is not None:
        email_msg.add_alternative(html_msg, subtype="html")
    with tracer.start_as_current_span(
        "background_send_email",
        attributes={
            "subject": subject,
            "recipients": (
                [str(user.uid) for user in recipients] if isinstance(recipients, list) else str(recipients.uid)
            ),
        },
    ):
        smtp_server.send_message(email_msg)


async def send_review_request_email(workflow: Workflow, version: WorkflowVersion) -> None:
    """
    Email all administrators and reviewers that a new workflow version must be reviewed.

    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        The workflow to review.
    version : clowmdb.models.WorkflowVersion
        The reviewed workflow to review.
    """
    async with dependencies.get_background_http_client() as client:
        lids = (await request_role_users(client=client, roles=[Roles.ADMINISTRATOR, Roles.REVIEWER])).result.roles
    async with get_background_db() as db:
        developer = await CRUDUser.get(db=db, uid=workflow.developer_id)
        recipients = await CRUDUser.get_by_lifescience_id(
            db=db, lifescience_ids=[lid for users in lids.values() for lid in users]
        )
    html, plain = EmailTemplates.REVIEW_REQUEST.render(workflow=workflow, version=version, developer=developer)
    with smtp_connection() as server:
        send_email(
            recipients=recipients,
            subject=f"Workflow Review {workflow.name}@{version.version}",
            html_msg=html,
            plain_msg=plain,
            smtp_server=server,
        )


async def send_workflow_status_update_email(workflow: Workflow, version: WorkflowVersion) -> None:
    """
    Email the developer of a workflow that the workflow was reviewed.

    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        The reviewed workflow.
    version : clowmdb.models.WorkflowVersion
        The reviewed workflow version.
    """
    async with get_background_db() as db:
        developer = await CRUDUser.get(db=db, uid=workflow.developer_id)
    html, plain = EmailTemplates.REVIEW_RESPONSE.render(workflow=workflow, version=version, developer=developer)
    with smtp_connection() as server:
        send_email(
            recipients=developer,
            subject=f"Workflow Review Response {workflow.name}@{version.version}",
            html_msg=html,
            plain_msg=plain,
            smtp_server=server,
        )


async def send_new_workflow_email(workflow: Workflow, version: WorkflowVersion) -> None:
    """
    Email all administrators and reviewers that a new workflow was registered and needs to be reviewed.

    Parameters
    ----------
    workflow : clowmdb.models.Workflow
        The reviewed workflow.
    version : clowmdb.models.WorkflowVersion
        The reviewed workflow version.
    """
    async with dependencies.get_background_http_client() as client:
        lids = (await request_role_users(client=client, roles=[Roles.ADMINISTRATOR, Roles.REVIEWER])).result.roles
    async with get_background_db() as db:
        developer = await CRUDUser.get(db=db, uid=workflow.developer_id)
        recipients = await CRUDUser.get_by_lifescience_id(
            db=db, lifescience_ids=[lid for users in lids.values() for lid in users]
        )
    html, plain = EmailTemplates.NEW_WORKFLOW.render(workflow=workflow, version=version, developer=developer)
    with smtp_connection() as server:
        send_email(
            recipients=recipients,
            subject=f"New Workflow {workflow.name}@{version.version}",
            html_msg=html,
            plain_msg=plain,
            smtp_server=server,
        )
