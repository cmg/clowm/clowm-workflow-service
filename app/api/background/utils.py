from uuid import UUID

from opentelemetry import trace

from app.api.background.dependencies import get_background_db
from app.crud import CRUDResourceVersion

tracer = trace.get_tracer_provider().get_tracer(__name__)


async def update_used_resources(ids: list[UUID]) -> None:
    """
    Update the used resources for a workflow execution.

    Parameters
    ----------
    ids : list[uuid.UUID]
        List of resource version ids.
    """
    with tracer.start_as_current_span(
        "background_update_used_resources",
        attributes={"resource_version_ids": [str(rvid) for rvid in ids]},
    ):
        async with get_background_db() as db:
            for rvid in ids:
                await CRUDResourceVersion.update_used_resource_version(resource_version_id=rvid, db=db)
