from uuid import uuid4

import pytest
from clowmdb.models import WorkflowExecution, WorkflowVersion
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud import CRUDWorkflowExecution
from app.schemas.workflow_execution import DevWorkflowExecutionIn, WorkflowExecutionIn
from app.tests.utils.user import UserWithAuthHeader
from app.tests.utils.utils import random_hex_string


class TestWorkflowExecutionCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_workflow_execution(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion, random_user: UserWithAuthHeader
    ) -> None:
        """
        Test for creating a workflow execution with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        workflow_execution = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={},
        )
        workflow_execution_db = await CRUDWorkflowExecution.create(workflow_execution, random_user.user.uid, db=db)
        assert workflow_execution_db
        assert workflow_execution_db.executor_id == random_user.user.uid
        assert workflow_execution_db.workflow_version_id == random_workflow_version.git_commit_hash
        assert workflow_execution_db.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        workflow_execution_db = await db.scalar(
            select(WorkflowExecution).where(
                WorkflowExecution.execution_id_bytes == workflow_execution_db.execution_id.bytes
            )
        )

        assert workflow_execution_db
        assert workflow_execution_db.executor_id == random_user.user.uid
        assert workflow_execution_db.workflow_version_id == random_workflow_version.git_commit_hash
        assert workflow_execution_db.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

    @pytest.mark.asyncio
    async def test_create_dev_workflow_execution(self, db: AsyncSession, random_user: UserWithAuthHeader) -> None:
        """
        Test for creating a dev workflow execution with the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        workflow_execution = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(),
            repository_url="https://example.com",
            parameters={},
        )
        workflow_execution_db = await CRUDWorkflowExecution.create(workflow_execution, random_user.user.uid, db=db)
        assert workflow_execution_db
        assert workflow_execution_db.executor_id == random_user.user.uid
        assert workflow_execution_db.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        workflow_execution_db = await db.scalar(
            select(WorkflowExecution).where(
                WorkflowExecution.execution_id_bytes == workflow_execution_db.execution_id.bytes
            )
        )
        assert workflow_execution_db
        assert workflow_execution_db.executor_id == random_user.user.uid
        assert workflow_execution_db.status == WorkflowExecution.WorkflowExecutionStatus.PENDING


class TestWorkflowExecutionCRUDGet:
    @pytest.mark.asyncio
    async def test_get_workflow_execution(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting a workflow execution by its execution id.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        execution = await CRUDWorkflowExecution.get(random_running_workflow_execution.execution_id, db=db)
        assert execution is not None
        assert execution == random_running_workflow_execution

    @pytest.mark.asyncio
    async def test_get_non_existing_workflow_execution(self, db: AsyncSession) -> None:
        """
        Test for getting a non-existing workflow execution.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        execution = await CRUDWorkflowExecution.get(uuid4(), db=db)
        assert execution is None


class TestWorkflowExecutionCRUDList:
    @pytest.mark.asyncio
    async def test_list_workflow_executions(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for listing all workflow executions.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        executions = await CRUDWorkflowExecution.list(db=db)
        assert len(executions) > 0
        assert sum(1 for execution in executions if execution == random_running_workflow_execution) == 1

    @pytest.mark.asyncio
    async def test_get_list_workflow_executions_of_user(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for listing all workflow executions and filter by user.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        executions = await CRUDWorkflowExecution.list(db=db, executor_id=random_running_workflow_execution.executor_id)
        assert len(executions) > 0
        assert sum(1 for execution in executions if execution == random_running_workflow_execution) == 1
        assert (
            sum(1 for execution in executions if execution.executor_id == random_running_workflow_execution.executor_id)
            >= 1
        )

    @pytest.mark.asyncio
    async def test_get_list_workflow_executions_of_non_existing_user(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for listing all workflow executions and filter by non-existing user.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        executions = await CRUDWorkflowExecution.list(db=db, executor_id=uuid4())
        assert len(executions) == 0
        assert sum(1 for execution in executions if execution == random_running_workflow_execution) == 0

    @pytest.mark.asyncio
    async def test_get_list_workflow_executions_of_workflow_version(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for listing all workflow executions and filter by workflow version id.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        executions = await CRUDWorkflowExecution.list(
            workflow_version_id=random_running_workflow_execution.workflow_version_id, db=db
        )
        assert len(executions) > 0
        assert sum(1 for execution in executions if execution == random_running_workflow_execution) == 1
        assert (
            sum(
                1
                for execution in executions
                if execution.workflow_version_id == random_running_workflow_execution.workflow_version_id
            )
            >= 1
        )

    @pytest.mark.asyncio
    async def test_get_list_workflow_executions_of_non_existing_workflow_version(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for listing all workflow executions and filter by non-existing workflow version id.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        executions = await CRUDWorkflowExecution.list(db=db, workflow_version_id=random_hex_string())
        assert len(executions) == 0
        assert sum(1 for execution in executions if execution == random_running_workflow_execution) == 0

    @pytest.mark.asyncio
    async def test_get_list_workflow_executions_with_given_status(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for listing all workflow executions and filter by status.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        executions = await CRUDWorkflowExecution.list(db=db, status_list=[random_running_workflow_execution.status])
        assert len(executions) > 0
        assert sum(1 for execution in executions if execution == random_running_workflow_execution) == 1
        assert sum(1 for execution in executions if execution.status == random_running_workflow_execution.status) >= 1


class TestWorkflowExecutionCRUDLUpdate:
    @pytest.mark.asyncio
    async def test_cancel_workflow_execution(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for canceling a workflow execution.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        await CRUDWorkflowExecution.set_error(random_running_workflow_execution.execution_id, db=db)

        stmt = select(WorkflowExecution).where(
            WorkflowExecution.execution_id_bytes == random_running_workflow_execution.execution_id.bytes
        )
        execution = await db.scalar(stmt)
        assert execution is not None
        assert execution.status == WorkflowExecution.WorkflowExecutionStatus.CANCELED

    @pytest.mark.asyncio
    async def test_update_workflow_execution_slurm_job(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for updating the slurm job id of a workflow execution.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        await CRUDWorkflowExecution.update_slurm_job_id(random_running_workflow_execution.execution_id, 250, db=db)

        stmt = select(WorkflowExecution).where(
            WorkflowExecution.execution_id_bytes == random_running_workflow_execution.execution_id.bytes
        )
        execution = await db.scalar(stmt)
        assert execution is not None
        assert execution.slurm_job_id == 250


class TestWorkflowExecutionCRUDDelete:
    @pytest.mark.asyncio
    async def test_delete_workflow_execution(
        self, db: AsyncSession, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for deleting a workflow execution.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        await CRUDWorkflowExecution.delete(random_running_workflow_execution.execution_id, db=db)

        stmt = select(WorkflowExecution).where(
            WorkflowExecution.execution_id_bytes == random_running_workflow_execution.execution_id.bytes
        )
        execution = await db.scalar(stmt)
        assert execution is None
