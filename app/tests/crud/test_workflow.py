import random
from datetime import date, timedelta
from uuid import uuid4

import pytest
from clowmdb.models import Workflow, WorkflowExecution, WorkflowVersion
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from app.crud import CRUDWorkflow
from app.schemas.workflow import WorkflowIn, WorkflowOut
from app.schemas.workflow_mode import WorkflowModeIn
from app.tests.utils.cleanup import CleanupList, delete_workflow
from app.tests.utils.user import UserWithAuthHeader
from app.tests.utils.utils import random_hex_string, random_lower_string


class TestWorkflowCRUDList:
    @pytest.mark.asyncio
    async def test_get_all_workflows(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test get all workflows from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflows = await CRUDWorkflow.list_workflows(db=db)
        assert len(workflows) == 1
        assert workflows[0].workflow_id == random_workflow.workflow_id

    @pytest.mark.asyncio
    async def test_get_workflows_by_developer(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_user: UserWithAuthHeader
    ) -> None:
        """
        Test get only workflows from the CRUD Repository by specific developer.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        workflows = await CRUDWorkflow.list_workflows(developer_id=random_user.user.uid, db=db)
        assert len(workflows) == 1
        assert workflows[0].workflow_id == random_workflow.workflow_id

    @pytest.mark.asyncio
    async def test_get_workflows_with_unpublished_version(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test get only workflows from the CRUD Repository with an unpublished version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflows = await CRUDWorkflow.list_workflows(version_status=[WorkflowVersion.Status.CREATED], db=db)
        assert len(workflows) == 1
        assert workflows[0].workflow_id == random_workflow.workflow_id

    @pytest.mark.asyncio
    async def test_get_workflow_with_name_substring(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test get workflows with a substring in their name from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        substring_indices = [0, 0]
        while substring_indices[0] == substring_indices[1]:
            substring_indices = sorted(random.choices(range(len(random_workflow.name)), k=2))

        random_substring = random_workflow.name[substring_indices[0] : substring_indices[1]]
        workflows = await CRUDWorkflow.list_workflows(name_substring=random_substring, db=db)
        assert len(workflows) > 0
        assert random_workflow.workflow_id in map(lambda w: w.workflow_id, workflows)

    @pytest.mark.asyncio
    async def test_search_non_existing_workflow_by_name(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test for getting a non-existing workflow by its name from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflows = await CRUDWorkflow.list_workflows(name_substring=2 * random_workflow.name, db=db)
        assert sum(1 for w in workflows if w.workflow_id == random_workflow.workflow_id) == 0


class TestWorkflowCRUDGet:
    @pytest.mark.asyncio
    async def test_get_specific_workflow(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test for getting a workflow by its id from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflow = await CRUDWorkflow.get(random_workflow.workflow_id, db=db)
        assert workflow is not None
        assert workflow.workflow_id == random_workflow.workflow_id

    @pytest.mark.asyncio
    async def test_get_workflow_by_name(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test for getting a workflow by its name from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflow = await CRUDWorkflow.get_by_name(random_workflow.name, db=db)
        assert workflow is not None
        assert workflow.workflow_id == random_workflow.workflow_id

    @pytest.mark.asyncio
    async def test_get_workflow_statistics(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_running_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting the aggregated workflow statistics.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow.statistics(random_workflow.workflow_id, db=db)
        assert len(statistics) == 1
        assert statistics[0].day == date.today()
        assert statistics[0].count == 1

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow.developer_statistics(db=db)
        assert len(statistics) == 1
        assert statistics[0].started_at == date.today()
        assert statistics[0].workflow_id == random_workflow.workflow_id
        assert statistics[0].workflow_execution_id == random_completed_workflow_execution.execution_id
        assert statistics[0].workflow_version_id == random_completed_workflow_execution.workflow_version_id
        assert statistics[0].pseudo_uid != random_completed_workflow_execution.executor_id.hex
        assert statistics[0].developer_id == random_workflow.developer_id

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_developer_id(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a developer ID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow.developer_statistics(developer_id=random_workflow.developer_id, db=db)
        assert len(statistics) == 1
        assert statistics[0].started_at == date.today()
        assert statistics[0].workflow_id == random_workflow.workflow_id
        assert statistics[0].workflow_execution_id == random_completed_workflow_execution.execution_id
        assert statistics[0].workflow_version_id == random_completed_workflow_execution.workflow_version_id
        assert statistics[0].pseudo_uid != random_completed_workflow_execution.executor_id.hex
        assert statistics[0].developer_id == random_workflow.developer_id

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_non_existent_developer_id(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a non-existing developer ID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow.developer_statistics(developer_id=uuid4(), db=db)
        assert len(statistics) == 0

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_workflow_id(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a workflow ID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow.developer_statistics(workflow_ids=[random_workflow.workflow_id], db=db)
        assert len(statistics) == 1
        assert statistics[0].started_at == date.today()
        assert statistics[0].workflow_id == random_workflow.workflow_id
        assert statistics[0].workflow_execution_id == random_completed_workflow_execution.execution_id
        assert statistics[0].workflow_version_id == random_completed_workflow_execution.workflow_version_id
        assert statistics[0].pseudo_uid != random_completed_workflow_execution.executor_id.hex
        assert statistics[0].developer_id == random_workflow.developer_id

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_non_existent_workflow_id(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a non-existing workflow ID.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow.developer_statistics(workflow_ids=[uuid4()], db=db)
        assert len(statistics) == 0

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_start_date(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a start date in the past.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow.developer_statistics(start=date.today() - timedelta(days=7), db=db)
        assert len(statistics) == 1
        assert statistics[0].started_at == date.today()
        assert statistics[0].workflow_id == random_workflow.workflow_id
        assert statistics[0].workflow_execution_id == random_completed_workflow_execution.execution_id
        assert statistics[0].workflow_version_id == random_completed_workflow_execution.workflow_version_id
        assert statistics[0].pseudo_uid != random_completed_workflow_execution.executor_id.hex
        assert statistics[0].developer_id == random_workflow.developer_id

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_bad_start_day(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a start date in the future.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow.developer_statistics(start=date.today() + timedelta(days=7), db=db)
        assert len(statistics) == 0

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_end_date(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a end date in the future.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow.developer_statistics(end=date.today() + timedelta(days=7), db=db)
        assert len(statistics) == 1
        assert statistics[0].started_at == date.today()
        assert statistics[0].workflow_id == random_workflow.workflow_id
        assert statistics[0].workflow_execution_id == random_completed_workflow_execution.execution_id
        assert statistics[0].workflow_version_id == random_completed_workflow_execution.workflow_version_id
        assert statistics[0].pseudo_uid != random_completed_workflow_execution.executor_id.hex
        assert statistics[0].developer_id == random_workflow.developer_id

    @pytest.mark.asyncio
    async def test_get_workflow_developer_statistics_with_bad_end_day(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_completed_workflow_execution: WorkflowExecution
    ) -> None:
        """
        Test for getting workflow statistics for developer with a end date in the past.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_completed_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        statistics = await CRUDWorkflow.developer_statistics(end=date.today() - timedelta(days=7), db=db)
        assert len(statistics) == 0


class TestWorkflowCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_workflow(
        self, db: AsyncSession, random_user: UserWithAuthHeader, cleanup: CleanupList
    ) -> None:
        """
        Test for creating a workflow in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        workflow_in = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.com/example/example",
        )
        workflow = await CRUDWorkflow.create(workflow=workflow_in, developer_id=random_user.user.uid, db=db)
        assert workflow is not None
        cleanup.add_task(delete_workflow, db=db, workflow_id=workflow.workflow_id)

        stmt = select(Workflow).where(Workflow.workflow_id_bytes == workflow.workflow_id.bytes)
        created_workflow = await db.scalar(stmt)
        assert created_workflow is not None
        assert created_workflow == workflow

    @pytest.mark.asyncio
    async def test_create_workflow_with_mode(
        self, db: AsyncSession, random_user: UserWithAuthHeader, cleanup: CleanupList
    ) -> None:
        """
        Test for creating a workflow with a mode in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        workflow_in = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.com/example/example",
            modes=[
                WorkflowModeIn(
                    schema_path="example/schema.json", name=random_lower_string(), entrypoint=random_lower_string()
                )
            ],
        )
        workflow = await CRUDWorkflow.create(db=db, workflow=workflow_in, developer_id=random_user.user.uid)
        assert workflow is not None
        cleanup.add_task(delete_workflow, db=db, workflow_id=workflow.workflow_id)

        stmt = (
            select(Workflow)
            .where(Workflow.workflow_id_bytes == workflow.workflow_id.bytes)
            .options(joinedload(Workflow.versions).selectinload(WorkflowVersion.workflow_modes))
        )
        created_workflow = await db.scalar(stmt)
        assert created_workflow is not None
        assert created_workflow == workflow

        assert len(created_workflow.versions) == 1
        assert len(created_workflow.versions[0].workflow_modes) == 1

    @pytest.mark.asyncio
    async def test_create_workflow_credentials(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test for updating the workflow credentials in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        token = random_lower_string(15)
        await CRUDWorkflow.update_credentials(db=db, workflow_id=random_workflow.workflow_id, token=token)

        stmt = select(Workflow).where(Workflow.workflow_id_bytes == random_workflow.workflow_id.bytes)
        workflow = await db.scalar(stmt)
        assert workflow is not None
        assert workflow.credentials_token == token


class TestWorkflowCRUDDelete:
    @pytest.mark.asyncio
    async def test_delete_workflow(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test for deleting a workflow in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """

        await CRUDWorkflow.delete(db=db, workflow_id=random_workflow.workflow_id)

        stmt = select(Workflow).where(Workflow.workflow_id_bytes == random_workflow.workflow_id.bytes)
        created_workflow = await db.scalar(stmt)
        assert created_workflow is None

        stmt = select(WorkflowVersion).where(WorkflowVersion.workflow_id_bytes == random_workflow.workflow_id.bytes)
        versions = (await db.scalars(stmt)).all()

        assert len(versions) == 0

    @pytest.mark.asyncio
    async def test_delete_workflow_credentials(self, db: AsyncSession, random_private_workflow: WorkflowOut) -> None:
        """
        Test for deleting the workflow credentials in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_private_workflow : app.schemas.workflow.WorkflowOut
            Random private workflow for testing.
        """
        await CRUDWorkflow.update_credentials(db=db, workflow_id=random_private_workflow.workflow_id, token=None)

        stmt = select(Workflow).where(Workflow.workflow_id_bytes == random_private_workflow.workflow_id.bytes)
        workflow = await db.scalar(stmt)
        assert workflow is not None
        assert workflow.credentials_token is None
