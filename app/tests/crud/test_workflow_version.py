from uuid import uuid4

import pytest
from clowmdb.models import WorkflowMode, WorkflowVersion
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload

from app.crud import CRUDWorkflowVersion
from app.schemas.workflow import WorkflowOut
from app.schemas.workflow_version import ParameterExtension
from app.tests.utils.utils import random_hex_string


class TestWorkflowVersionCRUDGet:
    @pytest.mark.asyncio
    async def test_get_specific_workflow_version(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting a workflow version by its id from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        version = await CRUDWorkflowVersion.get(random_workflow_version.git_commit_hash, db=db)
        assert version is not None
        assert version.workflow_id == random_workflow_version.workflow_id
        assert version.git_commit_hash == random_workflow_version.git_commit_hash

    @pytest.mark.asyncio
    async def test_get_specific_workflow_version_with_workflow_id(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting a workflow version and constraint it by a workflow ID from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        version = await CRUDWorkflowVersion.get(
            random_workflow_version.git_commit_hash, workflow_id=random_workflow_version.workflow_id, db=db
        )
        assert version is not None
        assert version.workflow_id == random_workflow_version.workflow_id
        assert version.git_commit_hash == random_workflow_version.git_commit_hash

    @pytest.mark.asyncio
    async def test_get_specific_workflow_version_with_false_workflow_id(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting a workflow version and constraint it with a non-existing workflow ID CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        version = await CRUDWorkflowVersion.get(random_workflow_version.git_commit_hash, workflow_id=uuid4(), db=db)
        assert version is None

    @pytest.mark.asyncio
    async def test_get_specific_workflow_version_with_populated_workflow(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting a workflow version by its id with populated workflow from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        version = await CRUDWorkflowVersion.get(random_workflow_version.git_commit_hash, populate_workflow=True, db=db)
        assert version is not None
        assert version.workflow_id == random_workflow_version.workflow_id
        assert version.git_commit_hash == random_workflow_version.git_commit_hash
        assert version.workflow.workflow_id == random_workflow_version.workflow_id

    @pytest.mark.asyncio
    async def test_get_latest_unpublished_workflow_version(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting the latest workflow version from a workflow which is not published from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        workflow_version = await CRUDWorkflowVersion.get_latest(
            random_workflow_version.workflow_id, published=False, db=db
        )
        assert workflow_version is not None
        assert workflow_version.workflow_id == random_workflow_version.workflow_id
        assert workflow_version.git_commit_hash == random_workflow_version.git_commit_hash

    @pytest.mark.asyncio
    async def test_get_latest_published_workflow_version(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting the latest workflow version from a workflow which is published from CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        workflow_version = await CRUDWorkflowVersion.get_latest(random_workflow_version.workflow_id, db=db)
        assert workflow_version is None

    @pytest.mark.asyncio
    async def test_get_all_workflow_versions(self, db: AsyncSession, random_workflow_version: WorkflowVersion) -> None:
        """
        Test for getting all versions from a workflow from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        workflow_versions = await CRUDWorkflowVersion.list_workflow_versions(random_workflow_version.workflow_id, db=db)
        assert len(workflow_versions) == 1
        assert workflow_versions[0].git_commit_hash == random_workflow_version.git_commit_hash

    @pytest.mark.asyncio
    async def test_get_all_workflow_version_with_specific_status(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting all versions with a specific status from a workflow from the CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        workflow_versions = await CRUDWorkflowVersion.list_workflow_versions(
            random_workflow_version.workflow_id, version_status=[random_workflow_version.status], db=db
        )
        assert len(workflow_versions) == 1
        assert workflow_versions[0].git_commit_hash == random_workflow_version.git_commit_hash


class TestWorkflowVersionCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_workflow_version(self, db: AsyncSession, random_workflow: WorkflowOut) -> None:
        """
        Test for creating a workflow version in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflow_version = await CRUDWorkflowVersion.create(
            db=db,
            git_commit_hash=random_hex_string(),
            version="v2.0.0",
            workflow_id=random_workflow.workflow_id,
            previous_version=random_workflow.versions[-1].workflow_version_id,
        )
        assert workflow_version is not None

        stmt = (
            select(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == workflow_version.git_commit_hash)
            .options(selectinload(WorkflowVersion.workflow_modes))
        )
        created_workflow_version = await db.scalar(stmt)
        assert created_workflow_version is not None
        assert created_workflow_version == workflow_version
        assert len(created_workflow_version.workflow_modes) == 0

    @pytest.mark.asyncio
    async def test_create_workflow_version_with_mode(
        self, db: AsyncSession, random_workflow: WorkflowOut, random_workflow_mode: WorkflowMode
    ) -> None:
        """
        Test for creating a workflow version in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        random_workflow_mode : clowmdb.models.WorkflowMode
            Random workflow mode for testing.
        """
        workflow_version = await CRUDWorkflowVersion.create(
            db=db,
            git_commit_hash=random_hex_string(),
            version="v2.0.0",
            workflow_id=random_workflow.workflow_id,
            previous_version=random_workflow.versions[-1].workflow_version_id,
            modes=[random_workflow_mode.mode_id],
        )
        assert workflow_version is not None

        stmt = (
            select(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == workflow_version.git_commit_hash)
            .options(selectinload(WorkflowVersion.workflow_modes))
        )
        created_workflow_version = await db.scalar(stmt)
        assert created_workflow_version is not None
        assert created_workflow_version == workflow_version
        assert len(created_workflow_version.workflow_modes) == 1
        assert created_workflow_version.workflow_modes[0].mode_id == random_workflow_mode.mode_id


class TestWorkflowVersionCRUDUpdate:
    @pytest.mark.asyncio
    async def test_update_workflow_version_status(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for upadting a workflow version icon in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        await CRUDWorkflowVersion.update_status(
            db=db, workflow_version_id=random_workflow_version.git_commit_hash, status=WorkflowVersion.Status.PUBLISHED
        )

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        version = await db.scalar(stmt)
        assert version
        assert version.status == WorkflowVersion.Status.PUBLISHED

    @pytest.mark.asyncio
    async def test_update_workflow_version_parameter_extension(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for updating a workflow version parameter extension in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        await CRUDWorkflowVersion.update_parameter_extension(
            db=db, workflow_version_id=random_workflow_version.git_commit_hash, parameter_extension=ParameterExtension()
        )

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        version = await db.scalar(stmt)
        assert version
        assert version.parameter_extension is not None

    @pytest.mark.asyncio
    async def test_update_workflow_version_icon(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for updating the worklfow version icon

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        new_slug = random_hex_string()
        await CRUDWorkflowVersion.update_icon(
            db=db, workflow_version_id=random_workflow_version.git_commit_hash, icon_slug=new_slug
        )

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        version = await db.scalar(stmt)
        assert version
        assert version.git_commit_hash == random_workflow_version.git_commit_hash
        assert version.icon_slug == new_slug

    @pytest.mark.asyncio
    async def test_remove_workflow_version_icon(
        self, db: AsyncSession, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for removeing the workflow version icon

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        await CRUDWorkflowVersion.update_icon(
            db=db, workflow_version_id=random_workflow_version.git_commit_hash, icon_slug=None
        )

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        version = await db.scalar(stmt)
        assert version
        assert version.git_commit_hash == random_workflow_version.git_commit_hash
        assert version.icon_slug is None


class TestWorkflowVersionCRUDCheck:
    @pytest.mark.asyncio
    async def test_check_icon_dependency(self, db: AsyncSession, random_workflow_version: WorkflowVersion) -> None:
        """
        Test for checking if a workflow version is dependent on an icon

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_version : clowmdb.model.WorkflowVersion
            Random workflow version for testing.
        """
        dependent1 = await CRUDWorkflowVersion.icon_exists(random_workflow_version.icon_slug, db=db)

        assert dependent1

        dependent2 = await CRUDWorkflowVersion.icon_exists(random_hex_string(), db=db)
        assert not dependent2
