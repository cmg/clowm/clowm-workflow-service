from uuid import uuid4

import pytest
from clowmdb.models import ResourceVersion
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud import CRUDResourceVersion


class TestResourceVersionCRUDGet:
    @pytest.mark.asyncio
    async def test_get_resource_version(
        self,
        db: AsyncSession,
        random_resource_version: ResourceVersion,
    ) -> None:
        """
        Test for getting a resource version based on the resource and resource version id.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowmdb.models.ResourceVersion
            Random resource version for testing.
        """
        version = await CRUDResourceVersion.get(
            random_resource_version.resource_id, resource_version_id=random_resource_version.resource_version_id, db=db
        )
        assert version == random_resource_version

    @pytest.mark.asyncio
    async def test_get_resource_version_latest(
        self,
        db: AsyncSession,
        random_resource_version: ResourceVersion,
    ) -> None:
        """
        Test for getting the latest resource version of a resource.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowmdb.models.ResourceVersion
            Random resource version for testing.
        """
        version = await CRUDResourceVersion.get(
            random_resource_version.resource_id,
            latest=True,
            db=db,
        )
        assert version == random_resource_version

    @pytest.mark.asyncio
    async def test_get_resource_version_with_wrong_id(
        self,
        db: AsyncSession,
        random_resource_version: ResourceVersion,
    ) -> None:
        """
        Test for getting a non-existing resource version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowmdb.models.ResourceVersion
            Random resource version for testing.
        """
        assert (
            await CRUDResourceVersion.get(
                uuid4(),
                resource_version_id=random_resource_version.resource_version_id,
                db=db,
            )
            is None
        )

        assert (
            await CRUDResourceVersion.get(
                resource_id=random_resource_version.resource_id,
                resource_version_id=uuid4(),
                db=db,
            )
            is None
        )


class TestResourceVersionCRUDUpdate:
    @pytest.mark.asyncio
    async def test_update_used_resource_version(
        self,
        db: AsyncSession,
        random_resource_version: ResourceVersion,
    ) -> None:
        """
        Test for updating a used resource version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_resource_version : clowmdb.models.ResourceVersion
            Random resource version for testing.
        """
        await CRUDResourceVersion.update_used_resource_version(
            resource_version_id=random_resource_version.resource_version_id, db=db
        )

        db_version = await db.scalar(
            select(ResourceVersion).where(
                ResourceVersion.resource_version_id_bytes == random_resource_version.resource_version_id_bytes
            )
        )
        assert db_version is not None
        assert db_version == random_resource_version
        assert db_version.times_used == 1
        assert db_version.last_used_timestamp is not None
