import pytest
from clowmdb.models import WorkflowMode, WorkflowVersion
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.crud import CRUDWorkflowMode
from app.schemas.workflow_mode import WorkflowModeIn
from app.tests.utils.cleanup import CleanupList, delete_workflow_mode
from app.tests.utils.utils import random_hex_string, random_lower_string


class TestWorkflowModeCRUDCreate:
    @pytest.mark.asyncio
    async def test_create_workflow_mode(self, db: AsyncSession, cleanup: CleanupList) -> None:
        """
        Test for creating a single workflow mode in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        workflow_mode_in = WorkflowModeIn(
            name=random_lower_string(16),
            schema_path=random_lower_string(32),
            entrypoint=random_lower_string(10),
        )
        modes = await CRUDWorkflowMode.create(db=db, modes=[workflow_mode_in])
        for m in modes:
            cleanup.add_task(delete_workflow_mode, db=db, mode_id=m.mode_id)
        assert len(modes) == 1
        assert modes[0].name == workflow_mode_in.name

    @pytest.mark.asyncio
    async def test_create_multiple_workflow_mode(self, db: AsyncSession, cleanup: CleanupList) -> None:
        """
        Test for creating multiple workflow modes in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        n = 10
        modes_in = [
            WorkflowModeIn(
                name=random_lower_string(16),
                schema_path=random_lower_string(32),
                entrypoint=random_lower_string(10),
            )
            for _ in range(n)
        ]
        modes = await CRUDWorkflowMode.create(db=db, modes=modes_in)
        for m in modes:
            cleanup.add_task(delete_workflow_mode, db=db, mode_id=m.mode_id)

        assert len(modes) == n
        for mode in modes:
            db_mode = await db.scalar(select(WorkflowMode).where(WorkflowMode.mode_id_bytes == mode.mode_id.bytes))
            assert db_mode is not None
            assert db_mode.mode_id == mode.mode_id
            assert db_mode.name == mode.name


class TestWorkflowModeCRUDGet:
    @pytest.mark.asyncio
    async def test_get_workflow_mode(self, db: AsyncSession, random_workflow_mode: WorkflowMode) -> None:
        """
        Test for getting a workflow mode in CRUD Repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_mode : clowmdb.models.WorkflowMode
            Random workflow mode for testing.
        """
        mode = await CRUDWorkflowMode.get(db=db, mode_id=random_workflow_mode.mode_id)
        assert mode is not None
        assert mode.name == random_workflow_mode.name
        assert mode.mode_id == random_workflow_mode.mode_id

    @pytest.mark.asyncio
    async def test_get_workflow_mode_with_version_filter(
        self, db: AsyncSession, random_workflow_mode: WorkflowMode, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting a workflow mode while filtering for a specific workflow version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_mode : clowmdb.models.WorkflowMode
            Random workflow mode for testing.
        random_workflow_version : clowmdb.models.WorkflowMode
            Random workflow version for testing.
        """
        mode = await CRUDWorkflowMode.get(
            db=db, mode_id=random_workflow_mode.mode_id, workflow_version_id=random_workflow_version.git_commit_hash
        )
        assert mode is not None
        assert mode.name == random_workflow_mode.name
        assert mode.mode_id == random_workflow_mode.mode_id

    @pytest.mark.asyncio
    async def test_get_workflow_mode_with_wrong_version_filter(
        self, db: AsyncSession, random_workflow_mode: WorkflowMode
    ) -> None:
        """
        Test for getting a workflow mode while filtering for a non-existing workflow version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_mode : clowmdb.models.WorkflowMode
            Random workflow mode for testing.
        """
        mode = await CRUDWorkflowMode.get(
            db=db, mode_id=random_workflow_mode.mode_id, workflow_version_id=random_hex_string()
        )
        assert mode is None


class TestWorkflowModeCRUDList:
    @pytest.mark.asyncio
    async def test_get_workflow_modes_from_version(
        self, db: AsyncSession, random_workflow_mode: WorkflowMode, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting all workflow modes for a specific workflow version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_mode : clowmdb.models.WorkflowMode
            Random workflow mode for testing.
        random_workflow_version : clowmdb.models.WorkflowMode
            Random workflow version for testing.
        """
        modes = await CRUDWorkflowMode.list_modes(db=db, workflow_version_id=random_workflow_version.git_commit_hash)
        assert len(modes) == 1
        mode = modes[0]
        assert mode.name == random_workflow_mode.name
        assert mode.mode_id == random_workflow_mode.mode_id

    @pytest.mark.asyncio
    async def test_get_workflow_modes_from_non_existing_version(
        self, db: AsyncSession, random_workflow_mode: WorkflowMode, random_workflow_version: WorkflowVersion
    ) -> None:
        """
        Test for getting all workflow modes for a non-existing workflow version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_workflow_mode : clowmdb.models.WorkflowMode
            Random workflow mode for testing.
        random_workflow_version : clowmdb.models.WorkflowMode
            Random workflow version for testing.
        """
        modes = await CRUDWorkflowMode.list_modes(db=db, workflow_version_id=random_hex_string())
        assert len(modes) == 0


class TestWorkflowModeCRUDDelete:
    @pytest.mark.asyncio
    async def test_delete_multiple_workflow_modes(self, db: AsyncSession, cleanup: CleanupList) -> None:
        """
        Test for deleting multiple workflow modes.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        # Test setup. Create workflow modes
        n = 10
        mode_ids = []
        for _ in range(n):
            mode_db = WorkflowMode(
                name=random_lower_string(), entrypoint=random_lower_string(), schema_path=random_lower_string()
            )
            db.add(mode_db)
            await db.commit()
            cleanup.add_task(delete_workflow_mode, db=db, mode_id=mode_db.mode_id)
            mode_ids.append(mode_db.mode_id)

        # Actual test
        await CRUDWorkflowMode.delete(mode_ids, db=db)
        for mid in mode_ids:
            mode = await db.scalar(select(WorkflowMode).where(WorkflowMode.mode_id_bytes == mid.bytes))
            assert mode is None

    @pytest.mark.asyncio
    async def test_delete_single_workflow_modes(self, db: AsyncSession, random_workflow_mode: WorkflowMode) -> None:
        """
        Test for deleting a single workflow modes.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """

        await CRUDWorkflowMode.delete([random_workflow_mode.mode_id], db=db)
        mode = await db.scalar(
            select(WorkflowMode).where(WorkflowMode.mode_id_bytes == random_workflow_mode.mode_id.bytes)
        )
        assert mode is None
