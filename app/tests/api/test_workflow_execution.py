from io import BytesIO
from uuid import uuid4

import pytest
from clowmdb.models import ResourceVersion, Workflow, WorkflowExecution, WorkflowMode, WorkflowVersion
from fastapi import status
from httpx import AsyncClient
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.config import settings
from app.git_repository import build_repository
from app.schemas.workflow import WorkflowOut
from app.schemas.workflow_execution import DevWorkflowExecutionIn, WorkflowExecutionIn, WorkflowExecutionOut
from app.schemas.workflow_mode import WorkflowModeIn
from app.scm import SCM, SCMProvider
from app.tests.mocks import MockS3ServiceResource, MockSlurmCluster
from app.tests.utils.cleanup import CleanupList
from app.tests.utils.user import UserWithAuthHeader
from app.tests.utils.utils import random_hex_string, random_lower_string


class _TestWorkflowExecutionRoutes:
    base_path: str = "/workflow_executions"


class TestWorkflowExecutionRoutesCreate(_TestWorkflowExecutionRoutes):
    @pytest.mark.asyncio
    async def test_start_github_workflow_execution(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        random_workflow: WorkflowOut,
        mock_s3_service: MockS3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
    ) -> None:
        """
        Test for starting a workflow execution from a public GitHub repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        """
        execution_in = WorkflowExecutionIn(workflow_version_id=random_workflow_version.git_commit_hash, parameters={})
        response = await client.post(
            self.base_path, headers=random_user.auth_headers, json=execution_in.model_dump(exclude_none=True)
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        assert execution_response.workflow_version_id == execution_in.workflow_version_id
        assert execution_response.executor_id == random_user.user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING
        assert execution_response.workflow_version_id == random_workflow_version.git_commit_hash

        assert (
            f"params-{execution_response.execution_id.hex }.json"
            in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        )
        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["name"] == execution_response.execution_id.hex
        assert job["job"]["current_working_directory"] == settings.cluster.working_directory
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]
        assert "NXF_SCM_FILE" not in job["job"]["environment"].keys()

        nextflow_script = job["script"]
        assert "-hub github" in nextflow_script
        assert "-entry" not in nextflow_script
        assert f"-revision {random_workflow_version.git_commit_hash}" in nextflow_script
        assert f"run {random_workflow.repository_url}" in nextflow_script
        assert "export NXF_SCM_FILE" not in nextflow_script
        assert "-with-report" not in nextflow_script
        assert "-with-timeline" not in nextflow_script
        assert ">> $S3COMMANDFILE" not in nextflow_script

    @pytest.mark.asyncio
    async def test_start_private_github_workflow_execution(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        random_private_workflow: WorkflowOut,
        mock_s3_service: MockS3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
    ) -> None:
        """
        Test for starting a workflow execution from a private GitHub repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        random_private_workflow : app.schemas.workflow.WorkflowOut
            Random private workflow for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        """

        execution_in = WorkflowExecutionIn(workflow_version_id=random_workflow_version.git_commit_hash, parameters={})
        response = await client.post(
            self.base_path, headers=random_user.auth_headers, json=execution_in.model_dump(exclude_none=True)
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        assert execution_response.workflow_version_id == execution_in.workflow_version_id
        assert execution_response.executor_id == random_user.user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING
        assert execution_response.workflow_version_id == random_workflow_version.git_commit_hash

        assert (
            f"params-{execution_response.execution_id.hex}.json"
            in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        )

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]

        nextflow_script = job["script"]
        assert "-hub github" in nextflow_script
        assert "-entry" not in nextflow_script
        assert "export NXF_SCM_FILE" in nextflow_script
        assert f"-revision {random_workflow_version.git_commit_hash}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_gitlab_workflow_execution(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow: WorkflowOut,
        random_workflow_version: WorkflowVersion,
        mock_s3_service: MockS3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for starting a workflow execution from a public GitLab repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        scm_obj = mock_s3_service.Bucket(settings.s3.params_bucket).Object(
            SCM.generate_filename(random_workflow.workflow_id)
        )
        cleanup.add_task(scm_obj.delete)
        stmt = (
            update(Workflow)
            .where(Workflow.workflow_id_bytes == random_workflow_version.workflow_id.bytes)
            .values(repository_url="https://gitlab.com/example/example")
        )
        await db.execute(stmt)
        await db.commit()

        # Upload SCM file to PARAMS_BUCKET
        scm_provider = SCMProvider.from_repo(
            build_repository(random_workflow.repository_url, random_workflow_version.git_commit_hash),
            name=SCMProvider.generate_name(random_workflow.workflow_id),
        )
        assert scm_provider is not None
        with BytesIO() as f:
            SCM([scm_provider]).serialize(f)
            f.seek(0)
            scm_obj.upload_fileobj(f)

        execution_in = WorkflowExecutionIn(workflow_version_id=random_workflow_version.git_commit_hash, parameters={})
        response = await client.post(
            self.base_path, headers=random_user.auth_headers, json=execution_in.model_dump(exclude_none=True)
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        assert execution_response.workflow_version_id == execution_in.workflow_version_id
        assert execution_response.executor_id == random_user.user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING
        assert execution_response.workflow_version_id == random_workflow_version.git_commit_hash

        assert (
            f"params-{execution_response.execution_id.hex }.json"
            in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        )

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]

        nextflow_script = job["script"]
        assert f"-hub {SCMProvider.generate_name(random_workflow.workflow_id)}" in nextflow_script
        assert "-entry" not in nextflow_script
        assert "export NXF_SCM_FILE" in nextflow_script
        assert f"-revision {random_workflow_version.git_commit_hash}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_too_many_workflow_executions(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
    ) -> None:
        """
        Test for starting too many workflow executions.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        """
        active_execution_counter = 0
        while active_execution_counter < settings.cluster.active_workflow_execution_limit:
            execution = WorkflowExecution(
                executor_id_bytes=random_user.user.uid.bytes,
                workflow_version_id=random_workflow_version.git_commit_hash,
                slurm_job_id=1,
            )
            db.add(execution)
            active_execution_counter += 1
        await db.commit()
        execution_in = WorkflowExecutionIn(workflow_version_id=random_workflow_version.git_commit_hash, parameters={})
        response = await client.post(
            self.base_path, headers=random_user.auth_headers, json=execution_in.model_dump(exclude_none=True)
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_start_workflow_execution_with_unknown_workflow_version(
        self, client: AsyncClient, random_user: UserWithAuthHeader
    ) -> None:
        """
        Test for starting a workflow execution with an unknown workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_hex_string(),
            parameters={},
        )
        response = await client.post(
            self.base_path, headers=random_user.auth_headers, json=execution_in.model_dump(exclude_none=True)
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_start_workflow_execution_with_deprecated_workflow_version(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
    ) -> None:
        """
        Test for starting a workflow execution with a deprecated workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        """
        await db.execute(
            update(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
            .values(status=WorkflowVersion.Status.DEPRECATED.name)
        )
        await db.commit()
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={},
        )
        response = await client.post(
            self.base_path, headers=random_user.auth_headers, json=execution_in.model_dump(exclude_none=True)
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_start_workflow_execution_with_workflow_mode(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        random_workflow_mode: WorkflowMode,
        mock_slurm_cluster: MockSlurmCluster,
    ) -> None:
        """
        Test for starting a workflow execution with a workflow mode

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        """
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={},
            mode_id=random_workflow_mode.mode_id,
        )
        response = await client.post(
            self.base_path, headers=random_user.auth_headers, json=execution_in.model_dump(exclude_none=True)
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        assert execution_response.mode_id == random_workflow_mode.mode_id

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]

        nextflow_script = job["script"]
        assert f"-entry {random_workflow_mode.entrypoint}" in nextflow_script
        assert f"-revision {random_workflow_version.git_commit_hash}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_workflow_execution_with_non_existing_workflow_mode(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        mock_slurm_cluster: MockSlurmCluster,
    ) -> None:
        """
        Test for starting a workflow execution with a non-existing workflow mode

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        """
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash, parameters={}, mode_id=uuid4()
        ).model_dump()
        response = await client.post(self.base_path, headers=random_user.auth_headers, json=execution_in)
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_start_workflow_execution_without_required_workflow_mode(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        random_workflow_mode: WorkflowMode,
        mock_slurm_cluster: MockSlurmCluster,
    ) -> None:
        """
        Test for starting a workflow execution without specifying a required workflow mode

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        """
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={},
        ).model_dump(exclude_none=True)
        response = await client.post(self.base_path, headers=random_user.auth_headers, json=execution_in)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_start_workflow_execution_with_resource(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        random_resource_version_states: ResourceVersion,
    ) -> None:
        """
        Test for starting a workflow execution with a resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_resource_version_states : app.schemas.workflow.ResourceVersion
            Random resource version with all possible states for testing.
        """
        parameter = f"/some/path/CLDB-{random_resource_version_states.resource_id.hex}/"
        if random_resource_version_states.status is ResourceVersion.Status.LATEST:
            parameter += "latest"
        else:
            parameter += random_resource_version_states.resource_version_id.hex

        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={"someparameter": parameter},
        )
        response = await client.post(
            self.base_path, headers=random_user.auth_headers, json=execution_in.model_dump(exclude_none=True)
        )
        if random_resource_version_states.status in [
            ResourceVersion.Status.SYNCHRONIZED,
            ResourceVersion.Status.LATEST,
            ResourceVersion.Status.SETTING_LATEST,
        ]:
            assert response.status_code == status.HTTP_201_CREATED
        else:
            assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_start_workflow_execution_non_existing_resource(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
    ) -> None:
        """
        Test for starting a workflow execution with a non-existing resource.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        execution_in = WorkflowExecutionIn(
            workflow_version_id=random_workflow_version.git_commit_hash,
            parameters={"someparameter": f"/some/path/CLDB-{uuid4().hex}/latest"},
        )
        response = await client.post(
            self.base_path, headers=random_user.auth_headers, json=execution_in.model_dump(exclude_none=True)
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_start_execution_with_slurm_error(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        random_workflow: WorkflowOut,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for starting a workflow execution from a public GitHub repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        execution_in = WorkflowExecutionIn(workflow_version_id=random_workflow_version.git_commit_hash, parameters={})
        mock_slurm_cluster.send_error = True

        def repair_slurm_cluster() -> None:
            mock_slurm_cluster.send_error = False

        cleanup.add_task(repair_slurm_cluster)
        response = await client.post(
            self.base_path,
            headers=random_user.auth_headers,
            json=execution_in.model_dump(exclude_none=True),
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_id = WorkflowExecutionOut.model_validate_json(response.content).execution_id

        stmt = select(WorkflowExecution).where(WorkflowExecution.execution_id_bytes == execution_id.bytes)
        execution_db = await db.scalar(stmt)
        assert execution_db
        assert execution_db.status == WorkflowExecution.WorkflowExecutionStatus.ERROR

    @pytest.mark.asyncio
    async def test_start_execution_broken_workflow_bucket(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        random_workflow: WorkflowOut,
        mock_s3_service: MockS3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
    ) -> None:
        """
        Test for starting a workflow execution from a public GitHub repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        """
        mock_s3_service.Bucket(settings.s3.workflow_bucket).Object(
            f"{random_workflow_version.git_commit_hash}.json"
        ).delete()
        execution_in = WorkflowExecutionIn(workflow_version_id=random_workflow_version.git_commit_hash, parameters={})
        response = await client.post(
            self.base_path, headers=random_user.auth_headers, json=execution_in.model_dump(exclude_none=True)
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None

        # raises an exception if the object does not exist in the bucket
        assert (
            mock_s3_service.Bucket(settings.s3.workflow_bucket)
            .Object(f"{random_workflow_version.git_commit_hash}.json")
            .load()  # type: ignore[func-returns-value]
            is None
        )


class TestDevWorkflowExecutionRoutesCreate(_TestWorkflowExecutionRoutes):
    @pytest.mark.asyncio
    async def test_start_dev_workflow_execution_from_github_with_mode(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
    ) -> None:
        """
        Test for starting a workflow execution with an arbitrary GitHub repository and a workflow mode.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        """
        mode = WorkflowModeIn(
            name=random_lower_string(10), entrypoint=random_lower_string(16), schema_path=random_lower_string(16)
        )
        execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(),
            repository_url="https://github.com/example-user/example",
            parameters={},
            mode=mode,
        )
        response = await client.post(
            f"{self.base_path}/arbitrary", headers=random_user.auth_headers, json=execution_in.model_dump()
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        assert execution_response.executor_id == random_user.user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        assert (
            f"params-{execution_response.execution_id.hex }.json"
            in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        )

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]
        assert "NXF_SCM_FILE" not in job["job"]["environment"].keys()

        nextflow_script = job["script"]
        assert "-hub github" in nextflow_script
        assert f"-entry {mode.entrypoint}" in nextflow_script
        assert f"-revision {execution_in.git_commit_hash}" in nextflow_script
        assert "export NXF_SCM_FILE" not in nextflow_script
        assert f"run {execution_in.repository_url}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_dev_workflow_execution_from_github(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
    ) -> None:
        """
        Test for starting a workflow execution with an arbitrary GitHub repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        """
        execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(), repository_url="https://github.com/example-user/example", parameters={}
        )
        response = await client.post(
            f"{self.base_path}/arbitrary", headers=random_user.auth_headers, json=execution_in.model_dump()
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        assert execution_response.executor_id == random_user.user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        assert (
            f"params-{execution_response.execution_id.hex}.json"
            in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        )

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]
        assert "NXF_SCM_FILE" not in job["job"]["environment"].keys()

        nextflow_script = job["script"]
        assert "-hub github" in nextflow_script
        assert "-entry" not in nextflow_script
        assert "export NXF_SCM_FILE" not in nextflow_script
        assert f"-revision {execution_in.git_commit_hash}" in nextflow_script
        assert f"run {execution_in.repository_url}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_dev_workflow_execution_from_gitlab(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for starting a workflow execution with an arbitrary Gitlab repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(), repository_url="https://gitlab.com/example-user/example", parameters={}
        )
        response = await client.post(
            f"{self.base_path}/arbitrary", headers=random_user.auth_headers, json=execution_in.model_dump()
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        scm_obj = mock_s3_service.Bucket(settings.s3.params_bucket).Object(
            SCM.generate_filename(execution_response.execution_id)
        )
        cleanup.add_task(scm_obj.delete)

        assert execution_response.executor_id == random_user.user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        # Check if params file is created
        params_file_name = f"params-{execution_response.execution_id.hex}.json"
        assert params_file_name in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        cleanup.add_task(mock_s3_service.Bucket(settings.s3.params_bucket).Object(params_file_name).delete)

        with BytesIO() as f:
            scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        # Check content of SCM file
        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.password is None
        assert provider.name == SCMProvider.generate_name(execution_response.execution_id)
        assert provider.platform == "gitlab"
        assert provider.server == "https://gitlab.com"

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]

        nextflow_script = job["script"]
        assert f"-hub {SCMProvider.generate_name(execution_response.execution_id)}" in nextflow_script
        assert "-entry" not in nextflow_script
        assert f"-revision {execution_in.git_commit_hash}" in nextflow_script
        assert f"run {execution_in.repository_url}" in nextflow_script
        assert "export NXF_SCM_FILE" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_dev_workflow_execution_from_private_gitlab(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for starting a workflow execution with an arbitrary private Gitlab repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        token = random_lower_string(15)
        execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(),
            repository_url="https://gitlab.com/example-user/example",
            parameters={},
            token=token,
        )
        response = await client.post(
            f"{self.base_path}/arbitrary", headers=random_user.auth_headers, json=execution_in.model_dump()
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)

        assert execution_response.executor_id == random_user.user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        # Check if params file is created
        params_file_name = f"params-{execution_response.execution_id.hex}.json"
        assert params_file_name in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        cleanup.add_task(mock_s3_service.Bucket(settings.s3.params_bucket).Object(params_file_name).delete)

        # Check if SCM file is created
        scm_file_name = SCM.generate_filename(execution_response.execution_id)
        assert scm_file_name in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        scm_obj = mock_s3_service.Bucket(settings.s3.params_bucket).Object(scm_file_name)
        cleanup.add_task(scm_obj.delete)
        with BytesIO() as f:
            scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        # Check content of SCM file
        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.password == token
        assert provider.name == SCMProvider.generate_name(execution_response.execution_id)
        assert provider.platform == "gitlab"
        assert provider.server == "https://gitlab.com"

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]

        nextflow_script = job["script"]
        assert f"-hub {SCMProvider.generate_name(execution_response.execution_id)}" in nextflow_script
        assert "-entry" not in nextflow_script
        assert f"-revision {execution_in.git_commit_hash}" in nextflow_script
        assert "export NXF_SCM_FILE" in nextflow_script
        assert f"run {execution_in.repository_url}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_dev_workflow_execution_from_private_github(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        mock_slurm_cluster: MockSlurmCluster,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for starting a workflow execution with an arbitrary private GitHub repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        token = random_lower_string(15)
        execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(),
            repository_url="https://github.com/example-user/example",
            parameters={},
            token=token,
        )
        response = await client.post(
            f"{self.base_path}/arbitrary", headers=random_user.auth_headers, json=execution_in.model_dump()
        )
        assert response.status_code == status.HTTP_201_CREATED
        execution_response = WorkflowExecutionOut.model_validate_json(response.content)
        assert execution_response.executor_id == random_user.user.uid
        assert execution_response.status == WorkflowExecution.WorkflowExecutionStatus.PENDING

        # Check if params file is created
        params_file_name = f"params-{execution_response.execution_id.hex}.json"
        assert params_file_name in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        cleanup.add_task(mock_s3_service.Bucket(settings.s3.params_bucket).Object(params_file_name).delete)

        # Check if SCM file is created
        scm_file_name = SCM.generate_filename(execution_response.execution_id)
        assert scm_file_name in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        scm_obj = mock_s3_service.Bucket(settings.s3.params_bucket).Object(scm_file_name)
        cleanup.add_task(scm_obj.delete)
        with BytesIO() as f:
            scm_obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        # Check content of SCM file
        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.password == token
        assert provider.name == "github"
        assert provider.user == "example-user"

        job = mock_slurm_cluster.get_job_by_name(execution_response.execution_id)
        assert job is not None
        assert job["job"]["environment"]["TOWER_WORKSPACE_ID"] == execution_response.execution_id.hex[:16]

        nextflow_script = job["script"]
        assert "-hub github" in nextflow_script
        assert "-entry" not in nextflow_script
        assert f"-revision {execution_in.git_commit_hash}" in nextflow_script
        assert "export NXF_SCM_FILE" in nextflow_script
        assert f"run {execution_in.repository_url}" in nextflow_script

    @pytest.mark.asyncio
    async def test_start_dev_workflow_execution_with_unknown_repository(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
    ) -> None:
        """
        Test for starting a workflow execution with an unknown git repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        execution_in = DevWorkflowExecutionIn(
            git_commit_hash=random_hex_string(), repository_url="https://bitbucket.com/example/example", parameters={}
        )
        response = await client.post(
            f"{self.base_path}/arbitrary", headers=random_user.auth_headers, json=execution_in.model_dump()
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST


class TestWorkflowExecutionRoutesGet(_TestWorkflowExecutionRoutes):
    @pytest.mark.asyncio
    async def test_get_workflow_execution(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_running_workflow_execution: WorkflowExecution,
    ) -> None:
        """
        Test for getting a workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_running_workflow_execution.execution_id)]),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_200_OK

        execution = response.json()
        assert execution["execution_id"] == str(random_running_workflow_execution.execution_id)

    @pytest.mark.asyncio
    async def test_get_workflow_execution_params(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_running_workflow_execution: WorkflowExecution,
    ) -> None:
        """
        Test for getting the parameters of a workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_running_workflow_execution.execution_id), "params"]),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_200_OK

        execution = response.json()
        assert len(execution) == 0

    @pytest.mark.asyncio
    async def test_get_non_existing_workflow_execution(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for getting a non-existing workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        response = await client.get("/".join([self.base_path, str(uuid4())]), headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestWorkflowExecutionRoutesList(_TestWorkflowExecutionRoutes):
    @pytest.mark.asyncio
    async def test_list_workflow_executions(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_running_workflow_execution: WorkflowExecution,
    ) -> None:
        """
        Test for listing all workflow executions.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        response = await client.get(self.base_path, headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_200_OK

        executions = response.json()
        assert len(executions) > 0
        assert (
            sum(
                1
                for execution in executions
                if execution["execution_id"] == str(random_running_workflow_execution.execution_id)
            )
            == 1
        )


class TestWorkflowExecutionRoutesDelete(_TestWorkflowExecutionRoutes):
    @pytest.mark.asyncio
    async def test_delete_unfinished_workflow_execution(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_running_workflow_execution: WorkflowExecution,
    ) -> None:
        """
        Test for deleting an unfinished workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_running_workflow_execution.execution_id)]),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_delete_finished_workflow_execution(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthHeader,
        random_running_workflow_execution: WorkflowExecution,
    ) -> None:
        """
        Test for deleting a workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        await db.execute(
            update(WorkflowExecution)
            .where(WorkflowExecution.execution_id_bytes == random_running_workflow_execution.execution_id.bytes)
            .values(status=WorkflowExecution.WorkflowExecutionStatus.SUCCESS.name)
        )
        await db.commit()
        response = await client.delete(
            "/".join([self.base_path, str(random_running_workflow_execution.execution_id)]),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT


class TestWorkflowExecutionRoutesCancel(_TestWorkflowExecutionRoutes):
    @pytest.mark.asyncio
    async def test_cancel_workflow_execution(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_running_workflow_execution: WorkflowExecution,
        mock_slurm_cluster: MockSlurmCluster,
    ) -> None:
        """
        Test for canceling a workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        mock_slurm_cluster : app.tests.mocks.mock_slurm_cluster.MockSlurmCluster
            Mock Slurm cluster to inspect submitted jobs.
        """
        response = await client.post(
            "/".join([self.base_path, str(random_running_workflow_execution.execution_id), "cancel"]),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        job_active = mock_slurm_cluster.job_active(random_running_workflow_execution.slurm_job_id)
        assert not job_active

    @pytest.mark.asyncio
    async def test_cancel_finished_workflow_execution(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthHeader,
        random_running_workflow_execution: WorkflowExecution,
    ) -> None:
        """
        Test for canceling a finished workflow execution.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing.
        """
        await db.execute(
            update(WorkflowExecution)
            .where(WorkflowExecution.execution_id_bytes == random_running_workflow_execution.execution_id.bytes)
            .values(status=WorkflowExecution.WorkflowExecutionStatus.SUCCESS.name)
        )
        await db.commit()
        response = await client.post(
            "/".join([self.base_path, str(random_running_workflow_execution.execution_id), "cancel"]),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST
