from datetime import date
from io import BytesIO
from uuid import uuid4

import pytest
from clowmdb.models import Workflow, WorkflowExecution, WorkflowMode, WorkflowVersion
from fastapi import status
from httpx import AsyncClient
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.config import settings
from app.schemas.workflow import WorkflowIn, WorkflowOut, WorkflowStatistic, WorkflowUpdate
from app.schemas.workflow_execution import AnonymizedWorkflowExecution
from app.schemas.workflow_mode import WorkflowModeIn
from app.schemas.workflow_version import WorkflowVersion as WorkflowVersionOut
from app.scm import SCM, SCMProvider
from app.tests.mocks import DefaultMockHTTPService, MockS3ServiceResource
from app.tests.utils.cleanup import CleanupList, delete_workflow, delete_workflow_mode
from app.tests.utils.user import UserWithAuthHeader
from app.tests.utils.utils import random_hex_string, random_lower_string


class _TestWorkflowRoutes:
    base_path: str = "/workflows"


class TestWorkflowRoutesCreate(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_create_workflow_with_github(
        self, db: AsyncSession, client: AsyncClient, random_user: UserWithAuthHeader, cleanup: CleanupList
    ) -> None:
        """
        Exhaustive Test for successfully creating a workflow.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        git_commit_hash = random_hex_string()
        workflow = WorkflowIn(
            git_commit_hash=git_commit_hash,
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.de/example-user/example",
        ).model_dump()
        response = await client.post(self.base_path, json=workflow, headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_201_CREATED
        created_workflow = WorkflowOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow, db=db, workflow_id=created_workflow.workflow_id)
        assert not created_workflow.private

        stmt = select(Workflow).where(Workflow.workflow_id_bytes == created_workflow.workflow_id.bytes)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None

    @pytest.mark.asyncio
    async def test_create_workflow_with_gitlab(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for successfully creating a workflow with a Gitlab repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        workflow = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://gitlab.de/example-user/example",
        ).model_dump()
        response = await client.post(self.base_path, json=workflow, headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_201_CREATED
        created_workflow = WorkflowOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow, db=db, workflow_id=created_workflow.workflow_id)
        assert not created_workflow.private

        stmt = select(Workflow).where(Workflow.workflow_id_bytes == created_workflow.workflow_id.bytes)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None

        # Download SCM file in PARAMS_BUCKET that should be created
        scm_file_name = SCM.generate_filename(db_workflow.workflow_id)
        assert scm_file_name in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        obj = mock_s3_service.Bucket(settings.s3.params_bucket).Object(scm_file_name)
        cleanup.add_task(obj.delete)
        with BytesIO() as f:
            obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        # Check content of SCM file
        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.password is None
        assert provider.name == SCMProvider.generate_name(db_workflow.workflow_id)
        assert provider.platform == "gitlab"
        assert provider.server == "https://gitlab.de"

    @pytest.mark.asyncio
    async def test_create_workflow_with_private_gitlab(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for successfully creating a workflow with a private Gitlab repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        token = random_lower_string(20)
        workflow = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://gitlab.de/example-user/example",
            token=token,
        ).model_dump()
        response = await client.post(self.base_path, json=workflow, headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_201_CREATED
        created_workflow = WorkflowOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow, db=db, workflow_id=created_workflow.workflow_id)
        assert created_workflow.private

        # Check if workflow is created in database
        stmt = select(Workflow).where(Workflow.workflow_id_bytes == created_workflow.workflow_id.bytes)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None
        # Check if token is saved
        assert db_workflow.credentials_token == token

        # Download SCM file in PARAMS_BUCKET that should be created
        scm_file_name = SCM.generate_filename(db_workflow.workflow_id)
        assert scm_file_name in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        obj = mock_s3_service.Bucket(settings.s3.params_bucket).Object(scm_file_name)
        cleanup.add_task(obj.delete)
        with BytesIO() as f:
            obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        # Check content of SCM file
        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.password == token
        assert provider.name == SCMProvider.generate_name(db_workflow.workflow_id)
        assert provider.platform == "gitlab"
        assert provider.server == "https://gitlab.de"

    @pytest.mark.asyncio
    async def test_create_workflow_with_private_github(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for successfully creating a workflow with a private GitHub repository.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        token = random_lower_string(20)
        workflow = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.de/example-user/example",
            token=token,
        ).model_dump()
        response = await client.post(self.base_path, json=workflow, headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_201_CREATED
        created_workflow = WorkflowOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow, db=db, workflow_id=created_workflow.workflow_id)
        assert created_workflow.private

        # Check if workflow is created in database
        stmt = select(Workflow).where(Workflow.workflow_id_bytes == created_workflow.workflow_id.bytes)
        db_workflow = await db.scalar(stmt)
        assert db_workflow is not None
        # Check if token is saved
        assert db_workflow.credentials_token == token

        # Download SCM file in PARAMS_BUCKET that should be created
        scm_file_name = SCM.generate_filename(db_workflow.workflow_id)
        assert scm_file_name in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()
        obj = mock_s3_service.Bucket(settings.s3.params_bucket).Object(scm_file_name)
        cleanup.add_task(obj.delete)
        with BytesIO() as f:
            obj.download_fileobj(f)
            f.seek(0)
            scm = SCM.deserialize(f)

        # Check content of SCM file
        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.password == token
        assert provider.name == "github"
        assert provider.user == "example-user"

    @pytest.mark.asyncio
    async def test_create_workflow_with_error(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        mock_default_http_server: DefaultMockHTTPService,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for creating a workflow where the file checks don't pass

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_default_http_server : app.tests.mocks.DefaultMockHTTPService
            Mock http service for testing
        """
        mock_default_http_server.send_error = True
        cleanup.add_task(mock_default_http_server.reset)
        workflow = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.de/example-user/example",
        ).model_dump()
        response = await client.post(
            self.base_path, params={"raise_error": True}, json=workflow, headers=random_user.auth_headers
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

        db_workflows = (await db.scalars(select(Workflow))).all()
        assert len(db_workflows) == 0

    @pytest.mark.asyncio
    async def test_create_workflow_with_taken_name(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for creating a workflow with an already taken name.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random bucket for testing.
        """
        workflow = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_workflow.name,
            short_description=random_lower_string(65),
            repository_url="https://github.de/example-user/example",
        ).model_dump()
        response = await client.post(self.base_path, json=workflow, headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_create_workflow_with_registered_git_commit(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for creating a workflow where the git commit is already in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        workflow = WorkflowIn(
            git_commit_hash=random_workflow.versions[0].workflow_version_id,
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.de/example-user/example",
        ).model_dump()
        response = await client.post(self.base_path, json=workflow, headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_create_workflow_with_unknown_git_repository(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for creating a workflow with an unknown git repository.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        workflow = WorkflowIn(
            git_commit_hash=random_hex_string(),
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://example.org",
        ).model_dump()
        response = await client.post(self.base_path, json=workflow, headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_create_workflow_with_workflow_mode(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        mock_s3_service: MockS3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Exhaustive Test for successfully creating a workflow with a workflow mode.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        git_commit_hash = random_hex_string()
        workflow_mode = WorkflowModeIn(
            name=random_lower_string(), entrypoint=random_lower_string(), schema_path=random_lower_string()
        )
        workflow = WorkflowIn(
            git_commit_hash=git_commit_hash,
            name=random_lower_string(10),
            short_description=random_lower_string(65),
            repository_url="https://github.de/example-user/example",
            modes=[workflow_mode, workflow_mode],
        )
        response = await client.post(self.base_path, json=workflow.model_dump(), headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_201_CREATED
        created_workflow = WorkflowOut.model_validate_json(response.content)
        cleanup.add_task(delete_workflow, db=db, workflow_id=created_workflow.workflow_id)
        assert len(created_workflow.versions[0].modes) == 2
        mode_id = created_workflow.versions[0].modes[0]
        cleanup.add_task(delete_workflow_mode, db=db, mode_id=mode_id)

        stmt = select(WorkflowMode).where(WorkflowMode.mode_id_bytes == mode_id.bytes)
        db_mode = await db.scalar(stmt)
        assert db_mode is not None
        assert db_mode.name == workflow_mode.name
        assert db_mode.entrypoint == workflow_mode.entrypoint
        assert db_mode.schema_path == workflow_mode.schema_path

        assert (
            mock_s3_service.Bucket(settings.s3.workflow_bucket)  # type: ignore[func-returns-value]
            .Object(f"{workflow.git_commit_hash}-{db_mode.mode_id.hex}.json")
            .load()
            is None
        )


class TestWorkflowRoutesList(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_list_all_unpublished_workflows(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for listing all workflows in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            self.base_path,
            headers=random_user.auth_headers,
            params={"version_status": WorkflowVersion.Status.CREATED.name},
        )
        assert response.status_code == status.HTTP_200_OK
        workflows = response.json()
        assert len(workflows) == 1
        assert workflows[0]["workflow_id"] == str(random_workflow.workflow_id)
        assert not workflows[0]["private"]

    @pytest.mark.asyncio
    async def test_list_all_workflows(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for listing all workflows in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            self.base_path,
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_200_OK
        workflows = response.json()
        assert len(workflows) == 0

    @pytest.mark.asyncio
    async def test_list_workflows_by_developer(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow: WorkflowOut,
        random_second_user: UserWithAuthHeader,
    ) -> None:
        """
        Test for listing all workflows in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            self.base_path,
            headers=random_second_user.auth_headers,
            params={"developer_id": str(random_user.user.uid), "version_status": WorkflowVersion.Status.CREATED.name},
        )
        assert response.status_code == status.HTTP_200_OK
        workflows = response.json()
        assert len(workflows) == 1
        assert workflows[0]["workflow_id"] == str(random_workflow.workflow_id)

    @pytest.mark.asyncio
    async def test_list_workflow_statistics(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow: WorkflowOut,
        random_completed_workflow_execution: WorkflowExecution,
    ) -> None:
        """
        Test for getting all workflow executions as workflow statistics.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_completed_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing
        """
        response = await client.get(
            f"{self.base_path}/developer_statistics",
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_200_OK
        executions = response.json()
        assert len(executions) == 1
        execution = AnonymizedWorkflowExecution(**executions[0])
        assert execution.workflow_id == random_workflow.workflow_id
        assert execution.workflow_execution_id == random_completed_workflow_execution.execution_id
        assert execution.workflow_version_id == random_completed_workflow_execution.workflow_version_id


class TestWorkflowRoutesGet(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_get_workflow_with_version_status(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for getting a workflow by its id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_workflow.workflow_id)]),
            params={"version_status": random_workflow.versions[0].status.name},
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_200_OK
        workflow = WorkflowOut.model_validate_json(response.content)
        assert workflow.workflow_id == random_workflow.workflow_id
        assert not workflow.private
        assert len(workflow.versions) > 0

    @pytest.mark.asyncio
    async def test_get_non_existing_workflow(self, client: AsyncClient, random_user: UserWithAuthHeader) -> None:
        """
        Test for getting a non-existing workflow.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(uuid4())]),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_workflow_statistics(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow: WorkflowOut,
        random_running_workflow_execution: WorkflowExecution,
    ) -> None:
        """
        Test for getting the aggregated workflow statistics.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_running_workflow_execution : clowmdb.models.WorkflowExecution
            Random workflow execution for testing
        """
        response = await client.get(
            "/".join([self.base_path, str(random_workflow.workflow_id), "statistics"]),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_200_OK
        statistics = response.json()
        assert len(statistics) == 1
        stat = WorkflowStatistic.model_validate(statistics[0])
        assert stat.day == date.today()
        assert stat.count == 1


class TestWorkflowRoutesDelete(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_delete_workflow(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow: WorkflowOut,
        mock_s3_service: MockS3ServiceResource,
    ) -> None:
        """
        Test for deleting a workflow by its id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_workflow.workflow_id)]),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        icon_slug = str(random_workflow.versions[0].icon_url).split("/")[-1]
        assert icon_slug not in mock_s3_service.Bucket(settings.s3.icon_bucket).objects.all_keys()
        schema_file = random_workflow.versions[0].workflow_version_id + ".json"
        assert schema_file not in mock_s3_service.Bucket(settings.s3.workflow_bucket).objects.all_keys()

    @pytest.mark.asyncio
    async def test_delete_private_workflow(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_private_workflow: WorkflowOut,
        mock_s3_service: MockS3ServiceResource,
    ) -> None:
        """
        Test for deleting a workflow by its id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_private_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_private_workflow.workflow_id)]),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        icon_slug = str(random_private_workflow.versions[0].icon_url).split("/")[-1]
        assert icon_slug not in mock_s3_service.Bucket(settings.s3.icon_bucket).objects.all_keys()
        schema_file = random_private_workflow.versions[0].workflow_version_id + ".json"
        assert schema_file not in mock_s3_service.Bucket(settings.s3.workflow_bucket).objects.all_keys()
        scm_file = SCM.generate_filename(random_private_workflow.workflow_id)
        assert scm_file not in mock_s3_service.Bucket(settings.s3.params_bucket).objects.all_keys()

    @pytest.mark.asyncio
    async def test_delete_workflow_with_mode(
        self,
        client: AsyncClient,
        db: AsyncSession,
        random_user: UserWithAuthHeader,
        random_workflow: WorkflowOut,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: MockS3ServiceResource,
    ) -> None:
        """
        Test for deleting a workflow by its id.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        random_workflow_mode : clowmdb.model.WorkflowMode
            Random workflow mode for testing
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response = await client.delete(
            "/".join([self.base_path, str(random_workflow.workflow_id)]),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        icon_slug = str(random_workflow.versions[0].icon_url).split("/")[-1]
        assert icon_slug not in mock_s3_service.Bucket(settings.s3.icon_bucket).objects.all_keys()
        schema_file = f"{random_workflow.versions[0].workflow_version_id}-{random_workflow_mode.mode_id.hex}.json"
        assert schema_file not in mock_s3_service.Bucket(settings.s3.workflow_bucket).objects.all_keys()

        mode_db = await db.scalar(
            select(WorkflowMode).where(WorkflowMode.mode_id_bytes == random_workflow_mode.mode_id.bytes)
        )
        assert mode_db is None


class TestWorkflowRoutesUpdate(_TestWorkflowRoutes):
    @pytest.mark.asyncio
    async def test_update_workflow(
        self, db: AsyncSession, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for successfully updating a workflow.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        git_commit_hash = random_hex_string()
        version_update = WorkflowUpdate(
            git_commit_hash=git_commit_hash,
            version=random_lower_string(8),
        ).model_dump()
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            json=version_update,
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_201_CREATED
        created_version = WorkflowVersionOut.model_validate_json(response.content)
        assert created_version.workflow_version_id == git_commit_hash
        assert created_version.status == WorkflowVersion.Status.CREATED
        assert created_version.icon_url == random_workflow.versions[0].icon_url

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None
        assert db_version.status == WorkflowVersion.Status.CREATED

    @pytest.mark.asyncio
    async def test_update_workflow_with_append_modes(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow: WorkflowOut,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: MockS3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for successfully updating a workflow and adding new modes.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_mode : clowmdb.model.WorkflowMode
            Random workflow mode for testing
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        git_commit_hash = random_hex_string()
        version_update = WorkflowUpdate(
            git_commit_hash=git_commit_hash,
            version=random_lower_string(8),
            append_modes=[
                WorkflowModeIn(
                    name=random_lower_string(10), entrypoint=random_lower_string(16), schema_path=random_lower_string()
                )
            ],
        ).model_dump()
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            json=version_update,
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_201_CREATED
        created_version = WorkflowVersionOut.model_validate_json(response.content)
        assert created_version.workflow_version_id == git_commit_hash
        assert created_version.status == WorkflowVersion.Status.CREATED
        assert created_version.icon_url == random_workflow.versions[0].icon_url
        assert created_version.modes is not None
        assert len(created_version.modes) == 2

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None
        assert db_version.status == WorkflowVersion.Status.CREATED

        new_mode_id = next((m for m in created_version.modes if m != random_workflow_mode.mode_id), None)
        assert new_mode_id is not None
        cleanup.add_task(delete_workflow_mode, db=db, mode_id=new_mode_id)
        cleanup.add_task(
            mock_s3_service.Bucket(settings.s3.workflow_bucket)
            .Object(f"{git_commit_hash}-{new_mode_id.hex}.json")
            .delete
        )
        assert (
            f"{git_commit_hash}-{new_mode_id.hex}.json"
            in mock_s3_service.Bucket(settings.s3.workflow_bucket).objects.all_keys()
        )
        assert (
            f"{git_commit_hash}-{random_workflow_mode.mode_id.hex}.json"
            in mock_s3_service.Bucket(settings.s3.workflow_bucket).objects.all_keys()
        )

    @pytest.mark.asyncio
    async def test_update_workflow_with_delete_non_existing_modes(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow: WorkflowOut,
    ) -> None:
        """
        Test for updating a workflow and delete an non-existing mode.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        version_update = WorkflowUpdate(
            git_commit_hash=random_hex_string(), version=random_lower_string(8), delete_modes=[str(uuid4())]
        ).model_dump()
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            json=version_update,
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_update_workflow_with_delete_modes(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow: WorkflowOut,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: MockS3ServiceResource,
    ) -> None:
        """
        Test for successfully updating a workflow and delete an old mode.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_mode : clowmdb.model.WorkflowMode
            Random workflow mode for testing
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        """
        git_commit_hash = random_hex_string()
        version_update = WorkflowUpdate(
            git_commit_hash=git_commit_hash,
            version=random_lower_string(8),
            delete_modes=[random_workflow_mode.mode_id],
        )
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            json=version_update.model_dump(),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_201_CREATED
        created_version = WorkflowVersionOut.model_validate_json(response.content)
        assert created_version.workflow_version_id == git_commit_hash
        assert created_version.status == WorkflowVersion.Status.CREATED
        assert created_version.icon_url == random_workflow.versions[0].icon_url
        assert created_version.modes is None or len(created_version.modes) == 0

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None
        assert db_version.status == WorkflowVersion.Status.CREATED

        assert (
            f"{git_commit_hash}-{random_workflow_mode.mode_id.hex}.json"
            not in mock_s3_service.Bucket(settings.s3.workflow_bucket).objects.all_keys()
        )

    @pytest.mark.asyncio
    async def test_update_workflow_with_append_and_delete_modes(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow: WorkflowOut,
        random_workflow_mode: WorkflowMode,
        mock_s3_service: MockS3ServiceResource,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for successfully updating a workflow with adding a new mode and delete an old mode.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        random_workflow_mode : clowmdb.model.WorkflowMode
            Random workflow mode for testing
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        git_commit_hash = random_hex_string()
        version_update = WorkflowUpdate(
            git_commit_hash=git_commit_hash,
            version=random_lower_string(8),
            delete_modes=[str(random_workflow_mode.mode_id)],
            append_modes=[
                WorkflowModeIn(
                    name=random_lower_string(10), entrypoint=random_lower_string(16), schema_path=random_lower_string()
                )
            ],
        ).model_dump()
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            json=version_update,
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_201_CREATED
        created_version = WorkflowVersionOut.model_validate_json(response.content)
        assert created_version.workflow_version_id == git_commit_hash
        assert created_version.status == WorkflowVersion.Status.CREATED
        assert created_version.icon_url == random_workflow.versions[0].icon_url
        assert len(created_version.modes) == 1
        assert created_version.modes[0] != random_workflow_mode.mode_id

        stmt = select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == git_commit_hash)
        db_version = await db.scalar(stmt)
        assert db_version is not None
        assert db_version.status == WorkflowVersion.Status.CREATED

        mode_id = created_version.modes[0]
        cleanup.add_task(delete_workflow_mode, db=db, mode_id=mode_id)
        cleanup.add_task(
            mock_s3_service.Bucket(settings.s3.workflow_bucket).Object(f"{git_commit_hash}-{mode_id.hex}.json").delete
        )
        assert (
            f"{git_commit_hash}-{mode_id.hex}.json"
            in mock_s3_service.Bucket(settings.s3.workflow_bucket).objects.all_keys()
        )
        assert (
            f"{git_commit_hash}-{random_workflow_mode.mode_id.hex}.json"
            not in mock_s3_service.Bucket(settings.s3.workflow_bucket).objects.all_keys()
        )

    @pytest.mark.asyncio
    async def test_update_workflow_with_error(
        self,
        db: AsyncSession,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow: WorkflowOut,
        mock_default_http_server: DefaultMockHTTPService,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for updating a workflow where the file checks don't pass

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        mock_default_http_server : app.tests.mocks.DefaultMockHTTPService
            Mock http service for testing
        """
        mock_default_http_server.send_error = True
        cleanup.add_task(mock_default_http_server.reset)
        version_update = WorkflowUpdate(
            git_commit_hash=random_hex_string(),
            version=random_lower_string(8),
        ).model_dump()
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            params={"raise_error": True},
            json=version_update,
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

        db_workflows = (
            await db.scalars(
                select(WorkflowVersion).where(WorkflowVersion.workflow_id_bytes == random_workflow.workflow_id.bytes)
            )
        ).all()
        assert len(db_workflows) == 1

    @pytest.mark.asyncio
    async def test_update_workflow_with_registered_git_commit(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for updating a workflow where the git commit is already in the system.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        version_update = WorkflowUpdate(
            git_commit_hash=random_workflow.versions[0].workflow_version_id,
            version=random_lower_string(8),
        ).model_dump()
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            json=version_update,
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_update_workflow_as_foreign_user(
        self, client: AsyncClient, random_second_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for updating a workflow where the requested user is not the developer of the workflow.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random second user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        version_update = WorkflowUpdate(
            git_commit_hash=random_hex_string(),
            version=random_lower_string(8),
        ).model_dump()
        response = await client.post(
            "/".join([self.base_path, str(random_workflow.workflow_id), "update"]),
            json=version_update,
            headers=random_second_user.auth_headers,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN
