from io import BytesIO
from uuid import uuid4

import pytest
from clowmdb.models import WorkflowMode, WorkflowVersion
from fastapi import status
from httpx import AsyncClient
from PIL import Image
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.endpoints.workflow_version import DocumentationEnum
from app.core.config import settings
from app.schemas.workflow import WorkflowOut
from app.schemas.workflow_version import ParameterExtension
from app.schemas.workflow_version import WorkflowVersion as WorkflowVersionOut
from app.schemas.workflow_version import WorkflowVersionStatus
from app.tests.mocks import MockS3ServiceResource
from app.tests.utils.cleanup import CleanupList
from app.tests.utils.user import UserWithAuthHeader
from app.tests.utils.utils import random_hex_string


class _TestWorkflowVersionRoutes:
    base_path: str = "/workflows"


class TestWorkflowVersionRoutesGet(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    async def test_get_workflow_version(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for getting a workflow version by its commit hash.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                ]
            ),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_200_OK
        version = WorkflowVersionOut.model_validate_json(response.content)
        assert version.workflow_id == random_workflow.workflow_id
        assert version.workflow_version_id == random_workflow.versions[0].workflow_version_id

    @pytest.mark.asyncio
    async def test_get_non_existing_version(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for getting a non-existing workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_workflow.workflow_id), "versions", random_hex_string()]),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestWorkflowVersionRoutesList(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    async def test_get_workflow_version(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test for listing all workflow version from a given workflow.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.get(
            "/".join([self.base_path, str(random_workflow.workflow_id), "versions"]),
            params={"version_status": WorkflowVersion.Status.CREATED.name},
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_200_OK
        versions = response.json()
        assert len(versions) == 1
        version = WorkflowVersionOut.model_validate(versions[0])
        assert version.workflow_id == random_workflow.workflow_id
        assert version.workflow_version_id == random_workflow.versions[0].workflow_version_id


class TestWorkflowVersionRoutesUpdate(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    async def test_update_workflow_version_parameter_extension(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test updating a workflow version status parameter extension.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.patch(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "parameter-extension",
                ]
            ),
            headers=random_user.auth_headers,
            content=ParameterExtension().model_dump_json(),
        )

        assert response.status_code == status.HTTP_200_OK
        version = WorkflowVersionOut.model_validate_json(response.content)
        assert version.workflow_version_id == random_workflow.versions[0].workflow_version_id
        assert version.parameter_extension is not None

    @pytest.mark.asyncio
    async def test_delete_workflow_version_parameter_extension(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test deleting a workflow version parameter extension.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.delete(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "parameter-extension",
                ]
            ),
            headers=random_user.auth_headers,
        )

        assert response.status_code == status.HTTP_204_NO_CONTENT

    @pytest.mark.asyncio
    async def test_update_workflow_version_status(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test updating a workflow version status.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.patch(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "status",
                ]
            ),
            headers=random_user.auth_headers,
            content=WorkflowVersionStatus(status=WorkflowVersion.Status.PUBLISHED).model_dump_json(),
        )

        assert response.status_code == status.HTTP_200_OK
        version = WorkflowVersionOut.model_validate_json(response.content)
        assert version.workflow_version_id == random_workflow.versions[0].workflow_version_id
        assert version.status == WorkflowVersion.Status.PUBLISHED

    @pytest.mark.asyncio
    async def test_deprecate_workflow_version(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test deprecate a workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.patch(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow.workflow_id),
                    "versions",
                    random_workflow.versions[0].workflow_version_id,
                    "deprecate",
                ]
            ),
            headers=random_user.auth_headers,
        )

        assert response.status_code == status.HTTP_200_OK
        version = WorkflowVersionOut.model_validate_json(response.content)
        assert version.workflow_version_id == random_workflow.versions[0].workflow_version_id
        assert version.status == WorkflowVersion.Status.DEPRECATED

    @pytest.mark.asyncio
    async def test_update_non_existing_workflow_version_status(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test updating a non-existing workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.patch(
            "/".join([self.base_path, str(random_workflow.workflow_id), "versions", random_hex_string(), "status"]),
            headers=random_user.auth_headers,
            content=WorkflowVersionStatus(status=WorkflowVersion.Status.PUBLISHED).model_dump_json(),
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_deprecate_non_existing_workflow_version(
        self, client: AsyncClient, random_user: UserWithAuthHeader, random_workflow: WorkflowOut
    ) -> None:
        """
        Test deprecate a non-existing workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow : app.schemas.workflow.WorkflowOut
            Random workflow for testing.
        """
        response = await client.patch(
            "/".join([self.base_path, str(random_workflow.workflow_id), "versions", random_hex_string(), "deprecate"]),
            headers=random_user.auth_headers,
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestWorkflowVersionRoutesGetDocumentation(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    @pytest.mark.parametrize("document", [d for d in DocumentationEnum])
    async def test_download_workflow_version_documentation(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        document: DocumentationEnum,
    ) -> None:
        """
        Test downloading all the different documentation file for a workflow version.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        document : app.api.endpoints.workflow_version.DocumentationEnum
            All possible documents as pytest parameter.
        """
        response = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "documentation",
                ]
            ),
            headers=random_user.auth_headers,
            params={"document": document.value},
        )
        assert response.status_code == status.HTTP_200_OK

    @pytest.mark.asyncio
    @pytest.mark.parametrize("document", [d for d in DocumentationEnum])
    async def test_download_workflow_version_documentation_with_non_existing_mode(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        document: DocumentationEnum,
    ) -> None:
        """
        Test downloading all the different documentation file for a workflow version with a non-existing workflow mode.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        document : app.api.endpoints.workflow_version.DocumentationEnum
            All possible documents as pytest parameter.
        """
        response = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "documentation",
                ]
            ),
            headers=random_user.auth_headers,
            params={"document": document.value, "mode_id": str(uuid4())},
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    @pytest.mark.parametrize("document", [d for d in DocumentationEnum])
    async def test_download_workflow_version_documentation_with_existing_mode(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        random_workflow_mode: WorkflowMode,
        document: DocumentationEnum,
    ) -> None:
        """
        Test downloading all the different documentation file for a workflow version with a workflow mode.

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        random_workflow_mode : clowmdb.models.WorkflowMode
            Random workflow mode for testing.
        document : app.api.endpoints.workflow_version.DocumentationEnum
            All possible documents as pytest parameter.
        """
        response = await client.get(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "documentation",
                ]
            ),
            headers=random_user.auth_headers,
            params={"document": document.value, "mode_id": str(random_workflow_mode.mode_id)},
        )
        assert response.status_code == status.HTTP_200_OK


class TestWorkflowVersionIconRoutes(_TestWorkflowVersionRoutes):
    @pytest.mark.asyncio
    async def test_upload_new_icon(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        mock_s3_service: MockS3ServiceResource,
        db: AsyncSession,
        cleanup: CleanupList,
    ) -> None:
        """
        Test for uploading a new icon for a workflow version

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        cleanup : app.tests.utils.utils.CleanupList
            Cleanup object where (async) functions can be registered which get executed after a (failed) test.
        """
        img_buffer = BytesIO()
        Image.linear_gradient(mode="L").save(img_buffer, "PNG")
        img_buffer.seek(0)
        files = {"icon": ("RickRoll.png", img_buffer, "image/png")}
        response = await client.post(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "icon",
                ]
            ),
            headers=random_user.auth_headers,
            files=files,
        )
        assert response.status_code == status.HTTP_201_CREATED
        icon_url = response.json()["icon_url"]
        icon_slug = icon_url.split("/")[-1]
        cleanup.add_task(mock_s3_service.Bucket(settings.s3.icon_bucket).Object(icon_slug).delete)
        assert icon_slug in mock_s3_service.Bucket(settings.s3.icon_bucket).objects.all_keys()
        db_version = await db.scalar(
            select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        )
        assert db_version is not None
        assert db_version.icon_slug == icon_slug

    @pytest.mark.asyncio
    async def test_upload_new_icon_as_text(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        mock_s3_service: MockS3ServiceResource,
        db: AsyncSession,
    ) -> None:
        """
        Test for uploading a textfile as a new icon for a workflow version

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """

        files = {"icon": ("RickRoll.png", BytesIO(b"Never gonna give you up"), "text/plain")}
        response = await client.post(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "icon",
                ]
            ),
            headers=random_user.auth_headers,
            files=files,
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @pytest.mark.asyncio
    async def test_upload_new_icon_as_non_developer(
        self,
        client: AsyncClient,
        random_second_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
    ) -> None:
        """
        Test for uploading a new icon for a workflow version

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        """
        files = {"icon": ("RickRoll.txt", BytesIO(b"Never gonna give you up"), "plain/text")}
        response = await client.post(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "icon",
                ]
            ),
            headers=random_second_user.auth_headers,
            files=files,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.asyncio
    async def test_delete_icon(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        mock_s3_service: MockS3ServiceResource,
        db: AsyncSession,
    ) -> None:
        """
        Test for deleting a workflow version icon

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        old_slug = random_workflow_version.icon_slug
        response = await client.delete(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "icon",
                ]
            ),
            headers=random_user.auth_headers,
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert old_slug not in mock_s3_service.Bucket(settings.s3.icon_bucket).objects.all_keys()
        db_version = await db.scalar(
            select(WorkflowVersion).where(WorkflowVersion.git_commit_hash == random_workflow_version.git_commit_hash)
        )
        assert db_version is not None
        assert db_version.icon_slug is None

    @pytest.mark.asyncio
    async def test_delete_icon_as_non_developer(
        self,
        client: AsyncClient,
        random_second_user: UserWithAuthHeader,
        random_workflow_version: WorkflowVersion,
        mock_s3_service: MockS3ServiceResource,
        db: AsyncSession,
    ) -> None:
        """
        Test for deleting a workflow version icon

        Parameters
        ----------
        client : httpx.AsyncClient
            HTTP Client to perform the request on.
        random_second_user : app.tests.utils.user.UserWithAuthHeader
            Random user for testing.
        random_workflow_version : clowmdb.models.WorkflowVersion
            Random workflow version for testing.
        mock_s3_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock S3 Service to manipulate objects.
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        """
        response = await client.delete(
            "/".join(
                [
                    self.base_path,
                    str(random_workflow_version.workflow_id),
                    "versions",
                    random_workflow_version.git_commit_hash,
                    "icon",
                ]
            ),
            headers=random_second_user.auth_headers,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN
