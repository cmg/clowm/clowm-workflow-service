from uuid import uuid4

import pytest
from clowmdb.models import WorkflowMode
from fastapi import status
from httpx import AsyncClient

from app.schemas.workflow_mode import WorkflowModeOut
from app.tests.utils.user import UserWithAuthHeader


class TestWorkflowModeRoutesGet:
    _base_path = "workflow_modes"

    @pytest.mark.asyncio
    async def test_get_non_existing_workflow_mode(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
    ) -> None:
        response = await client.get(f"{self._base_path}/{uuid4()}", headers=random_user.auth_headers)
        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.asyncio
    async def test_get_existing_workflow_mode(
        self,
        client: AsyncClient,
        random_user: UserWithAuthHeader,
        random_workflow_mode: WorkflowMode,
    ) -> None:
        response = await client.get(
            f"{self._base_path}/{str(random_workflow_mode.mode_id)}", headers=random_user.auth_headers
        )
        assert response.status_code == status.HTTP_200_OK
        mode = WorkflowModeOut.model_validate_json(response.content)

        assert mode.mode_id == random_workflow_mode.mode_id
        assert mode.name == random_workflow_mode.name
        assert mode.entrypoint == random_workflow_mode.entrypoint
        assert mode.schema_path == random_workflow_mode.schema_path
