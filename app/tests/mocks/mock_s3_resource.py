from datetime import datetime
from io import BytesIO
from typing import Any

from botocore.exceptions import ClientError


class MockS3Object:
    """
    Mock S3 object for the boto3 S3Object for testing purposes.

    Attributes
    ----------
    key : str
        Key of the S3 object.
    bucket_name : str
        Name of the corresponding bucket.
    """

    def __init__(self, bucket_name: str, key: str, bucket: "MockS3Bucket") -> None:
        """
        Initialize a MockS3Object.

        Parameters
        ----------
        bucket_name : str
            Name of the corresponding bucket.
        key : str
            Key of the S3 object.
        bucket : app.tests.mocks.mock_s3_resource.MockS3Bucket
            Bucket where the object is
        """
        self.key = key
        self.bucket_name = bucket_name
        self.content_type = "text/plain"
        self._content: bytes | None = None
        self._bucket = bucket

    def __repr__(self) -> str:
        return f"MockS3Object(key={self.key}, bucket={self.bucket_name})"

    def upload_fileobj(self, Fileobj: BytesIO, ExtraArgs: dict[str, Any] | None = None) -> None:
        """
        Mock function for uploading a file from a ByteStream

        Parameters
        ----------
        Fileobj : io.BytesIO
            Stream to read from. Ignored.
        ExtraArgs : dict[str, Any] | None, default None
            Extra arguments for file upload. Ignored.
        """
        self._content = Fileobj.read()
        self._bucket.add_object(self)

    def download_fileobj(self, Fileobj: BytesIO) -> None:
        if self._content is not None:
            Fileobj.write(self._content)
        else:
            raise ClientError(operation_name="Mock", error_response={})

    def load(self) -> None:
        if self._content is None:
            raise ClientError(operation_name="Mock", error_response={})

    def delete(self) -> None:
        """
        Delete the object in the bucket
        """
        self._bucket.objects.delete(self.key)
        self._content = None


class MockS3Bucket:
    """
    Mock S3 bucket for the boto3 Bucket for testing purposes.

    Functions
    ---------
    Policy() -> app.tests.mocks.mock_s3_resource.MockS3BucketPolicy
        Get the bucket policy from the bucket.
    create() -> None
        Create the bucket in the mock service.
    delete() -> None
        Delete the bucket in the mock service
    delete_objects(Delete: dict[str, list[dict[str, str]]]) -> None
        Delete multiple objects in the bucket.
    get_objects() -> list[app.tests.mocks.mock_s3_resource.MockS3Object]
        List of MockS3Object in the bucket.
    add_object(obj: app.tests.mocks.mock_s3_resource.MockS3Object) -> None
        Add a MockS3Object to the bucket.

    Attributes
    ----------
    name: str
        name of the bucket.
    creation_date : datetime
        Creation date of the bucket.
    objects : app.tests.mocks.mock_s3_resource.MockS3Bucket.MockS3ObjectList
        Object in the bucket in a proxy class.
    """

    class MockS3ObjectList:
        """
        Proxy object to package a list in a class.

        Important because you have to access all the S3Objects of a bucket with
        S3Bucket.objects.all()

        Functions
        ---------
        all() -> list[app.tests.mocks.mock_s3_resource.MockS3Object]
            Get the saved list.
        filter(Prefix: str) -> app.tests.mocks.mock_s3_resource.MockS3Bucket.MockS3ObjectList
            Filter the object in the list by the prefix all their keys should have.
        add(obj: app.tests.mocks.mock_s3_resource.MockS3Object) -> None
            Add a MockS3Object to the list.
        delete(key: str) -> None
            Delete a MockS3Object from the list
        """

        def __init__(self, obj_list: list[MockS3Object] | None = None) -> None:
            self._objs: list[MockS3Object] = [] if obj_list is None else obj_list

        def all(self) -> list[MockS3Object]:
            """
            Get the saved list.

            Returns
            -------
            objects : list[app.tests.mocks.mock_s3_resource.MockS3Object]
                List of MockS3Object
            """
            return self._objs

        def all_keys(self) -> list[str]:
            """
            Get the keys of all objects

            Returns
            -------
            keys : list[str]
                List of all keys
            """
            return [o.key for o in self._objs]

        def add(self, obj: MockS3Object) -> None:
            """
            Add a MockS3Object to the list.

            Parameters
            ----------
            obj : app.tests.mocks.mock_s3_resource.MockS3Object
                MockS3Object which should be added to the list
            """
            self._objs.append(obj)

        def delete(self, key: str) -> None:
            """
            Delete a MockS3Object from the list.

            Parameters
            ----------
            key : str
                Key of the object to delete
            """
            self._objs = [obj for obj in self._objs if obj.key != key]

        def filter(self, Prefix: str) -> "MockS3Bucket.MockS3ObjectList":
            """
            Filter the object in the list by the prefix all their keys should have.

            Parameters
            ----------
            Prefix : str
                The prefix that all keys should have.

            Returns
            -------
            obj_list : app.tests.mocks.mock_s3_resource.MockS3Bucket.MockS3ObjectList
                The filtered list.
            """
            return MockS3Bucket.MockS3ObjectList(obj_list=list(filter(lambda x: x.key.startswith(Prefix), self._objs)))

        def get(self, key: str) -> MockS3Object | None:
            return next((obj for obj in self._objs if obj.key == key), None)

    def __init__(self, name: str, parent_service: "MockS3ServiceResource"):
        """
        Initialize a MockS3Bucket.

        Parameters
        ----------
        name : str
            Name of the bucket.
        parent_service : app.tests.mocks.mock_s3_resource.MockS3ServiceResource
            Mock service object where the bucket will be saved.
        """
        self.name = name
        self.creation_date: datetime = datetime.now()
        self.objects = MockS3Bucket.MockS3ObjectList()
        self._parent_service: MockS3ServiceResource = parent_service

    def create(self) -> None:
        """
        Create the bucket in the mock S3 service.
        """
        self._parent_service.create_bucket(self)

    def delete(self) -> None:
        """
        Delete the bucket in the mock S3 service.
        """
        self._parent_service.delete_bucket(self.name)

    def delete_objects(self, Delete: dict[str, list[dict[str, str]]]) -> None:
        """
        Delete multiple objects in the bucket.

        Parameters
        ----------
        Delete : dict[str, list[dict[str, str]]]
            The keys of the objects to delete.

        Notes
        -----
        The `Delete` parameter has the form
        {
            'Objects': [
                { 'Key' : 'test.txt' },...
            ]
        }
        """
        for key_object in Delete["Objects"]:
            self.objects.delete(key=key_object["Key"])

    def get_objects(self) -> list[MockS3Object]:
        """
        Get the MockS3Object in the bucket.
        Convenience function for testing.

        Returns
        -------
        objects : list[app.tests.mocks.mock_s3_resource.MockS3Object]
            List of MockS3Object in the bucket.
        """
        return self.objects.all()

    def add_object(self, obj: MockS3Object) -> None:
        """
        Add a MockS3Object to the bucket.
        Convenience function for testing.

        Parameters
        ----------
        obj : app.tests.mocks.mock_s3_resource.MockS3Object
            Object to add.
        """
        self.objects.add(obj)

    def Object(self, key: str) -> MockS3Object:
        """
        Get a virtual Object in the bucket. Will only be saved if the upload file function of the object is called

        Parameters
        ----------
        key : str
            Key of the object

        Returns
        -------
        obj : app.tests.mocks.mock_s3_resource.MockS3Object
            Mock S3 Object.
        """
        obj = self.objects.get(key)
        if obj is None:
            return MockS3Object(bucket_name=self.name, key=key, bucket=self)
        return obj

    def __repr__(self) -> str:
        return f"MockS3Bucket(name={self.name}, objects={[obj.key for obj in self.get_objects()]})"


class MockS3ServiceResource:
    """
    A functional mock class of the boto3 S3ServiceResource for testing purposes.

    Functions
    ---------
    Bucket(name: str) -> app.tests.mocks.mock_s3_resource.MockS3bucket
        Get/Create a mock bucket.
    create_bucket(bucket: app.tests.mocks.mock_s3_resource.MockS3bucket) -> None
        Create a bucket in the mock service.
    delete_bucket(name: str) -> None
        Delete a bucket in the mock service.
    create_object_in_bucket(bucket_name: str, key: str) -> app.tests.mocks.mock_s3_resource.MockS3Object
        Create an MockS3Object in a bucket.
    """

    def __init__(self) -> None:
        self._buckets: dict[str, MockS3Bucket] = {}

    def Bucket(self, name: str) -> MockS3Bucket:
        """
        Get an existing bucket from the mock service or create a new mock bucket but doesn't save it yet.
        Call `bucket.create()` on returned object for that.

        Parameters
        ----------
        name : str
            Name of the bucket.

        Returns
        -------
        bucket : app.tests.mocks.mock_s3_resource.MockS3bucket
            Existing/Created mock bucket.
        """
        return self._buckets[name] if name in self._buckets else MockS3Bucket(name=name, parent_service=self)

    def Object(self, bucket_name: str, key: str) -> MockS3Object:
        """
        Create a mock object.

        Parameters
        ----------
        bucket_name : str
            Name of the corresponding bucket.
        key : str
            Key of the S3 object.

        Returns
        -------
        obj : app.tests.mocks.mock_s3_resource.MockS3object
              Existing MockS3Object.
        """
        return MockS3Object(bucket_name=bucket_name, key=key, bucket=self._buckets[bucket_name])

    def create_bucket(self, bucket: MockS3Bucket) -> None:
        """
        Create a bucket in the mock service.
        Convenience function for testing.

        Parameters
        ----------
        bucket : app.tests.mocks.mock_s3_resource.MockS3bucket
            Bucket which should be created.
        """
        if bucket.name not in self._buckets.keys():
            self._buckets[bucket.name] = bucket

    def delete_bucket(self, name: str, force_delete: bool = False) -> None:
        """
        Delete am empty bucket in the mock service.
        Convenience function for testing.

        Parameters
        ----------
        name : str
            Name of the bucket.
        force_delete : bool, default False
            Force deletes even a non-empty bucket.
        """
        if name in self._buckets:
            if not force_delete and len(self._buckets[name].get_objects()) > 0:
                raise ClientError(operation_name="Mock", error_response={})
            del self._buckets[name]

    def create_object_in_bucket(self, bucket_name: str, key: str) -> MockS3Object:
        """
        Create an MockS3Object in a bucket.
        Convenience function for testing.

        Parameters
        ----------
        bucket_name : str
            Name of the bucket.
        key : str
            key of the S3 object.

        Returns
        -------
        obj : app.tests.mocks.mock_s3_resource.MockS3Object
            Newly created MockS3Object.
        """
        obj = MockS3Object(bucket_name=bucket_name, key=key, bucket=self._buckets[bucket_name])
        self._buckets[bucket_name].add_object(obj)
        return obj

    def __repr__(self) -> str:
        return f"MockS3ServiceResource(buckets={[bucket_name for bucket_name in self._buckets.keys()]})"
