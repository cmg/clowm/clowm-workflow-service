import json
from abc import ABC, abstractmethod

from fastapi import status
from httpx import ByteStream, Request, Response

_response_bytes = json.dumps(
    {
        # When checking if a file exists in a git repository, the GitHub API expects this in a response
        "download_url": "https://example.com",
    }
).encode("utf-8")


class MockHTTPService(ABC):
    def __init__(self) -> None:
        self.send_error = False

    @abstractmethod
    def handle_request(self, request: Request) -> Response: ...

    def reset(self) -> None:
        self.send_error = False


class DefaultMockHTTPService(MockHTTPService):
    def handle_request(self, request: Request) -> Response:
        # Stream response for documentation endpoint
        return Response(
            status_code=status.HTTP_404_NOT_FOUND if self.send_error else status.HTTP_200_OK,
            headers={"content-type": "application/json"},
            stream=ByteStream(_response_bytes),
        )
