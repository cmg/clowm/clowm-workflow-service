from math import ceil, floor, log2

from app.utils.backoff_strategy import ExponentialBackoff, LinearBackoff, NoBackoff


class TestExponentialBackoffStrategy:
    def test_exponential_without_initial_delay(self) -> None:
        """
        Test generating a bounded exponential backoff strategy series in seconds without an initial delay.
        """
        for max_val in range(1023, 1026):  # numbers around a power of 2
            index_at_maximum = floor(log2(max_val))
            sleep_generator = ExponentialBackoff(initial_delay=0, max_value=max_val)

            for i, sleep in enumerate(sleep_generator):
                if i < index_at_maximum:
                    assert sleep == 2 ** (i + 1)
                elif i == index_at_maximum:
                    assert sleep == max_val
                elif i == index_at_maximum + 1:
                    assert sleep == max_val
                    sleep_generator.close()
                else:
                    assert False, "Iteration should have stopped"

    def test_unbounded_exponential_without_initial_delay(self) -> None:
        """
        Test generating an unbounded exponential backoff strategy series in seconds without an initial delay
        """
        sleep_generator = ExponentialBackoff(initial_delay=0, max_value=-1)

        for i, sleep in enumerate(sleep_generator):
            assert sleep == 2 ** (i + 1)
            if i == 20:
                sleep_generator.close()
            elif i > 20:
                assert False, "Iteration should have stopped"

    def test_exponential_with_initial_delay(self) -> None:
        """
        Test generating a bounded exponential backoff strategy series in seconds with an initial delay
        """
        for max_val in range(1023, 1026):  # numbers around a power of 2
            index_at_maximum = ceil(log2(max_val))
            sleep_generator = ExponentialBackoff(initial_delay=30, max_value=max_val)

            for i, sleep in enumerate(sleep_generator):
                if i == 0:
                    assert sleep == 30
                elif i < index_at_maximum:
                    assert sleep == 2**i
                elif i == index_at_maximum:
                    assert sleep == max_val
                elif i == index_at_maximum + 1:
                    assert sleep == max_val
                    sleep_generator.close()
                else:
                    assert False, "Iteration should have stopped"


class TestLinearBackoffStrategy:
    def test_linear_without_initial_delay(self) -> None:
        """
        Test generating a bounded linear backoff strategy series in seconds without an initial delay
        """
        linear_backoff = 5
        repetition_factor = 5
        for max_val in range((linear_backoff * repetition_factor) - 1, (linear_backoff * repetition_factor) + 2):
            index_at_maximum = max_val // linear_backoff
            sleep_generator = LinearBackoff(initial_delay=0, backoff=linear_backoff, max_value=max_val)

            for i, sleep in enumerate(sleep_generator):
                if i < index_at_maximum:
                    assert sleep == linear_backoff * (i + 1)
                elif i == index_at_maximum:
                    assert sleep == max_val
                elif i == index_at_maximum + 1:
                    assert sleep == max_val
                    sleep_generator.close()
                else:
                    assert False, "Iteration should have stopped"

    def test_unbounded_linear_without_initial_delay(self) -> None:
        """
        Test generating an unbounded linear backoff strategy series in seconds without an initial delay
        """
        sleep_generator = LinearBackoff(initial_delay=0, backoff=6, max_value=-1)

        for i, sleep in enumerate(sleep_generator):
            assert sleep == 6 * (i + 1)
            if i == 200:
                sleep_generator.close()
            elif i > 200:
                assert False, "Iteration should have stopped"

    def test_linear_with_initial_delay(self) -> None:
        """
        Test generating a bounded linear backoff strategy series in seconds with an initial delay
        """
        linear_backoff = 5
        repetition_factor = 5
        for max_val in range((linear_backoff * repetition_factor) - 1, (linear_backoff * repetition_factor) + 2):
            index_at_maximum = (max_val // linear_backoff) + 1
            sleep_generator = LinearBackoff(initial_delay=30, backoff=linear_backoff, max_value=max_val)

            for i, sleep in enumerate(sleep_generator):
                if i == 0:
                    assert sleep == 30
                elif i < index_at_maximum:
                    assert sleep == linear_backoff * i
                elif i == index_at_maximum:
                    assert sleep == max_val
                elif i == index_at_maximum + 1:
                    assert sleep == max_val
                    sleep_generator.close()
                else:
                    assert False, "Iteration should have stopped"


class TestNoBackoffStrategy:
    def test_no_backoff_without_initial_delay(self) -> None:
        """
        Test generating no backoff strategy series in seconds without an initial delay
        """
        sleep_generator = NoBackoff(initial_delay=0, constant_value=40)

        for i, sleep in enumerate(sleep_generator):
            assert sleep == 40
            if i == 20:
                sleep_generator.close()
            elif i > 20:
                assert False, "Iteration should have stopped"

    def test_no_backoff_with_initial_delay(self) -> None:
        """
        Test generating no backoff strategy series in seconds with an initial delay
        """
        sleep_generator = NoBackoff(initial_delay=20, constant_value=40)

        for i, sleep in enumerate(sleep_generator):
            if i == 0:
                assert sleep == 20
            else:
                assert sleep == 40
            if i == 20:
                sleep_generator.close()
            elif i > 20:
                assert False, "Iteration should have stopped"
