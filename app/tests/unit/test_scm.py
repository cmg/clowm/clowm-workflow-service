from io import BytesIO
from uuid import uuid4

import pytest
from pydantic import AnyHttpUrl

from app.git_repository import GitHubRepository, GitlabRepository, build_repository
from app.scm import SCM, GitHubSCMProvider, GitlabSCMProvider, SCMProvider
from app.tests.utils.utils import random_hex_string


class TestSCM:
    def test_generate_scm_filename(self) -> None:
        scm_id = uuid4()
        assert SCM.generate_filename(scm_id) == f"{scm_id.hex}.scm"

    def test_single_provider_deserialization_with_blank_lines(
        self,
    ) -> None:
        """
        Test for deserialization of the SCM file with black lines
        """
        file = b"providers {\n\n\tgithub {\n\n\t\tuser = 'empty'\n\t\tpassword = 'password'\n\n\t}\n}"

        with BytesIO(file) as handle:
            scm = SCM.deserialize(handle)

        assert len(scm.providers) == 1
        provider = scm.providers[0]
        assert provider.name == "github"
        assert provider.user == "empty"
        assert provider.password == "password"

    def test_single_provider_deserialization_with_error(
        self,
    ) -> None:
        """
        Test for deserialization of the SCM file with an unknown attribute
        """
        file = b"providers {\n\n\tgithub {\n\n\t\tuser = 'empty'\n\t\tpasswor = 'password'\n\n\t}\n}"
        with pytest.raises(TypeError):
            with BytesIO(file) as handle:
                SCM.deserialize(handle)

    def test_single_provider_serialization(
        self,
    ) -> None:
        """
        Test for serialization and deserialization of the SCM file with a single provider
        """
        provider = SCMProvider(
            name="workflow1", user="username", password="password", platform="github", server="https://github.com"
        )
        scm = SCM([provider])
        with BytesIO() as handle:
            scm.serialize(handle)
            handle.seek(0)
            scm_content = handle.read().decode("utf-8")
        assert scm == scm
        assert "user = 'username'" in scm_content
        assert "password = 'password'" in scm_content
        assert "platform = 'github'" in scm_content
        assert "server = 'https://github.com'" in scm_content
        assert "workflow1" in scm_content

        with BytesIO(scm_content.encode("utf-8")) as handle:
            scm_parsed = SCM.deserialize(handle)
        assert scm == scm_parsed

    def test_multiple_provider_serialization(
        self,
    ) -> None:
        """
        Test for serialization and deserialization of the SCM file with a multiple providers
        """
        provider1 = SCMProvider(
            name="workflow1", user="username1", password="password1", platform="github1", server="https://github1.com"
        )
        provider2 = SCMProvider(
            name="workflow2", user="username2", password="password2", platform="github2", server="https://github2.com"
        )
        provider3 = SCMProvider(
            name="workflow3", user="username3", password="password3", platform="github3", server="https://github3.com"
        )
        providers = [provider1, provider2, provider3]
        scm = SCM(providers)
        with BytesIO() as handle:
            scm.serialize(handle)
            handle.seek(0)
            scm_content = handle.read().decode("utf-8")
        assert scm == scm
        for provider in providers:
            assert f"user = '{provider.user}'" in scm_content
            assert f"password = '{provider.password}'" in scm_content
            assert f"platform = '{provider.platform}'" in scm_content
            assert f"server = '{provider.server}'" in scm_content
            assert provider.name in scm_content

        with BytesIO(scm_content.encode("utf-8")) as handle:
            scm_parsed = SCM.deserialize(handle)
        assert scm == scm_parsed


class TestSCMProvider:
    def test_generate_provider_name(self) -> None:
        scm_id = uuid4()
        assert SCMProvider.generate_name(scm_id) == f"repo{scm_id.hex}"

    def test_build_provider_from_private_github_repo(
        self,
    ) -> None:
        """
        Test for building the SCM object from a private GitHub repo
        """
        repo = build_repository(
            url=AnyHttpUrl("https://github.com/bilbobaggins/example"),
            git_commit_hash=random_hex_string(),
            token=random_hex_string(16),
        )
        assert isinstance(repo, GitHubRepository)
        provider = SCMProvider.from_repo(repo, name="empty")
        assert provider is not None
        assert provider.name == "github"
        assert isinstance(provider, GitHubSCMProvider)
        assert provider.password == repo.token

    def test_build_provider_from_private_gitlab_repo(
        self,
    ) -> None:
        """
        Test for building the SCM object from a private GitLab repo
        """
        repo = build_repository(
            url=AnyHttpUrl("https://gitlab.com/bilbo/baggins/example"),
            git_commit_hash=random_hex_string(),
            token=random_hex_string(16),
        )
        assert isinstance(repo, GitlabRepository)
        provider_name = random_hex_string(length=10)
        provider = SCMProvider.from_repo(repo, name=provider_name)
        assert provider is not None
        assert provider.name == provider_name
        assert provider.platform == "gitlab"
        assert isinstance(provider, GitlabSCMProvider)
        assert provider.user is not None
        assert len(provider.user) > 0
        assert provider.password == repo.token

    def test_build_provider_from_public_git_repo(
        self,
    ) -> None:
        """
        Test for building the SCM object from a public GitHub repo
        """
        repo = build_repository(
            url=AnyHttpUrl("https://github.com/bilbobaggins/example"), git_commit_hash=random_hex_string()
        )
        provider = SCMProvider.from_repo(repo, name="empty")
        assert provider is not None
        assert provider.password is None
