from inspect import iscoroutinefunction
from typing import Any, Awaitable, Callable, Generic, ParamSpec, TypeVar
from uuid import UUID

from clowmdb.models import Workflow, WorkflowMode
from sqlalchemy import delete
from sqlalchemy.ext.asyncio import AsyncSession

P = ParamSpec("P")
T = TypeVar("T")


class Job(Generic[P, T]):
    def __init__(self, func: Callable[P, T], *args: P.args, **kwargs: P.kwargs) -> None:
        self.func = func
        self.args = args
        self.kwargs = kwargs

    @property
    def is_async(self) -> bool:
        return iscoroutinefunction(self.func)

    def __call__(self) -> T:
        return self.func(*self.args, **self.kwargs)


class AsyncJob(Job):
    def __init__(self, func: Callable[P, Awaitable[T]], *args: P.args, **kwargs: P.kwargs) -> None:
        super().__init__(func, *args, **kwargs)
        assert iscoroutinefunction(self.func)

    async def __call__(self) -> T:
        return await super().__call__()


class CleanupList:
    """
    Helper object to hold a queue of functions that can be executed later
    """

    def __init__(self) -> None:
        self.queue: list[Job] = []

    def add_task(self, func: Callable[P, Any], *args: P.args, **kwargs: P.kwargs) -> None:
        """
        Add a (async) function to the queue.

        Parameters
        ----------
        func : Callable[P, Any]
            Function to register.
        args : P.args
            Arguments to the function.
        kwargs : P.kwargs
            Keyword arguments to the function.
        """
        if iscoroutinefunction(func):
            self.queue.append(AsyncJob(func, *args, **kwargs))
        else:
            self.queue.append(Job(func, *args, **kwargs))

    async def empty_queue(self) -> None:
        """
        Empty the queue by executing the registered functions.
        """
        while len(self.queue) > 0:
            func = self.queue.pop()
            if func.is_async:
                await func()
            else:
                func()


async def delete_workflow(db: AsyncSession, workflow_id: UUID) -> None:
    await db.execute(delete(Workflow).where(Workflow.workflow_id_bytes == workflow_id.bytes))
    await db.commit()


async def delete_workflow_mode(db: AsyncSession, mode_id: UUID) -> None:
    await db.execute(delete(WorkflowMode).where(WorkflowMode.mode_id_bytes == mode_id.bytes))
    await db.commit()
