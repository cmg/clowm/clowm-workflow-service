import random
import string


def random_lower_string(length: int = 32) -> str:
    """
    Creates a random string with arbitrary length.

    Parameters
    ----------
    length : int, default 32
        Length for the random string.

    Returns
    -------
    string : str
        Random string.
    """
    return "".join(random.choices(string.ascii_lowercase, k=length))


def random_hex_string(length: int = 40) -> str:
    """
    Creates a random string with only hexadecimal digits

    Parameters
    ----------
    length : int, default 32
        Length for the random string.

    Returns
    -------
    string : str
        Random string.
    """
    return "".join(random.choices("0123456789abcdef", k=length))


def random_ipv4_string() -> str:
    """
    Creates a random IPv4 address.

    Returns
    -------
    string : str
        Random IPv4 address.
    """
    return ".".join(str(random.randint(0, 255)) for _ in range(4))
