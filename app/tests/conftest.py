import asyncio
import time
from contextlib import asynccontextmanager
from functools import partial
from io import BytesIO
from secrets import token_urlsafe
from typing import AsyncIterator, Iterator
from uuid import uuid4

import httpx
import pytest
import pytest_asyncio
from clowmdb.db.session import get_async_session
from clowmdb.models import (
    Bucket,
    Resource,
    ResourceVersion,
    Workflow,
    WorkflowExecution,
    WorkflowMode,
    WorkflowVersion,
    workflow_mode_association_table,
)
from sqlalchemy import insert, select, update
from sqlalchemy.ext.asyncio import AsyncSession

from app.api import dependencies
from app.api.dependencies import get_db, get_decode_jwt_function, get_httpx_client, get_rgw_admin, get_s3_resource
from app.core.config import settings
from app.git_repository import build_repository
from app.main import app
from app.schemas.workflow import WorkflowOut
from app.scm import SCM, SCMProvider
from app.tests.mocks import (
    DefaultMockHTTPService,
    MockHTTPService,
    MockOpaService,
    MockRGWAdmin,
    MockS3ServiceResource,
    MockSlurmCluster,
)
from app.tests.utils.bucket import create_random_bucket
from app.tests.utils.cleanup import CleanupList
from app.tests.utils.user import UserWithAuthHeader, create_random_user, decode_mock_token, get_authorization_headers
from app.tests.utils.utils import random_hex_string, random_lower_string

jwt_secret = token_urlsafe(32)


@pytest.fixture(scope="session")
def event_loop() -> Iterator:
    """
    Creates an instance of the default event loop for the test session.
    """
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def mock_s3_service() -> Iterator[MockS3ServiceResource]:
    """
    Fixture for creating a mock object for the rgwadmin package.
    """
    mock_s3 = MockS3ServiceResource()
    mock_s3.Bucket(settings.s3.icon_bucket).create()
    mock_s3.Bucket(settings.s3.params_bucket).create()
    mock_s3.Bucket(settings.s3.workflow_bucket).create()
    yield mock_s3
    mock_s3.delete_bucket(name=settings.s3.icon_bucket, force_delete=True)
    mock_s3.delete_bucket(name=settings.s3.params_bucket, force_delete=True)
    mock_s3.delete_bucket(name=settings.s3.workflow_bucket, force_delete=True)


@pytest.fixture(scope="session")
def mock_slurm_cluster() -> Iterator[MockSlurmCluster]:
    mock_slurm = MockSlurmCluster()
    yield mock_slurm
    mock_slurm.reset()


@pytest.fixture(scope="session")
def mock_rgw_admin() -> MockRGWAdmin:
    """
    Fixture for creating a mock object for the rgwadmin package.
    """
    return MockRGWAdmin()


@pytest.fixture(scope="session")
def mock_opa_service() -> Iterator[MockOpaService]:
    mock_opa = MockOpaService()
    yield mock_opa
    mock_opa.reset()


@pytest.fixture(scope="session")
def mock_default_http_server() -> Iterator[DefaultMockHTTPService]:
    mock_server = DefaultMockHTTPService()
    yield mock_server
    mock_server.reset()


@pytest_asyncio.fixture(scope="session")
async def mock_client(
    mock_opa_service: MockOpaService,
    mock_slurm_cluster: MockSlurmCluster,
    mock_default_http_server: DefaultMockHTTPService,
) -> AsyncIterator[httpx.AsyncClient]:
    def mock_request_handler(request: httpx.Request) -> httpx.Response:
        url = str(request.url)
        handler: MockHTTPService
        if url.startswith(str(settings.cluster.slurm.uri)):
            handler = mock_slurm_cluster
        elif url.startswith(str(settings.opa.uri)):
            handler = mock_opa_service
        else:
            handler = mock_default_http_server
        return handler.handle_request(request=request)

    async with httpx.AsyncClient(transport=httpx.MockTransport(mock_request_handler)) as http_client:
        yield http_client


@pytest.fixture(autouse=True)
def monkeypatch_background_connections(
    monkeypatch: pytest.MonkeyPatch,
    db: AsyncSession,
    mock_s3_service: MockS3ServiceResource,
    mock_client: httpx.AsyncClient,
    mock_rgw_admin: MockRGWAdmin,
) -> None:
    """
    Patch the functions to get resources in background tasks with mock resources.
    """

    @asynccontextmanager
    async def get_http_client() -> AsyncIterator[httpx.AsyncClient]:
        yield mock_client

    async def get_patch_db() -> AsyncIterator[AsyncSession]:
        yield db

    monkeypatch.setattr(dependencies, "get_db", get_patch_db)
    monkeypatch.setattr(dependencies, "get_s3_resource", lambda: mock_s3_service)
    monkeypatch.setattr(dependencies, "get_background_http_client", get_http_client)
    monkeypatch.setattr(dependencies, "get_rgw_admin", lambda: mock_rgw_admin)


@pytest_asyncio.fixture(scope="module")
async def client(
    mock_s3_service: MockS3ServiceResource,
    db: AsyncSession,
    mock_opa_service: MockOpaService,
    mock_slurm_cluster: MockSlurmCluster,
    mock_client: httpx.AsyncClient,
    mock_rgw_admin: MockRGWAdmin,
) -> AsyncIterator[httpx.AsyncClient]:
    """
    Fixture for creating a TestClient and perform HTTP Request on it.
    Overrides several dependencies.
    """
    app.dependency_overrides[get_rgw_admin] = lambda: mock_rgw_admin
    app.dependency_overrides[get_httpx_client] = lambda: mock_client
    app.dependency_overrides[get_s3_resource] = lambda: mock_s3_service
    app.dependency_overrides[get_decode_jwt_function] = lambda: partial(decode_mock_token, secret=jwt_secret)
    app.dependency_overrides[get_db] = lambda: db
    async with httpx.AsyncClient(transport=httpx.ASGITransport(app=app), base_url="http://localhost") as ac:  # type: ignore[arg-type]
        yield ac
    app.dependency_overrides = {}


@pytest_asyncio.fixture(scope="module")
async def db() -> AsyncIterator[AsyncSession]:
    """
    Fixture for creating a database session to connect to.
    """
    async with get_async_session(url=str(settings.db.dsn_async), verbose=settings.db.verbose) as dbSession:
        yield dbSession


@pytest_asyncio.fixture(scope="function")
async def random_user(
    db: AsyncSession, mock_opa_service: MockOpaService, mock_rgw_admin: MockRGWAdmin
) -> AsyncIterator[UserWithAuthHeader]:
    """
    Create a random user and deletes him afterward.
    """
    user = await create_random_user(db)
    mock_rgw_admin.create_key(uid=str(user.uid))
    mock_opa_service.add_user(user.lifescience_id, privileged=True)
    yield UserWithAuthHeader(user=user, auth_headers=get_authorization_headers(uid=user.uid, secret=jwt_secret))
    mock_rgw_admin.delete_user(uid=str(user.uid))
    mock_opa_service.delete_user(user.lifescience_id)
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="module")
async def random_second_user(
    db: AsyncSession, mock_opa_service: MockOpaService, mock_rgw_admin: MockRGWAdmin
) -> AsyncIterator[UserWithAuthHeader]:
    """
    Create a random second user and deletes him afterward.
    """
    user = await create_random_user(db)
    mock_rgw_admin.create_key(uid=str(user.uid))
    mock_opa_service.add_user(user.lifescience_id)
    yield UserWithAuthHeader(user=user, auth_headers=get_authorization_headers(uid=user.uid, secret=jwt_secret))
    mock_rgw_admin.delete_user(uid=str(user.uid))
    mock_opa_service.delete_user(user.lifescience_id)
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="module")
async def random_third_user(
    db: AsyncSession, mock_opa_service: MockOpaService, mock_rgw_admin: MockRGWAdmin
) -> AsyncIterator[UserWithAuthHeader]:
    """
    Create a random third user and deletes him afterward.
    """
    user = await create_random_user(db)
    mock_rgw_admin.create_key(uid=str(user.uid))
    mock_opa_service.add_user(user.lifescience_id)
    yield UserWithAuthHeader(user=user, auth_headers=get_authorization_headers(uid=user.uid, secret=jwt_secret))
    mock_rgw_admin.delete_user(uid=str(user.uid))
    mock_opa_service.delete_user(user.lifescience_id)
    await db.delete(user)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_bucket(
    db: AsyncSession, random_user: UserWithAuthHeader, mock_s3_service: MockS3ServiceResource
) -> AsyncIterator[Bucket]:
    """
    Create a random user and deletes him afterwards.
    """
    bucket = await create_random_bucket(db, random_user.user)
    mock_s3_service.Bucket(name=bucket.name).create()
    yield bucket
    mock_s3_service.delete_bucket(name=bucket.name, force_delete=True)
    await db.delete(bucket)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_workflow(
    db: AsyncSession, random_user: UserWithAuthHeader, mock_s3_service: MockS3ServiceResource
) -> AsyncIterator[WorkflowOut]:
    """
    Create a random workflow and deletes it afterwards.
    """
    workflow_db = Workflow(
        name=random_lower_string(10),
        repository_url="https://github.de/example-user/example",
        short_description=random_lower_string(65),
        developer_id_bytes=random_user.user.uid.bytes,
    )
    db.add(workflow_db)
    await db.commit()
    icon_slug = f"{uuid4().hex}.png"
    workflow_version = WorkflowVersion(
        git_commit_hash=random_hex_string(40),
        version="v1.0.0",
        workflow_id_bytes=workflow_db.workflow_id.bytes,
        icon_slug=icon_slug,
    )
    db.add(workflow_version)
    await db.commit()
    mock_s3_service.Bucket(settings.s3.icon_bucket).Object(icon_slug).upload_fileobj(BytesIO(b"{}"))
    mock_s3_service.Bucket(settings.s3.workflow_bucket).Object(
        f"{workflow_version.git_commit_hash}.json"
    ).upload_fileobj(BytesIO(b"{}"))
    yield WorkflowOut.from_db_workflow(db_workflow=workflow_db, versions=[workflow_version], load_modes=False)
    mock_s3_service.Bucket(settings.s3.workflow_bucket).Object(f"{workflow_version.git_commit_hash}.json").delete()
    mock_s3_service.Bucket(settings.s3.icon_bucket).Object(icon_slug).delete()
    await db.delete(workflow_db)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_private_workflow(
    db: AsyncSession,
    random_workflow: WorkflowOut,
    random_workflow_version: WorkflowVersion,
    mock_s3_service: MockS3ServiceResource,
) -> AsyncIterator[WorkflowOut]:
    """
    Transform the random workflow into a private workflow.
    """
    # Update credentials in Database
    token = random_lower_string(15)
    await db.execute(
        update(Workflow)
        .where(Workflow.workflow_id_bytes == random_workflow.workflow_id.bytes)
        .values(credentials_token=token)
    )
    await db.commit()

    # Upload SCM file to PARAMS_BUCKET
    scm_provider = SCMProvider.from_repo(
        build_repository(random_workflow.repository_url, random_workflow_version.git_commit_hash, token=token),
        name=SCMProvider.generate_name(random_workflow.workflow_id),
    )
    assert scm_provider is not None
    with BytesIO() as f:
        SCM([scm_provider]).serialize(f)
        f.seek(0)
        mock_s3_service.Bucket(settings.s3.params_bucket).Object(
            SCM.generate_filename(random_workflow.workflow_id)
        ).upload_fileobj(f)
    yield random_workflow
    # Delete SCM in PARAMS_BUCKET after tests
    mock_s3_service.Bucket(settings.s3.params_bucket).Object(
        SCM.generate_filename(random_workflow.workflow_id)
    ).delete()


@pytest_asyncio.fixture(scope="function")
async def random_workflow_version(db: AsyncSession, random_workflow: WorkflowOut) -> WorkflowVersion:
    """
    Create a random workflow version. Will be deleted, when the workflow is deleted.
    """
    stmt = select(WorkflowVersion).where(
        WorkflowVersion.git_commit_hash == random_workflow.versions[0].workflow_version_id
    )
    return await db.scalar(stmt)


@pytest_asyncio.fixture(scope="function")
async def random_running_workflow_execution(
    db: AsyncSession,
    random_workflow_version: WorkflowVersion,
    random_user: UserWithAuthHeader,
    mock_s3_service: MockS3ServiceResource,
    mock_slurm_cluster: MockSlurmCluster,
) -> AsyncIterator[WorkflowExecution]:
    """
    Create a random running workflow execution. Will be deleted, when the user is deleted.
    """
    execution = WorkflowExecution(
        executor_id_bytes=random_user.user.uid.bytes,
        workflow_version_id=random_workflow_version.git_commit_hash,
        slurm_job_id=-1,
    )
    db.add(execution)
    await db.commit()
    slurm_job_id = mock_slurm_cluster.add_workflow_execution({"job": {"name": str(execution.execution_id)}})
    await db.execute(
        update(WorkflowExecution)
        .where(WorkflowExecution.execution_id_bytes == execution.execution_id.bytes)
        .values(slurm_job_id=slurm_job_id)
    )
    mock_s3_service.Bucket(settings.s3.params_bucket).Object(
        f"params-{execution.execution_id.hex}.json"
    ).upload_fileobj(BytesIO(b"{}"))
    yield execution
    mock_s3_service.Bucket(settings.s3.params_bucket).Object(f"params-{execution.execution_id.hex}.json").delete()


@pytest_asyncio.fixture(scope="function")
async def random_completed_workflow_execution(
    db: AsyncSession, random_running_workflow_execution: WorkflowExecution
) -> WorkflowExecution:
    """
    Create a random workflow execution which is completed.
    """

    await db.execute(
        update(WorkflowExecution)
        .where(WorkflowExecution.execution_id_bytes == random_running_workflow_execution.execution_id.bytes)
        .values(end_time=round(time.time()), status=WorkflowExecution.WorkflowExecutionStatus.SUCCESS)
    )
    await db.commit()
    return random_running_workflow_execution


@pytest_asyncio.fixture(scope="function")
async def random_workflow_mode(
    db: AsyncSession,
    random_workflow_version: WorkflowVersion,
    mock_s3_service: MockS3ServiceResource,
) -> AsyncIterator[WorkflowMode]:
    """
    Create a random workflow execution. Will be deleted, when the user is deleted.
    """
    mode = WorkflowMode(
        name=random_lower_string(8), schema_path=random_lower_string(32), entrypoint=random_lower_string(16)
    )
    db.add(mode)
    await db.commit()
    await db.execute(
        insert(workflow_mode_association_table),
        [
            {
                "workflow_version_commit_hash": random_workflow_version.git_commit_hash,
                "workflow_mode_id": mode.mode_id.bytes,
            }
        ],
    )
    await db.commit()
    key = f"{random_workflow_version.git_commit_hash}-{mode.mode_id.hex}.json"
    parameter_schema = mock_s3_service.Bucket(settings.s3.workflow_bucket).Object(key)
    parameter_schema.upload_fileobj(BytesIO(b"{}"))
    yield mode
    parameter_schema.delete()
    await db.delete(mode)
    await db.commit()


@pytest.fixture(scope="function", params=[status for status in ResourceVersion.Status])
def resource_state(request: pytest.FixtureRequest) -> ResourceVersion.Status:
    """
    Return all possible resource version sates as fixture
    """
    return request.param


@pytest_asyncio.fixture(scope="function")
async def random_resource_version(db: AsyncSession, random_user: UserWithAuthHeader) -> AsyncIterator[ResourceVersion]:
    """
    Create a random workflow execution. Will be deleted, when the user is deleted.
    """
    resource = Resource(
        name=random_lower_string(8),
        short_description=random_lower_string(32),
        source=random_lower_string(16),
        maintainer_id_bytes=random_user.user.uid_bytes,
    )
    db.add(resource)
    await db.commit()
    version = ResourceVersion(
        release=random_lower_string(4),
        resource_id_bytes=resource.resource_id_bytes,
        status=ResourceVersion.Status.LATEST,
    )
    db.add(version)
    await db.commit()
    yield version
    await db.delete(resource)
    await db.commit()


@pytest_asyncio.fixture(scope="function")
async def random_resource_version_states(
    db: AsyncSession,
    random_resource_version: ResourceVersion,
    resource_state: ResourceVersion.Status,
) -> ResourceVersion:
    """
    Create a random resource version with all possible resource version status.
    """
    stmt = (
        update(ResourceVersion)
        .where(ResourceVersion.resource_version_id_bytes == random_resource_version.resource_version_id.bytes)
        .values(status=resource_state)
    )
    await db.execute(stmt)
    await db.commit()
    await db.refresh(random_resource_version, attribute_names=["status"])
    return random_resource_version


@pytest_asyncio.fixture(scope="function")
async def cleanup(db: AsyncSession) -> AsyncIterator[CleanupList]:
    """
    Yields a Cleanup object where (async) functions can be registered which get executed after a (failed) test
    """
    cleanup_list = CleanupList()
    yield cleanup_list
    await cleanup_list.empty_queue()
