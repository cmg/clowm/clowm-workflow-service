from abc import ABC, abstractmethod
from collections.abc import Generator
from types import TracebackType
from typing import Any, Type


class BackoffStrategy(ABC, Generator):
    def __init__(self, initial_delay: int = 0):
        """
        Initialize the class BackoffStrategy

        Parameters
        ----------
        initial_delay : int, default 0
            The initial delay in seconds that should be emitted first. if smaller than 1 then don't emit this value.
        """
        self._current_val = 0
        self._delay = initial_delay
        self._delay_first_iteration = initial_delay > 0
        self._iteration = 0
        self._stop_next = False

    def throw(  # type: ignore
        self, __typ: Type[BaseException], __val: BaseException | object = "", __tb: TracebackType | None = None
    ) -> Any:  # pragma: no cover
        raise __typ(__val, __tb)

    def send(self, __value: Any) -> None:  # pragma: no cover
        pass

    def close(self) -> None:
        """
        Stop the iteration before the next turn.
        """
        self._stop_next = True

    def __iter__(self) -> "BackoffStrategy":
        return self

    @abstractmethod
    def _compute_next_value(self, iteration: int) -> int:
        """
        Compute the next value in the infinite series for this backoff strategy.

        Parameters
        ----------
        iteration : int
            The number of the current interation

        Returns
        -------
        val : int
            Next value to emit.
        """
        ...

    def __next__(self) -> int:
        """
        Emits the next value for this Generator.
        If there is an initial delay, it will be emitted first.
        Returns
        -------
        val : int
            Next value in the iteration
        """
        if self._stop_next:
            raise StopIteration
        self._iteration += 1
        if self._delay_first_iteration:
            self._delay_first_iteration = False
            self._iteration -= 1
            return self._delay
        self._current_val = self._compute_next_value(self._iteration)
        return self._current_val

    def __str__(self) -> str:
        return f"BackoffStrategyGenerator(iterations={self._iteration}, current_val={self._current_val})"

    def __repr__(self) -> str:
        return str(self)


class ExponentialBackoff(BackoffStrategy):
    """
    An exponential Backoff strategy based on the power of two. The generated values should be put into a sleep function.
    """

    def __init__(self, initial_delay: int = 0, max_value: int = 300):
        """
        Initialize the exponential BackoffStrategy class

        Parameters
        ----------
        initial_delay : int, default 0
            The initial delay in seconds that should be emitted first. if smaller than 1 then don't emit this value.
        max_value : int, default 300
            The maximum this generator can emit. If smaller than 1 then this series is unbounded.
        """
        super().__init__(initial_delay=initial_delay)
        self.max_value = max_value
        self._reached_max = False

    def _compute_next_value(self, iteration: int) -> int:
        if self._reached_max:
            return self._current_val
        next_val = 2 << (iteration - 1)
        if 0 < self.max_value < next_val:
            self._reached_max = True
            return self.max_value
        return next_val


class NoBackoff(BackoffStrategy):
    """
    No Backoff strategy. It always emits a constant value. The generated values should be put into a sleep function.
    """

    def __init__(self, initial_delay: int = 0, constant_value: int = 30):
        """
        Initialize the no BackoffStrategy class

        Parameters
        ----------
        initial_delay : int, default 0
            The initial delay in seconds that should be emitted first. if smaller than 1 then don't emit this value.
        constant_value : int, default 30
            The constant value this generator should emit.
        """
        super().__init__(initial_delay=initial_delay)
        self._val = constant_value

    def _compute_next_value(self, iteration: int) -> int:
        return self._val


class LinearBackoff(BackoffStrategy):
    """
    A linear Backoff strategy. The generated values should be put into a sleep function.
    """

    def __init__(self, initial_delay: int = 0, backoff: int = 5, max_value: int = 300):
        """
        Initialize the linear BackoffStrategy class

        Parameters
        ----------
        initial_delay : int, default 0
            The initial delay in seconds that should be emitted first. if smaller than 1 then don't emit this value.
        backoff : int, default 5
            The linear factor that is added each iteration.
        max_value : int, default 300
            The maximum this generator can emit. If smaller than 1 then this series is unbounded.
        """
        super().__init__(initial_delay=initial_delay)
        self.max_value = max_value
        self._backoff = backoff
        self._reached_max = False

    def _compute_next_value(self, iteration: int) -> int:
        if self._reached_max:
            return self._current_val
        next_val = self._current_val + self._backoff
        if 0 < self.max_value < next_val:
            self._reached_max = True
            return self.max_value
        return next_val
