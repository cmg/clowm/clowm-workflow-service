from urllib.parse import urlparse

from pydantic import AnyHttpUrl

from app.git_repository.abstract_repository import GitRepository
from app.git_repository.github import GitHubRepository
from app.git_repository.gitlab import GitlabRepository

__all__ = ["GitlabRepository", "GitRepository", "GitHubRepository", "build_repository"]


def build_repository(url: AnyHttpUrl, git_commit_hash: str, token: str | None = None) -> GitRepository:
    """
    Build the right git repository object based on the url

    Parameters
    ----------
    url : str
        URL of the git repository
    git_commit_hash : str
        Pin down git commit hash
    token : str | None
        Token to access a private git repository
    Returns
    -------
    repo : GitRepository
        Specialized Git repository object
    """
    domain = urlparse(str(url)).netloc
    if "github" in domain:
        return GitHubRepository(url=str(url), git_commit_hash=git_commit_hash, token=token)
    elif "gitlab" in domain:
        return GitlabRepository(url=str(url), git_commit_hash=git_commit_hash, token=token)
    raise NotImplementedError("Unknown Git repository Provider")
