from typing import Any, BinaryIO, Optional
from uuid import UUID

from pydantic import AnyHttpUrl

from app.git_repository import GitHubRepository, GitlabRepository, GitRepository


class SCMProvider:
    """
    The Provider class represents a provider in an SCM file for Nextflow.
    """

    def __init__(
        self,
        name: str,
        user: str | None = None,
        password: str | None = None,
        platform: str | None = None,
        server: str | None = None,
        token: str | None = None,
        endpoint: str | None = None,
    ):
        self.name = name
        self.user = user
        self.password = password
        self.platform = platform
        self.server = server
        self.token = token
        self.endpoint = endpoint

    @staticmethod
    def generate_name(scm_file_id: UUID) -> str:
        return f"repo{scm_file_id.hex}"

    def serialize(self, handle: BinaryIO) -> None:
        """
        Serialize a SCMProvider object to a file-like object.
        Should be not called directly.

        Parameters
        ----------
        handle : io.BytesIO
            Open stream to write to
        """
        handle.write(4 * b" ")
        handle.write(f"{self.name} {{\n".encode("utf-8"))
        for attribute, value in self.__dict__.items():
            if value and attribute != "name":
                handle.write(8 * b" ")
                handle.write(f"{attribute} = '{value}'\n".encode("utf-8"))
        handle.write(4 * b" ")
        handle.write(b"}\n")

    @staticmethod
    def deserialize(handle: BinaryIO) -> Optional["SCMProvider"]:
        """
        Deserialize an SCMProvider from a file-like object.
        Should be not called directly.

        Parameters
        ----------
        handle : io.BytesIO
            Open stream to read from

        Returns
        -------
        provider : SCMProvider | None
            Provider object if there is one, else None

        Notes
        -----
        Raises an TypeError if there is an unknown property
        """
        params = dict()
        first_line = handle.readline().strip()
        while len(first_line) == 0:
            first_line = handle.readline().strip()
        if b"}" in first_line:
            return None
        params["name"] = first_line.decode("utf-8").split(" ")[0]
        while True:
            line = handle.readline().strip()
            if len(line) == 0:
                continue
            if b"}" in line:
                break
            split = line.decode("utf-8").split(" ")
            params[split[0]] = split[-1][1:-1]
        return SCMProvider(**params)

    def __str__(self) -> str:
        return f"Provider(name={self.name} user={self.user}{'password protected' if self.password else ''})"

    def __repr__(self) -> str:
        return str(self)

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, SCMProvider):
            return False
        return other.name == self.name and other.password == self.password and other.user == self.user

    @staticmethod
    def from_repo(repo: GitRepository, name: str) -> "SCMProvider":
        """
        Build a provider from a git repository.

        Parameters
        ----------
        repo : app.git_repository.GitRepository
            Git repository from which the provider is built
        name : str
            Name of the provider

        Returns
        -------
        provider : SCMProvider | None
            Provider with data from the git repository. If the repository is public, return None
        """
        if isinstance(repo, GitHubRepository):
            return GitHubSCMProvider(user=repo.account, password=repo.token)
        elif isinstance(repo, GitlabRepository):
            return GitlabSCMProvider(
                name=name, password=repo.token, server=str(AnyHttpUrl.build(scheme="https", host=repo.domain))[:-1]
            )
        return SCMProvider(name=name, password=repo.token, server=repo.url)  # pragma: no cover


class GitHubSCMProvider(SCMProvider):
    """
    Convenience class for the GitHub provider
    """

    def __init__(self, user: str, password: str | None = None):
        super().__init__(name="github", user=user, password=password)

    def __str__(self) -> str:
        return f"GitHubProvider(user={self.user}{'password protected' if self.password else ''})"


class GitlabSCMProvider(SCMProvider):
    """
    Convenience class for the GitLab provider
    """

    def __init__(self, name: str, server: str, password: str | None):
        super().__init__(name=name, user="nonempty", password=password, platform="gitlab", server=server)

    def __str__(self) -> str:
        return f"GitlabProvider(name={self.name} server={self.server} user={self.user}{'password protected' if self.password else ''})"  # noqa: E501


class SCM:
    """
    The SCM class represents an SCM file for Nextflow.
    https://www.nextflow.io/docs/latest/sharing.html#scm-configuration-file
    """

    def __init__(self, providers: list[SCMProvider] | None = None):
        self.providers = providers if providers else []

    def serialize(self, handle: BinaryIO) -> None:
        """
        Serialize an SCM object to a file-like object.

        Parameters
        ----------
        handle : io.BytesIO
            Open stream to write to
        """
        handle.write(b"providers {\n")
        for provider in self.providers:
            provider.serialize(handle)
        handle.write(b"}")

    @staticmethod
    def generate_filename(scm_file_id: UUID) -> str:
        return f"{scm_file_id.hex}.scm"

    @staticmethod
    def deserialize(handle: BinaryIO) -> "SCM":
        """
        Deserialize an SCM file from a file-like object.

        Parameters
        ----------
        handle : io.BytesIO
            Open stream to read from

        Returns
        -------
        scm : SCM
            SCM object containing all providers

        Notes
        -----
        Raises an TypeError if there is an unknown property in a provider
        """
        providers = []
        # Skip first line
        handle.readline()
        while True:
            try:
                provider = SCMProvider.deserialize(handle)
                if provider is None:
                    break
                providers.append(provider)
            except TypeError as e:
                raise e
        return SCM(providers)

    def __str__(self) -> str:
        return f"SCM(providers={self.providers})"

    def __repr__(self) -> str:
        return str(self)

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, SCM):
            return False
        return sum(1 for p1, p2 in zip(self.providers, other.providers) if p1 == p2) == len(self.providers)
