import io

from fastapi import status
from httpx import AsyncClient
from opentelemetry import trace

from app.core.config import settings
from app.slurm.schemas import SlurmJobSubmission

tracer = trace.get_tracer_provider().get_tracer(__name__)


def _filter_script(script: str) -> str:
    """
    Filter string of lines that contains '# clowm_filter'

    Parameters
    ----------
    script : str
        Script that needs to be filtered

    Returns
    -------
    filtered_script : str
        Filtered script
    """
    filtered_script = ""
    print(script)
    with io.StringIO(script) as f:
        for line in f:
            if "# clowm_filter" not in line:
                print(line)
                filtered_script += line
            else:
                print("Filtered:", line)
    return filtered_script


class SlurmClient:
    def __init__(self, client: AsyncClient):
        """
        Initialize the client to communicate with a Slurm cluster.

        Parameters
        ----------
        client : httpx.AsyncClient
            Async HTTP client with an open connection.
        """
        self._client = client
        self._headers = {
            "X-SLURM-USER-TOKEN": settings.cluster.slurm.token.get_secret_value(),
            "X-SLURM-USER-NAME": settings.cluster.slurm.user,
        }
        self._base_url = f"{settings.cluster.slurm.uri}slurm/v0.0.38"

    async def submit_job(self, job_submission: SlurmJobSubmission) -> int:
        """
        Submit a job to the slurm cluster.

        Parameters
        ----------
        job_submission : app.slurm.schemas.SlurmJobSubmission
            Job with properties to execute on the cluster.

        Returns
        -------
        slurm_job_id : int
            Slurm job ID of submitted job.
        """
        with tracer.start_as_current_span(
            "slurm_submit_job",
            attributes={
                "parameters": job_submission.job.model_dump_json(
                    exclude_none=True, context={"obscure_keys": ["AWS_ACCESS_KEY_ID", "AWS_SECRET_ACCESS_KEY"]}
                ),
                "job_script": _filter_script(job_submission.script),
            },
        ) as span:
            response = await self._client.post(
                self._base_url + "/job/submit",
                headers={"Content-Type": "application/json", **self._headers},
                content=job_submission.model_dump_json(exclude_none=True),
            )
            span.set_attribute("response_status_code", response.status_code)
            if response.status_code != status.HTTP_200_OK:
                raise KeyError("Error at slurm")
            job_id = int(response.json()["job_id"])
            span.set_attribute("job_id", job_id)
            return job_id

    async def cancel_job(self, job_id: int) -> None:
        """
        Cancel a Slurm job on the cluster.

        Parameters
        ----------
        job_id : int
            ID of the job to cancel.
        """
        with tracer.start_as_current_span("slurm_cancel_job"):
            await self._client.delete(self._base_url + f"/job/{job_id}", headers=self._headers)

    async def is_job_finished(self, job_id: int) -> bool:  # pragma: no cover
        """
        Check if the job with the given is completed

        Parameters
        ----------
        job_id : int
            ID of the job to cancel.

        Returns
        -------
        finished : bool
            Flag if the job is finished
        """
        with tracer.start_as_current_span("slurm_check_job_status") as span:
            response = await self._client.get(self._base_url + f"/job/{job_id}", headers=self._headers)
            span.set_attribute("slurm.job-status.request.code", response.status_code)
            if response.status_code != status.HTTP_200_OK:
                return True
            try:
                job_state = response.json()["jobs"][0]["job_state"]
                span.set_attribute("slurm.job-status.state", job_state)
                return job_state == "COMPLETED" or job_state == "FAILED" or job_state == "CANCELLED"
            except (KeyError, IndexError) as ex:
                span.record_exception(ex)
                return True
