from typing import Any
from uuid import uuid4

from pydantic import BaseModel, Field, SecretStr, SerializationInfo, field_serializer


class SlurmJobProperties(BaseModel):
    current_working_directory: str = Field(
        "/tmp", description="Instruct Slurm to connect the batch script's standard output directly to the file name."
    )
    environment: dict[str, Any] = Field(default_factory=lambda: {}, description="Dictionary of environment entries.")
    name: str = Field(default_factory=lambda: uuid4().hex, description="Specify a name for the job allocation.")
    requeue: bool = Field(False, description="Specifies that the batch job should eligible to being requeue.")
    standard_output: str | None = Field(
        None, description="Instruct Slurm to connect the batch script's standard output directly to the file name."
    )
    argv: list[str] | None = Field(None, description="Arguments to the script.")

    @field_serializer("environment")
    def remove_env_with_keys(self, v: dict[str, Any], info: SerializationInfo) -> dict[str, Any]:
        context = info.context
        if context:
            env_keys = context.get("obscure_keys", set())
            return {key: str(SecretStr(value)) if key in env_keys else value for key, value in v.items()}
        return v


class SlurmJobSubmission(BaseModel):
    script: str = Field(..., description="Executable script to run in batch step")
    job: SlurmJobProperties = Field(...)
