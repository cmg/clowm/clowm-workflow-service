from uuid import UUID

from opentelemetry import trace
from rgwadmin import RGWAdmin

from app.core.config import settings
from app.schemas.s3key import S3Key

tracer = trace.get_tracer_provider().get_tracer(__name__)

rgw = RGWAdmin(
    access_key=settings.s3.admin_access_key,
    secret_key=settings.s3.admin_secret_key.get_secret_value(),
    secure=settings.s3.uri.scheme == "https",
    server=str(settings.s3.uri).split("://")[-1][:-1],
)


def get_s3_keys(uid: UUID, *, rgw: RGWAdmin) -> list[S3Key]:
    with tracer.start_as_current_span("s3_get_user_keys", attributes={"uid": str(uid)}):
        return [S3Key(uid=uid, **key) for key in rgw.get_user(uid=str(uid), stats=False)["keys"]]
