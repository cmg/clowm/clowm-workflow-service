from typing import Sequence
from uuid import UUID

from clowmdb.models import WorkflowExecution
from opentelemetry import trace
from sqlalchemy import delete, func, or_, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from app.schemas.workflow_execution import DevWorkflowExecutionIn, WorkflowExecutionIn

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDWorkflowExecution:
    @staticmethod
    async def create(
        execution: WorkflowExecutionIn | DevWorkflowExecutionIn,
        executor_id: UUID,
        notes: str | None = None,
        *,
        db: AsyncSession,
    ) -> WorkflowExecution:
        """
        Create a workflow execution in the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        execution : app.schemas.workflow_execution.WorkflowExecutionIn | DevWorkflowExecutionIn
            Workflow execution input parameters.
        executor_id : uuid.UUID
            User who started the workflow execution.
        notes : str | None, default None
            Notes to add to the workflow execution. Only usd if 'execution' has type 'DevWorkflowExecutionIn'.

        Returns
        -------
        workflow_execution : clowmdb.models.WorkflowExecution
            The newly created workflow execution
        """
        with tracer.start_as_current_span(
            "db_create_workflow_execution", attributes={"executor_id": str(executor_id)}
        ) as span:
            if isinstance(execution, WorkflowExecutionIn):
                span.set_attribute("workflow_version_id", execution.workflow_version_id)
                workflow_execution = WorkflowExecution(
                    executor_id_bytes=executor_id.bytes,
                    workflow_version_id=execution.workflow_version_id,
                    notes=execution.notes,
                    slurm_job_id=-1,
                    _workflow_mode_id=execution.mode_id.bytes if execution.mode_id is not None else None,
                )
            else:
                span.set_attributes(
                    {"git_commit_hash": execution.git_commit_hash, "repository_url": str(execution.repository_url)}
                )
                workflow_execution = WorkflowExecution(
                    executor_id_bytes=executor_id.bytes,
                    workflow_version_id=None,
                    notes=notes,
                    slurm_job_id=-1,
                )
            db.add(workflow_execution)
            await db.commit()
            await db.refresh(workflow_execution)
            span.set_attribute("workflow_execution_id", str(workflow_execution.execution_id))
            with tracer.start_as_current_span(
                "db_create_workflow_execution_update_paths",
                attributes={"execution_id": str(workflow_execution.execution_id)},
            ):
                await db.execute(
                    update(WorkflowExecution)
                    .where(WorkflowExecution.execution_id_bytes == workflow_execution.execution_id.bytes)
                    .values(
                        logs_path=(
                            None
                            if execution.logs_s3_path is None
                            else execution.logs_s3_path + f"/run-{workflow_execution.execution_id.hex}"
                        ),
                        debug_path=(
                            None
                            if execution.debug_s3_path is None
                            else execution.debug_s3_path + f"/run-{workflow_execution.execution_id.hex}"
                        ),
                        provenance_path=(
                            None
                            if execution.provenance_s3_path is None
                            else execution.provenance_s3_path + f"/run-{workflow_execution.execution_id.hex}"
                        ),
                    )
                )
                await db.commit()
            return workflow_execution

    @staticmethod
    async def get(execution_id: UUID, *, db: AsyncSession) -> WorkflowExecution | None:
        """
        Get a workflow execution by its execution id from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        execution_id : uuid.UUID
            ID of the workflow execution

        Returns
        -------
        workflow_execution : clowmdb.models.WorkflowExecution
            The workflow execution with the given id if it exists, None otherwise
        """
        stmt = (
            select(WorkflowExecution)
            .where(WorkflowExecution.execution_id_bytes == execution_id.bytes)
            .options(joinedload(WorkflowExecution.workflow_version))
        )
        with tracer.start_as_current_span(
            "db_get_workflow_execution", attributes={"workflow_execution_id": str(execution_id), "sql_query": str(stmt)}
        ):
            return await db.scalar(stmt)

    @staticmethod
    async def list(
        executor_id: UUID | None = None,
        workflow_version_id: str | None = None,
        status_list: list[WorkflowExecution.WorkflowExecutionStatus] | None = None,
        *,
        db: AsyncSession,
    ) -> Sequence[WorkflowExecution]:
        """
        List all workflow executions and apply filter.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        executor_id : uuid.UUID | None, default None
            Filter for the user who started the workflow execution.
        workflow_version_id : str | None, default None
            Filter for the workflow version
        status_list : list[clowmdb.models.WorkflowExecution.WorkflowExecutionStatus] | None, default None
            Filter for the status of the workflow executions.

        Returns
        -------
        workflow_executions : list[clowmdb.models.WorkflowExecution]
            List of all workflow executions with applied filters.
        """
        with tracer.start_as_current_span("db_list_workflow_executions") as span:
            stmt = select(WorkflowExecution).options(joinedload(WorkflowExecution.workflow_version))
            if executor_id is not None:
                span.set_attribute("executor_id", str(executor_id))
                stmt = stmt.where(WorkflowExecution.executor_id_bytes == executor_id.bytes)
            if workflow_version_id is not None:
                span.set_attribute("git_commit_hash", workflow_version_id)
                stmt = stmt.where(WorkflowExecution.workflow_version_id == workflow_version_id)
            if status_list is not None:
                span.set_attribute("status", [stat.name for stat in status_list])
                stmt = stmt.where(or_(*[WorkflowExecution.status == status for status in status_list]))
            span.set_attribute("sql_query", str(stmt))
            return (await db.scalars(stmt)).all()

    @staticmethod
    async def delete(execution_id: UUID, *, db: AsyncSession) -> None:
        """
        Delete a workflow execution from the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        execution_id : uuid.UUID
            ID of the workflow execution
        """
        stmt = delete(WorkflowExecution).where(WorkflowExecution.execution_id_bytes == execution_id.bytes)
        with tracer.start_as_current_span(
            "db_delete_workflow_execution",
            attributes={"workflow_execution_id": str(execution_id), "sql_query": str(stmt)},
        ):
            await db.execute(stmt)
            await db.commit()

    @staticmethod
    async def set_error(
        execution_id: UUID,
        status: WorkflowExecution.WorkflowExecutionStatus = WorkflowExecution.WorkflowExecutionStatus.CANCELED,
        *,
        db: AsyncSession,
    ) -> None:
        """
        Update the status of a workflow execution to CANCELED in the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        execution_id : uuid.UUID
            ID of the workflow execution
        status : clowmdb.models.WorkflowExecution.WorkflowExecutionStatus, default WorkflowExecutionStatus.CANCELED
            Error status the workflow execution should get
        """
        stmt = (
            update(WorkflowExecution)
            .where(WorkflowExecution.execution_id_bytes == execution_id.bytes)
            .values(status=status.name, end_time=func.UNIX_TIMESTAMP())
        )
        with tracer.start_as_current_span(
            "db_cancel_workflow_execution",
            attributes={"workflow_execution_id": str(execution_id), "status": status.name, "sql_query": str(stmt)},
        ):
            await db.execute(stmt)
            await db.commit()

    @staticmethod
    async def update_slurm_job_id(execution_id: UUID, slurm_job_id: int, *, db: AsyncSession) -> None:
        """
        Update the status of a workflow execution to CANCELED in the database.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        execution_id : uuid.UUID
            ID of the workflow execution
        slurm_job_id : int
            New slurm job ID
        """
        stmt = (
            update(WorkflowExecution)
            .where(WorkflowExecution.execution_id_bytes == execution_id.bytes)
            .values(slurm_job_id=slurm_job_id)
        )
        with tracer.start_as_current_span(
            "db_update_workflow_execution_slurm_id",
            attributes={
                "workflow_execution_id": str(execution_id),
                "slurm_job_id": slurm_job_id,
                "sql_query": str(stmt),
            },
        ):
            await db.execute(stmt)
            await db.commit()
