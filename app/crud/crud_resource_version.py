from uuid import UUID

from clowmdb.models import ResourceVersion
from opentelemetry import trace
from sqlalchemy import select, text, update
from sqlalchemy.ext.asyncio import AsyncSession

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDResourceVersion:
    @staticmethod
    async def get(
        resource_id: UUID, resource_version_id: UUID | None = None, latest: bool = False, *, db: AsyncSession
    ) -> ResourceVersion | None:
        """
        Get a resource version based on some conditions.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        resource_id : uuid.UUID
            ID of a resource.
        resource_version_id : uuid.UUID | None
            ID of a resource version.
        latest : bool, default False
            Add the condition that the resource version has the status LATEST.

        Returns
        -------
        resource_version : clowmdb.models.ResourceVesion | None
            The resource version with the given conditions if it exists, None otherwise
        """

        stmt = select(ResourceVersion).where(ResourceVersion.resource_id_bytes == resource_id.bytes)
        if resource_version_id is not None:
            stmt = stmt.where(ResourceVersion.resource_version_id_bytes == resource_version_id.bytes)
        if latest:
            stmt = stmt.where(ResourceVersion.status == ResourceVersion.Status.LATEST)
        with tracer.start_as_current_span(
            "db_get_resource", attributes={"resource_id": str(resource_id), "sql_query": str(stmt)}
        ):
            return await db.scalar(stmt)

    @staticmethod
    async def update_used_resource_version(resource_version_id: UUID, *, db: AsyncSession) -> None:
        """
        Update the last used timestamp and the number of times the resource version is used.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        resource_version_id : uuid.UUID
            ID of a resource version.
        """

        stmt = (
            update(ResourceVersion)
            .where(ResourceVersion.resource_version_id_bytes == resource_version_id.bytes)
            .values(times_used=ResourceVersion.times_used + 1, last_used_timestamp=text("UNIX_TIMESTAMP()"))
        )
        with tracer.start_as_current_span(
            "db_update_used_resource_version",
            attributes={"resource_version_id": str(resource_version_id), "sql_query": str(stmt)},
        ):
            await db.execute(stmt)
            await db.commit()
