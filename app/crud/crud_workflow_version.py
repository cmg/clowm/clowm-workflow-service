import json
from typing import Sequence
from uuid import UUID

from clowmdb.models import WorkflowVersion, workflow_mode_association_table
from opentelemetry import trace
from sqlalchemy import desc, insert, or_, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload, selectinload

from app.schemas.workflow_version import ParameterExtension

tracer = trace.get_tracer_provider().get_tracer(__name__)


class CRUDWorkflowVersion:
    @staticmethod
    async def get(
        workflow_version_id: str, workflow_id: UUID | None = None, populate_workflow: bool = False, *, db: AsyncSession
    ) -> WorkflowVersion | None:
        """
        Get a workflow version by its commit git_commit_hash.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        workflow_version_id : str
            Git commit git_commit_hash of the version.
        workflow_id : UUID | None, default None
            Specify the workflow the version has to belong to.
        populate_workflow: boolean, default False
            Flag if to populate the workflow attribute.

        Returns
        -------
        user : clowmdb.models.WorkflowVersion | None
            The workflow version with the given git_commit_hash if it exists, None otherwise
        """
        with tracer.start_as_current_span(
            "db_get_workflow_version",
            attributes={"workflow_version_id": workflow_version_id, "populate_workflow": populate_workflow},
        ) as span:
            stmt = (
                select(WorkflowVersion)
                .where(WorkflowVersion.git_commit_hash == workflow_version_id)
                .options(selectinload(WorkflowVersion.workflow_modes))
            )
            if populate_workflow:
                stmt = stmt.options(joinedload(WorkflowVersion.workflow))
            if workflow_id is not None:
                span.set_attribute("workflow_id", str(workflow_id))
                stmt = stmt.where(WorkflowVersion.workflow_id_bytes == workflow_id.bytes)
            span.set_attribute("sql_query", str(stmt))
            return await db.scalar(stmt)

    @staticmethod
    async def get_latest(workflow_id: UUID, published: bool = True, *, db: AsyncSession) -> WorkflowVersion | None:
        """
        Get the latest version of a workflow.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        workflow_id : uuid.UUID
            Id of a workflow
        published : bool, default = True
            Get the latest versions that is published, otherwise get latest version overall

        Returns
        -------
        user : clowmdb.models.WorkflowVersion | None
            The latest workflow version of the given workflow if the workflow exists, None otherwise
        """
        with tracer.start_as_current_span(
            "db_get_latest_workflow_version", attributes={"workflow_id": str(workflow_id), "published": published}
        ) as span:
            stmt = (
                select(WorkflowVersion)
                .where(WorkflowVersion.workflow_id_bytes == workflow_id.bytes)
                .order_by(desc(WorkflowVersion.created_at))
                .limit(1)
                .options(selectinload(WorkflowVersion.workflow_modes))
            )
            if published:
                stmt = stmt.where(
                    or_(
                        *[
                            WorkflowVersion.status == status
                            for status in [WorkflowVersion.Status.PUBLISHED, WorkflowVersion.Status.DEPRECATED]
                        ]
                    )
                )
            span.set_attribute("sql_query", str(stmt))
            return await db.scalar(stmt)

    @staticmethod
    async def list_workflow_versions(
        workflow_id: UUID, version_status: list[WorkflowVersion.Status] | None = None, *, db: AsyncSession
    ) -> Sequence[WorkflowVersion]:
        """
        List all versions of a workflow.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        workflow_id : uuid.UUID
            ID of a workflow.
        version_status : list[clowmdb.models.WorkflowVersion.Status] | None, default None
            Filter versions based on the status

        Returns
        -------
        user : list[clowmdb.models.WorkflowVersion]
            All workflow version of the given workflow
        """
        with tracer.start_as_current_span(
            "db_list_workflow_versions", attributes={"workflow_id": str(workflow_id)}
        ) as span:
            stmt = (
                select(WorkflowVersion)
                .options(selectinload(WorkflowVersion.workflow_modes))
                .where(WorkflowVersion.workflow_id_bytes == workflow_id.bytes)
            )
            if version_status is not None and len(version_status) > 0:
                span.set_attribute("version_status", [stat.name for stat in version_status])
                stmt = stmt.where(or_(*[WorkflowVersion.status == status for status in version_status]))
            stmt = stmt.order_by(WorkflowVersion.created_at)
            span.set_attribute("sql_query", str(stmt))
            return (await db.scalars(stmt)).unique().all()

    @staticmethod
    async def create(
        git_commit_hash: str,
        version: str,
        workflow_id: UUID,
        icon_slug: str | None = None,
        previous_version: str | None = None,
        modes: list[UUID] | None = None,
        parameter_extension: dict | None = None,
        *,
        db: AsyncSession,
    ) -> WorkflowVersion:
        """
        Create a new workflow version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        git_commit_hash : str
            Git commit git_commit_hash of the version.
        version : str
            New version in string format
        workflow_id : uuid.UUID
            ID of the corresponding workflow
        icon_slug : str | None, default None
            Slug of the icon
        previous_version : str | None, default None
            Git commit git_commit_hash of the previous version
        modes : list[UUID] | None, default None
            List of workflow mode IDs that are linked to the new workflow version
        parameter_extension : dict | None, default None
            The parameter extension dict for the new workflow version

        Returns
        -------
        workflow_version : clowmdb.models.WorkflowVersion
            Newly create WorkflowVersion
        """
        with tracer.start_as_current_span(
            "db_create_workflow_version",
            attributes={"git_commit_version": git_commit_hash, "workflow_id": str(workflow_id)},
        ) as span:
            if previous_version is not None:  # pragma: no cover
                span.set_attribute("previous_version", previous_version)
            if parameter_extension is not None:  # pragma: no cover
                span.set_attribute("parameter_extension", json.dumps(parameter_extension))
            if modes is None:
                modes = []
            workflow_version = WorkflowVersion(
                git_commit_hash=git_commit_hash,
                version=version,
                workflow_id_bytes=workflow_id.bytes,
                icon_slug=icon_slug,
                previous_version_hash=previous_version,
                parameter_extension=parameter_extension,
            )
            db.add(workflow_version)
            if len(modes) > 0:
                span.set_attribute("mode_ids", [str(m) for m in modes])
                with tracer.start_as_current_span(
                    "db_create_workflow_version_connect_modes",
                    attributes={"workflow_version_id": git_commit_hash, "mode_ids": [str(m) for m in modes]},
                ):
                    await db.commit()
                    await db.execute(
                        insert(workflow_mode_association_table),
                        [
                            {"workflow_version_commit_hash": git_commit_hash, "workflow_mode_id": mode_id.bytes}
                            for mode_id in modes
                        ],
                    )
            await db.commit()
            return workflow_version

    @staticmethod
    async def update_status(workflow_version_id: str, status: WorkflowVersion.Status, *, db: AsyncSession) -> None:
        """
        Update the status of a workflow version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        workflow_version_id : str
            Git commit git_commit_hash of the version.
        status : clowmdb.models.WorkflowVersion.Status
            New status of the workflow version
        """
        stmt = (
            update(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == workflow_version_id)
            .values(status=status.name)
        )
        with tracer.start_as_current_span(
            "db_update_workflow_version_status",
            attributes={"status": status.name, "workflow_version_id": workflow_version_id, "sql_query": str(stmt)},
        ):
            await db.execute(stmt)
            await db.commit()

    @staticmethod
    async def update_parameter_extension(
        workflow_version_id: str, parameter_extension: ParameterExtension | None, *, db: AsyncSession
    ) -> None:
        """
        Update the parameter extension of a workflow version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        workflow_version_id : str
            Git commit git_commit_hash of the version.
        parameter_extension : app.schemas.workflow_version.ParameterExtension | None
            New parameter extension of the workflow version
        """
        stmt = (
            update(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == workflow_version_id)
            .values(parameter_extension=None if parameter_extension is None else parameter_extension.model_dump())
        )
        with tracer.start_as_current_span(
            "db_update_workflow_version_parameter_extension",
            attributes={
                "parameters": "" if parameter_extension is None else parameter_extension.model_dump_json(),
                "workflow_version_id": workflow_version_id,
                "sql_query": str(stmt),
            },
        ):
            await db.execute(stmt)
            await db.commit()

    @staticmethod
    async def update_icon(workflow_version_id: str, icon_slug: str | None = None, *, db: AsyncSession) -> None:
        """
        Update the icon slug for a workflow version.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        workflow_version_id : str
            Git commit git_commit_hash of the version.
        icon_slug : str | None, default None
            The new icon slug
        """
        stmt = (
            update(WorkflowVersion)
            .where(WorkflowVersion.git_commit_hash == workflow_version_id)
            .values(icon_slug=icon_slug)
        )
        with tracer.start_as_current_span(
            "db_update_workflow_version_icon",
            attributes={
                "workflow_version_id": workflow_version_id,
                "icon_slug": icon_slug if icon_slug else "None",
                "sql_query": str(stmt),
            },
        ):
            await db.execute(stmt)
            await db.commit()

    @staticmethod
    async def icon_exists(icon_slug: str, *, db: AsyncSession) -> bool:
        """
        Check if there is a workflow version that depends on a icon.

        Parameters
        ----------
        db : sqlalchemy.ext.asyncio.AsyncSession.
            Async database session to perform query on.
        icon_slug : str
            The icon slug to search for

        Returns
        -------
        exists : bool
            Flag if a version exists that depends on the icon
        """
        stmt = select(WorkflowVersion).where(WorkflowVersion.icon_slug == icon_slug).limit(1)
        with tracer.start_as_current_span(
            "db_check_workflow_version_icon_exists", attributes={"icon_slug": icon_slug, "sql_query": str(stmt)}
        ):
            version_with_icon = await db.scalar(stmt)
            return version_with_icon is not None
